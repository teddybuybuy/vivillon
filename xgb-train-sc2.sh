#!/usr/bin/env bash
set -e -x

INPUT_DIR=$1

if [ -z "$2" ]; then
    OUTPUT_FILE='xgb_model_2.dat'
else
    OUTPUT_FILE=$2
fi

if [ -z "$3" ]; then
    SEED=895
else
    SEED=$3
fi


if [ -z "$4" ]; then
    OUTPUT_DIR=/tmp
else
    OUTPUT_DIR=$4
fi

if [ -z "$5" ]; then
    TESTPART=0.33
else
    TESTPART=$5
fi

python car-plate/xgbooster.py --exam_metadata=${INPUT_DIR}/exams_metadata.tsv --output=${OUTPUT_DIR}/per_breast_metadata.tsv --phase=0
python car-plate/xgbooster.py --phase=1 --dcm_info=${INPUT_DIR}/dcm_info.json --image_crosswalk=${INPUT_DIR}/images_crosswalk.tsv --exam_metadata=${INPUT_DIR}/exams_metadata.tsv --features_result=${INPUT_DIR}/features_result.json --output=${OUTPUT_DIR}/per_breast_dcminfo_sc2.tsv
python car-plate/xgbooster.py --phase=2 --exam_metadata=${OUTPUT_DIR}/per_breast_metadata.tsv --dcm_info=${OUTPUT_DIR}/per_breast_dcminfo_sc2.tsv --output=${OUTPUT_DIR}/per_breast_training_data_sc2.tsv
python car-plate/xgbooster.py --exam_metadata=${OUTPUT_DIR}/per_breast_training_data_sc2.tsv --model=${OUTPUT_FILE} --phase=3 --seed=${SEED} --testpart=${TESTPART}
# python car-plate/inference.py --image_crosswalk_csv=${INPUT_DIR}/images_crosswalk.tsv --exams_metadata_csv=${INPUT_DIR}/exams_metadata.tsv --dcm_info=${INPUT_DIR}/dcm_info.json --features_result=${INPUT_DIR}/features_result.json --model=${OUTPUT_FILE}
rm -f ${OUTPUT_DIR}/per_breast_dcminfo_sc2.tsv ${OUTPUT_DIR}/per_breast_metadata.tsv ${OUTPUT_DIR}/per_breast_training_data_sc2.tsv

