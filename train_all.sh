#!/bin/sh
cd /src
./train.sh resize_400.csv /metadata /modelState /preprocessedData /scratch/model_resize;
./train.sh calci-result.csv /metadata /modelState /preprocessedData /scratch/model_calci;
./train.sh crop_meta.tsv /metadata /modelState /preprocessedData /scratch/model_mass;
