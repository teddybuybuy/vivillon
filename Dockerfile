#from docker.synapse.org/syn8048873/t4@sha256:d908ac2c91ff13fb8810aec873d726eef57cb03829da6c8874e2ef0218e4e36a
#from docker.synapse.org/syn8048873/t5@sha256:144aa98a3ccf17d8836091490bbfc7d6904de463deeea338142bc786bd19d987
#from vacuum/dl:tensorflow-1.0.0sse-plus-b
#from docker.synapse.org/syn8048873/f1@sha256:98ff5ba2f5a8588d3266b2accfdf0dce77040b04a27bd7b291a188c8c6af493a
#from docker.synapse.org/syn8048873/f1@sha256:c15a693f80aa152ce5c9c767f838c8b24d9ddccd48dbbb70ae0359062cdc350a
#from docker.synapse.org/syn8048873/f1@sha256:c3868985259f09b88498b85fb79394668b05e18f89f554817ce6e62ab0e263fe
from docker.synapse.org/syn8048873/f1@sha256:bda1b6625711c528528e26f75deed4dac5bffbeeb748ad5da04e4f1b06ef693d

WORKDIR /
COPY . /src/
COPY preprocess.sh /
COPY train_all.sh /train.sh
#ENV DREAM_TEST 1

COPY sc1_infer.sh /
COPY sc2_infer.sh /
