import argparse
import csv
import os
import sys
import logging

import numpy as np
import sklearn
from sklearn import metrics
import matplotlib.pylab as plt

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

def read_score_file(fname):
    rv_dict = {}
    for line in open(fname):
        items = line.strip().split()
        rv_dict[items[0]] = float(items[1])
    #print rv_dict
    return rv_dict

def main(args):
    parser = argparse.ArgumentParser(description = "calculate aoc curve given prediction score and ground truth")
    parser.add_argument("--truth-file", required=True, type=str, default=None)
    parser.add_argument("--predict-file", required=True, type=str, default=None)

    opts = parser.parse_args(args[1:])

    truth_dict = read_score_file(opts.truth_file)
    predict_dict = read_score_file(opts.predict_file)

    predict_list = []
    truth_list = []
    for key, val in predict_dict.items():
        if truth_dict.has_key(key):
            predict_list.append(val)
            truth_list.append(truth_dict[key])
        else:
            print("XXX Sth wrong with this key, no truth!", key)
            pass
        pass

    #print predict_list, truth_list
    Y = np.array(truth_list)
    Y_prob = np.array(predict_list)

    fpr, tpr, thresholds = metrics.roc_curve(Y, Y_prob)
    #print (fpr, tpr, thresholds)
    print("Auc: %.4g" % metrics.auc(fpr, tpr))
    # copied from xgbooster.py code.

    tpr80 = [ 0 if x < 0.8 else 1 for x in tpr ] # which tpr have > 80 sensitivity.
    x = [a*b for a,b in zip(tpr80,fpr)] # mask
    xx = [y for y in x if y > 0] # filter out tpr < 80.
    print("Specificity at Sensitivity 0.8: %.4g" % (1 - np.min(xx)))
    x = [a*b for a,b in zip(tpr80,tpr)]
    xx = [y - 0.8 for y in x if y > 0]
    print("pAUC %.4g" % (np.sum(xx) / len(tpr)))
    if False:
        plt.plot(fpr, tpr, label='ROC curve (area = %0.3f)' % metrics.roc_auc_score(Y, Y_prob))
        plt.plot([0, 1], [0, 1], 'k--')  # random predictions curve
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.0])
        plt.xlabel('False Positive Rate or (1 - Specifity)')
        plt.ylabel('True Positive Rate or (Sensitivity)')
        plt.title('Receiver Operating Characteristic')
        plt.legend(loc="lower right")
        plt.show()

    
if __name__ == '__main__':
    main(sys.argv)

