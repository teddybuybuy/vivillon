#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging
import os,sys
#os.environ['LD_LIBRARY_PATH']='/opt/cuda/lib64'
import picpac
import tensorflow as tf
import time
import numpy as np
import cv2
import random
import errno

import dc_utils
import fcn32_vgg
import fcn32_vgg2
import fcn8_vgg
import fcn8_vgg2
import fcn4_vgg2
import fcns_vgg2
import fcnh_vgg2
import fcn_slim
from scipy.stats import threshold

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

BATCH = 10

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_float('learning_rate', 0.01, 'Initial learning rate.')
flags.DEFINE_float('dropout_rate', 0.5, 'Dropout rate.')
flags.DEFINE_integer('max_steps', 400000, 'Number of steps to run trainer.')
flags.DEFINE_integer('max_size', 1000, 'Max long side of images.')
flags.DEFINE_integer('perturb_color_1', 10, 'Perturb color_1 percent.')
flags.DEFINE_integer('perturb_color_2', 10, 'Perturb color_2 percent.')
flags.DEFINE_integer('perturb_color_3', 10, 'Perturb color_3 percent.')
flags.DEFINE_string('perturb_colorspace', 'RGB', 'Perturb colorspace (RGB/Lab/HSV).')
flags.DEFINE_integer('perturb_angle', 10, 'Perturb angle.')
flags.DEFINE_float('perturb_min_scale', 0.6, 'Perturb min scale.')
flags.DEFINE_float('perturb_max_scale', 2.0, 'Perturb max scale.')
flags.DEFINE_float('perturb_gamma', 0, 'Perturb gamma from 1-x to 1+x')
flags.DEFINE_float('resize_scale', 1.0, 'resize the image')
flags.DEFINE_integer('min_average', 0, 'min average for trainning/validate data')
flags.DEFINE_string('db', 'db', 'Training db path.')
flags.DEFINE_string('db2', '', 'Training db2 path.')
flags.DEFINE_string('db3', '', 'Training db3 path.')
flags.DEFINE_string('db4', '', 'Training db4 path.')
flags.DEFINE_string('db5', '', 'Training db5 path.')
flags.DEFINE_integer('db2_multiplier', 1, 'alternate between db and db2, 1 mean equal ops, > 1 means db2 has preference')
flags.DEFINE_string('db_validate', 'db-validate-2', 'Validation db path.')
flags.DEFINE_string('db2_validate', None, 'Validation db2 path.')
flags.DEFINE_string('db3_validate', None, 'Validation db3 path.')
flags.DEFINE_string('db4_validate', None, 'Validation db4 path.')
flags.DEFINE_string('db5_validate', None, 'Validation db5 path.')
flags.DEFINE_string('validate_prefix', '', 'Validation file prefix.')
flags.DEFINE_integer('split_fold', 5, 'Validation split_fold.')
flags.DEFINE_integer('max_validate', 10, 'Max number of images to validate.')
flags.DEFINE_integer('channels', 3, '')
flags.DEFINE_integer('real_channels', 1, '')
flags.DEFINE_integer('out_channels', 2, '')
flags.DEFINE_integer('batch_size', 1, 'Batch size.  '
                     'Must divide evenly into the dataset sizes.')
flags.DEFINE_string('train_dir', 'data', 'Directory to put the training data.')
flags.DEFINE_boolean('fake_data', False, 'If true, uses fake data '
                     'for unit testing.')
flags.DEFINE_string('load', '', 'If non empty, load existing model')
flags.DEFINE_integer('lr_decay_step', 0, 'Steps between lr')
flags.DEFINE_boolean('use_adam', True, 'If true, uses adam optimizer instead of momentum')
flags.DEFINE_boolean('use_vgg', False, 'If true, uses vgg')
flags.DEFINE_boolean('use_vgg2', False, 'If true, uses vgg2')
flags.DEFINE_boolean('use_vgg8', False, 'If true, uses fcn8_vgg')
flags.DEFINE_boolean('use_vgg8_2', False, 'If true, uses fcn8_vgg2')
flags.DEFINE_boolean('use_vgg4_2', False, 'If true, uses fcn4_vgg2')
flags.DEFINE_boolean('use_vggs_2', False, 'If true, uses fcns_vgg2')
flags.DEFINE_boolean('use_vggh_2', False, 'If true, uses fcnh_vgg2')
flags.DEFINE_float('vgg_keep_split', 1, 'How many of original filters to keep, 0.5 means only keep half! Also works for use_vgg2')
flags.DEFINE_boolean('print_var', False, 'If true, print all variables')
flags.DEFINE_boolean('perturb_validate', True, 'perturb during validate, default true')
flags.DEFINE_boolean('picpac_cache', False, 'picpac cache, default false')
flags.DEFINE_float('training_hour', 1e10, 'At most train this many hours.')


def safe_load (stream, is_training=True):
    def reshape(b):
        if len(b.shape) == 2:
            shape = (1,) + b.shape + (1,)
        elif len(b.shape) == 3:
            shape = (1,) + b.shape
        else:
            assert False, b.shape
        return np.reshape(b, shape)

    def adjust_gamma(image, max_v, gamma=1.0):
        # build a lookup table mapping the pixel values [0, 255] to
        # their adjusted gamma values
        max = np.max(image)
        if max > max_v:
            # logger.debug("max too big: %s > %s. do threshold", max, max_v)
            image = threshold(image, threshmin=-max_v, threshmax=max_v-1, newval=max_v-1)
        min = np.min(image)
        if min < 0:
            # logger.debug("min too small: %s > %s. do threshold", min, 0)
            image = threshold(image, threshmin=0, threshmax=max_v-1, newval=0)

        invGamma = 1.0 / gamma
        table = np.array([((i / max_v) ** invGamma) * max_v
                          for i in np.arange(0, int(max_v) + 1)]).astype("int")

        # apply gamma correction using the lookup table
        ret = table[image.astype("int")].astype("float32")
        # logger.info("gamma: %.2f, max %.1f/%.1f, after %.1f/%.1f", gamma, np.max(image), np.max(ret), np.min(image), np.min(ret))
        return ret

    while True:
        images, labels, pad = stream.next()
        m = np.max(images)
        if m < 65536 and images.shape[1] > 120 and images.shape[2] > 120:
            if FLAGS.min_average:
                avg = np.average(images)
                if avg < FLAGS.min_average:
                    # logging.error("image is too dark max=%s, avg=%s", str(m), str(avg))
                    if random.randint(0, 5) == 0:
                        # logger.error("still use it")
                        pass
                    else:
                        continue
                else:
                    # logging.debug("ok: max=%s, avg=%s", str(m), str(avg))
                    pass

            if FLAGS.real_channels == 1 and images.shape[3] == 3:
                b, _, _ = cv2.split(images[0])
                shape = (1,) + b.shape + (1,)
                images = np.reshape(b, shape)


            if FLAGS.resize_scale != 1:
                image = images[0]
                image = cv2.resize(image, None, None, FLAGS.resize_scale, FLAGS.resize_scale, interpolation=cv2.INTER_LINEAR)
                label = cv2.resize(labels[0], None, None, FLAGS.resize_scale, FLAGS.resize_scale, interpolation=cv2.INTER_NEAREST)
                images = reshape(image)
                labels = reshape(label)

            if FLAGS.perturb_gamma != 0:
                image = images[0]
                gamma = random.uniform(1.0 - FLAGS.perturb_gamma, 1.0 + FLAGS.perturb_gamma)
                image = adjust_gamma(image, 4095, gamma)
                images = reshape(image)

            return images, labels, pad
        logging.error("bad max-pixel=%s, shape=%s", str(m), images.shape)
        pass
    pass

def fcn_loss (logits, labels):
    with tf.name_scope('loss'):
        logits = tf.reshape(logits, (-1, FLAGS.out_channels))
        labels = tf.to_int32(labels)    # float from picpac
        labels = tf.reshape(labels, (-1,))
        xe = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=labels, name='xentropy')
        return tf.reduce_mean(xe, name='xentropy_mean')
    pass

def fcn_predict(logits):
    shape = tf.shape(logits)
    logits = tf.reshape(logits, (-1, FLAGS.out_channels))
    prob = tf.nn.softmax(logits)
    prob = tf.reshape(prob, shape, name='predict')
    return prob

def training (loss, rate):
    tf.summary.scalar(loss.op.name, loss)
    global_step = tf.Variable(0, name='global_step', trainable=False)
    if FLAGS.use_adam:
      rate *= 0.01
    if FLAGS.lr_decay_step > 0:
        decay_step =  FLAGS.lr_decay_step
    else:
        decay_step = 5000 if FLAGS.use_vgg or FLAGS.use_vgg2 or FLAGS.use_vgg8 or FLAGS.use_vgg8_2 or FLAGS.use_vgg4_2 or FLAGS.use_vggs_2 or FLAGS.use_vggh_2 else 10000
    logger.info("lr_decay_step = %s", decay_step)
    rate = tf.train.exponential_decay(rate, global_step, decay_step, 0.9, staircase=True)
    if not FLAGS.use_adam:
      optimizer = tf.train.MomentumOptimizer(rate, 0.9)
    else:
      optimizer = tf.train.AdamOptimizer(rate)
    #optimizer = tf.train.GradientDescentOptimizer(rate)
    return optimizer.minimize(loss, global_step=global_step)

def run_training ():
    seed = 1996
    config = dict(seed=seed,
                  cache=FLAGS.picpac_cache,
                shuffle=True,
                reshuffle=True,
                #resize_width=256,
                #resize_height=256,
                batch=1,
                split=1,
                split_fold=0,
                annotate='json',
                channels=FLAGS.channels,
                stratify=False,
                #mixin="db0",
                #mixin_group_delta=0,
                max_size=FLAGS.max_size,
                pert_color1=FLAGS.perturb_color_1,
                pert_color2=FLAGS.perturb_color_2,
                pert_color3=FLAGS.perturb_color_3,
                pert_colorspace = FLAGS.perturb_colorspace,
                pert_angle=FLAGS.perturb_angle,
                pert_min_scale=FLAGS.perturb_min_scale,
                pert_max_scale=FLAGS.perturb_max_scale,
                #pad=False,
                pert_hflip=True,
                channel_first=False # this is tensorflow specific
                                    # Caffe's dimension order is different.
                )

    # it is intended that the parameters same for training & validation all go into config
    # can only keep the different ones below
    # about loop:  with loop = True,   for image, label, _ in tr_stream: never ends
    # about loop:  with loop = False,  for image, label, pad in tr_stream:  ends when data deplete
    #                                  and have to reset before we can loop again
    # normally, image and label should have batch entries and pad == 0
    # but with loop = False, the last batch might be only (batch - pad) entries, with pad >= 0
    # this never happens with batch = 1, so we can always disregard pad in this program.

    # cross validation
    # without using external file, we can do cross validation this way:
    # config = {..., split=5, split_fold=0, ...} # 5-fold validation
    # tr_stream = picpac.ImageStream(db, loop=True, perturb=True, negate=False, **config)
    #   ^ uses only 4/5 the data
    # val_stream = picpac.ImageStream(db, loop=False, perturb=False, negate=True, **config)
    #   ^ negate=True tells PicPac to use the other 1/5 of the data

    # For 5-fold cross-validation, run the whole thing 5 times with split_fold = 0, 1, 2, 3, 4

    split_fold = FLAGS.split_fold
    if split_fold == 5:
        split_negate = False
    else:
        print("use split_fold %s" % split_fold)
        FLAGS.db_validate = FLAGS.db
        FLAGS.db2_validate = FLAGS.db2
        FLAGS.db3_validate = FLAGS.db3
        FLAGS.db4_validate = FLAGS.db4
        FLAGS.db5_validate = FLAGS.db5
        config["split"] = 5
        config["split_fold"] = split_fold
        split_negate = True

    dbs = []
    if FLAGS.db:
       dbs.append(FLAGS.db)
    if FLAGS.db2:
       dbs.append(FLAGS.db2)
    if FLAGS.db3:
       dbs.append(FLAGS.db3)
    if FLAGS.db4:
       dbs.append(FLAGS.db4)
    if FLAGS.db5:
       dbs.append(FLAGS.db5)

    tr_streams = []
    total_tr_size = 0
    for db in dbs:
        import copy
        config = copy.deepcopy(config)
        config['seed'] = config['seed'] + 1
        s = picpac.ImageStream(db, loop=True, perturb=True, split_negate=False, **config)
        tr_streams.append(s)
        total_tr_size += s.size()
        print("%s samples in %s" % (s.size(), db))
    print("training db size: %s" % (total_tr_size))

    dbs = []
    if FLAGS.db_validate:
       dbs.append(FLAGS.db_validate)
    if FLAGS.db2_validate:
       dbs.append(FLAGS.db2_validate)
    if FLAGS.db3_validate:
       dbs.append(FLAGS.db3_validate)
    if FLAGS.db4_validate:
       dbs.append(FLAGS.db4_validate)
    if FLAGS.db5_validate:
       dbs.append(FLAGS.db5_validate)
    val_streams = []
    for db in dbs:
        config = copy.deepcopy(config)
        config['seed'] = config['seed'] + 1
        s = picpac.ImageStream(db, loop=False, perturb=FLAGS.perturb_validate, split_negate=split_negate, **config)
        val_streams.append(s)

    with tf.Graph().as_default() as graph:
        if not FLAGS.load:
            X = tf.placeholder(tf.float32, shape=(None, None, None, FLAGS.real_channels), name="images")
            Y_ = tf.placeholder(tf.int32, shape=(None, None, None, 1), name="labels")
            dropout_rate = tf.Variable(FLAGS.dropout_rate, trainable=False, name="dropout_rate")
            dropout_update_placeholder = tf.placeholder(dropout_rate.dtype, shape=dropout_rate.get_shape(), name="dropout_placeholder")
            dropout_update_op = dropout_rate.assign(dropout_update_placeholder)

            if FLAGS.use_vgg:
                print("use vgg")
                fcn32 = fcn32_vgg.FCN32VGG(vgg16_npy_path="vgg16.npy")
                fcn32.build(X, train=True, dropout_rate=dropout_rate, num_classes=FLAGS.out_channels, random_init_fc8=False, debug=False)
                logits = fcn32.upscore
            elif FLAGS.use_vgg2:
                print("use vgg2 %s" % (FLAGS.vgg_keep_split))
                fcn32 = fcn32_vgg2.FCN32VGG(vgg16_npy_path="vgg16_conv.npy", keep_split=FLAGS.vgg_keep_split)
                fcn32.build(X, train=True, dropout_rate=dropout_rate, num_classes=FLAGS.out_channels, random_init_fc8=True, debug=False)
                logits = fcn32.upscore
            elif FLAGS.use_vgg8:
                print("use vgg8")
                fcn8 = fcn8_vgg.FCN8VGG(vgg16_npy_path="vgg16.npy")
                fcn8.build(X, train=True, dropout_rate=dropout_rate, num_classes=FLAGS.out_channels, random_init_fc8=True, debug=False)
                logits = fcn8.upscore32
            elif FLAGS.use_vgg8_2:
                print("use vgg8_2")
                fcn8 = fcn8_vgg2.FCN8VGG(vgg16_npy_path="vgg16_conv.npy")
                fcn8.build(X, train=True, dropout_rate=dropout_rate, num_classes=FLAGS.out_channels, random_init_fc8=True, debug=False)
                logits = fcn8.upscore32
            elif FLAGS.use_vgg4_2:
                print("use vgg4_2")
                fcn4 = fcn4_vgg2.FCN4VGG(vgg16_npy_path="vgg16_conv.npy")
                fcn4.build(X, train=True, dropout_rate=dropout_rate, num_classes=FLAGS.out_channels, random_init_fc8=True, debug=False)
                logits = fcn4.upscore32
            elif FLAGS.use_vggs_2:
                print("use vggs_2")
                fcns = fcns_vgg2.FCNSVGG(vgg16_npy_path="vgg16_conv.npy")
                fcns.build(X, train=True, dropout_rate=dropout_rate, num_classes=FLAGS.out_channels, random_init_fc8=True, debug=False)
                logits = fcns.upscore
            elif FLAGS.use_vggh_2:
                print("use vggh_2 %s" % (FLAGS.vgg_keep_split))
                fcnh = fcnh_vgg2.FCNHVGG(vgg16_npy_path="vgg16_conv.npy", keep_split=FLAGS.vgg_keep_split)
                fcnh.build(X, train=True, dropout_rate=dropout_rate, num_classes=FLAGS.out_channels, random_init_fc8=True, debug=False)
                logits = fcnh.upscore32
            else:
                print("use slim")
                logits = fcn_slim.FcnSlim().build(rgb=X, train=True, dropout_rate=dropout_rate, inchannels=FLAGS.real_channels, num_classes=FLAGS.out_channels)

            loss = fcn_loss(logits, Y_)
            predict = fcn_predict(logits)
            train_op = training(loss, FLAGS.learning_rate)
            summary_op = tf.summary.merge_all()

        else:
            old_model = tf.train.import_meta_graph(FLAGS.load + '.meta')
            if FLAGS.print_var:
                for n in graph.as_graph_def().node:
                    print('graph node: %s' % (n.name))
            X = graph.get_tensor_by_name("images:0")
            Y_ = graph.get_tensor_by_name("labels:0")
            loss = graph.get_tensor_by_name("loss/xentropy_mean:0")
            predict = graph.get_tensor_by_name("predict:0")
            if not FLAGS.use_adam:
              train_op = graph.get_tensor_by_name("Momentum:0")
            else:
              train_op = graph.get_tensor_by_name("Adam:0")
            global_step = graph.get_tensor_by_name("global_step:0")
            try:
                summary_op = graph.get_tensor_by_name("Merge/MergeSummary:0")
            except Exception:
                summary_op = graph.get_tensor_by_name("MergeSummary/MergeSummary:0")
            dropout_update_placeholder = graph.get_tensor_by_name("dropout_placeholder:0")
            dropout_update_op = graph.get_tensor_by_name("Assign:0")
            #dropout_update_op = graph.get_tensor_by_name("dropout_rate/Assign:0")

        try:
            os.makedirs(FLAGS.train_dir, mode=0777)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(FLAGS.train_dir):
                pass
            else:
                raise

        summary_writer = tf.summary.FileWriter(FLAGS.train_dir, tf.get_default_graph())
        learn_rate = graph.get_tensor_by_name("ExponentialDecay:0")

        #init = tf.initialize_all_variables()
        init = tf.global_variables_initializer()

        graph_txt = tf.get_default_graph().as_graph_def().SerializeToString()
        with open(os.path.join(FLAGS.train_dir, "graph"), "w") as f:
            f.write(graph_txt)
            pass

        saver = tf.train.Saver()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        print_interval = 100
        initial_time = time.time()
        with tf.Session(config=config) as sess:
            sess.run(init)
            step_start = 0
            if FLAGS.load:
                old_model.restore(sess, FLAGS.load)
                step_start = global_step.eval()
                """
                for n in tf.global_variables():
                    print('variable: %s' % (n.name))
                    if n.name == 'global_step:0':
                        step_start = int(n.eval()) + 1
                """
                print('start from step %s' % (step_start))

            total_losses = []
            start_time = time.time()
            num_tr_streams = len(tr_streams)
            for step in xrange(step_start, FLAGS.max_steps):
                stream_idx = step % num_tr_streams
                stream = tr_streams[stream_idx]
                images, labels, pad = safe_load(stream, is_training=True)
                # print(images.shape, labels.shape, labels)
                feed_dict = {X: images,
                             Y_: labels}
                _, loss_value, rate = sess.run([train_op, loss, learn_rate], feed_dict=feed_dict)
                total_losses.append(loss_value)
                if step % print_interval == 0:
                    vlose = []
                    # no dropout for validation
                    new_dropout_rate = 1
                    sess.run(dropout_update_op, {dropout_update_placeholder: new_dropout_rate})

                    val_count_total = 0
                    max_validate = FLAGS.max_validate
                    if os.path.exists("/tmp/val_all"):
                        max_validate = 100000
                    for stream in val_streams:
                        if stream is None:
                            continue
                        stream.reset()
                        val_losses = []
                        val_count = 0
                        while True:
                            try:
                                images_v, labels_v, _ = safe_load(stream, is_training=False)
                            except StopIteration:
                                break
                            sz = images_v.shape[1:3]
                            val_loss, val_predict = sess.run([loss, predict], feed_dict={X:images_v, Y_:labels_v})
                            val_losses.append(val_loss)
                            result_file = '{}val_{}'.format(FLAGS.validate_prefix, val_count_total)
                            dc_utils.write_prob_file(file_prefix=result_file, file='{}'.format(val_count_total), prob=val_predict,
                                                     origin=images_v, label=labels_v, size=sz, loss=val_loss)
                            val_count += 1
                            val_count_total += 1
                            if val_count >= max_validate:
                                break
                            pass
                        vlose.append("%.4f(%.4f)" % (np.average(val_losses), np.median(val_losses)))

                    # restore dropout
                    new_dropout_rate = FLAGS.dropout_rate
                    sess.run(dropout_update_op, {dropout_update_placeholder: new_dropout_rate})

                    stop_time = time.time()
                    print('step %d: loss=%.4f(%.4f) v-loss=%s t=%.3fs r=%.7f' % (step, np.average(total_losses),
                                                                                 np.median(total_losses),
                                                                                 vlose, stop_time - start_time, rate))
                    start_time = stop_time

                    summary_str = sess.run(summary_op, feed_dict=feed_dict)
                    summary_writer.add_summary(summary_str, step)
                    summary_writer.flush()
                    total_losses = []
                if (step + 1) % 1000 == 0 or (step + 1) == FLAGS.max_steps:
                    saver.save(sess, os.path.join(FLAGS.train_dir, "model"), global_step=step)
                    pass
                if time.time() > initial_time + (float)(FLAGS.training_hour) * 3600:
                    print("Done training, time used up")
                    break
                pass
            pass
        pass
    pass


def main (_):
    run_training()

if __name__ == '__main__':
    print("CMD argvs: %s" % " ".join(sys.argv))
    tf.app.run()

