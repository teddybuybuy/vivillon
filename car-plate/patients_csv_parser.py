import argparse
import csv
import simplejson
import os
import sys
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

CANCER_NEGATIVE = 0
CANCER_POSITIVE = 1
CANCER_UNKNOWN = -1

DEFAULT_EXAM_ID = 0

class ExamImage(object):
    def __init__(self, image_id, patient_id, exam_id, l_r, view_type, dcm_info, features, raw_row):
        super(ExamImage, self).__init__()
        filename, file_extension = os.path.splitext(image_id)
        self.image_file = image_id
        self.image_id = filename
        self.patient_id = patient_id
        try:
            self.exam_id = int(exam_id)
        except Exception:
            self.exam_id = DEFAULT_EXAM_ID
        self.l_r = l_r.lower()
        self.view_type = view_type
        self._has_cancer = CANCER_UNKNOWN
        self.dcm_info = dcm_info.get(self.image_id, {})
        self.features = features.get(self.image_id, {})
        self.raw_row = raw_row
        self.implant_now = False

    def has_cancer(self):
        return self._has_cancer

    def update_has_cancer(self, l_has_cancer, r_has_cancer):
        if self.l_r == 'l' or self.l_r == 'L':
            self._has_cancer = l_has_cancer
        else:
            self._has_cancer = r_has_cancer

    def update_implant_now(self, v):
        self.implant_now = v
        if v:
            # logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %s %s has implant", self.patient_id, self.image_file)
            pass

    def features_are_from_prediction(self):
        if len(self.features) == 0:
            return False
        return self.features.get('prediction', True)

    def  _get_feature(self, feature_name):
        if not self.features_are_from_prediction():
            return None
        names = feature_name.split('_', 1)
        assert len(names) >= 1
        ret = self.features.get(names[0], None)
        if ret != None:
            try:
                try:
                    r = float(ret)
                    assert len(names) == 1
                    return r
                except:
                    if isinstance(ret, list):
                        r = ret
                    else:
                        r = simplejson.loads(ret)
                    if len(names) == 1:
                        return float(r[0])
                    else:
                        return float(r[int(names[1])])

            except Exception:
                logger.error("v=%s", ret)
                ret = None
        else:
            if feature_name != 'mass':
                logger.error("no feature %s", feature_name) #zheng!!!
            pass
        return ret

    def get_feature(self, name):
        if name == 'mass':
            return self.feature_mass()
        elif name == 'calci':
            return self.feature_calci()
        elif name == 'glob':
            return self.feature_glob()
        else:
            #return 0 # !!!!!!!!!!!!! disable them because other features doesn't help, ask zheng

            if name in ['bravg', 'bravg_1']:
                return 0
            r = self._get_feature(name)
            logger.debug("feature %s = %s", name, r)
            return r
        # method = getattr(self, 'feature_{}'.format(name))
        # return method()

    def feature_mass(self):
        if "IGNORE_F_MASS" in os.environ:
            return 0
        ret = self._get_feature('mass')
        if ret is None:
            return 0
        max_mass = 40.0
        if ret > max_mass:
            return max_mass
        else:
            return ret

    def feature_calci(self):
        if "IGNORE_F_CALCI" in os.environ:
            return 0
        ret = self._get_feature('calci')
        return ret

    def feature_glob(self):
        if "IGNORE_F_GLOB" in os.environ:
            return 0
        ret = self._get_feature('glob')
        return ret

    def __repr__(self):
        return "id={} pid={} eid={}, {}, view={}, cancer={}".format(self.image_id, self.patient_id,
                                                                    self.exam_id, self.l_r, self.view_type, self.has_cancer())

class Exam(object):
    def __init__(self, patient_id, exam_id, l_has_cancer, r_has_cancer, raw_row, implant_now):
        super(Exam, self).__init__()
        self.patient_id = patient_id
        self.exam_id = exam_id
        self.l_has_cancer = l_has_cancer
        self.r_has_cancer = r_has_cancer
        self.implant_now = implant_now
        self.exam_images = []
        self.raw_row = raw_row
        if implant_now[0] or implant_now[1]:
            # logger.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@ %s %s has implant (%s) cancer=%s %s", self.patient_id, self.exam_id, implant_now, self.l_has_cancer, self.r_has_cancer)
            pass

    def add_exam_image(self, exam_image):
        exam_image.update_has_cancer(l_has_cancer=self.l_has_cancer, r_has_cancer=self.r_has_cancer)
        self.exam_images.append(exam_image)

    def get_all_images(self, cancer=None):
        r = []
        for image in self.exam_images:
            if cancer == CANCER_POSITIVE and image.has_cancer == CANCER_NEGATIVE:
                continue
            if cancer == CANCER_NEGATIVE and image.has_cancer == CANCER_POSITIVE:
                continue
            r.append(image)
        return r

    def get_image_by_type_l_r(self, view_type, l_r, reverse_lr=False):
        assert view_type
        assert l_r
        if reverse_lr:
            if l_r in ['l','L']:
                l_r = 'r'
            else:
                l_r = 'l'
        candidate = None
        for image in self.exam_images:
            if image.view_type.lower() == view_type.lower() and image.l_r.lower() == l_r.lower():
                if candidate == None:
                    candidate = image
                elif candidate.image_id > image.image_id:
                    candidate = image
        return candidate

    def has_implant_now(self, l_r):
        if l_r == 'l' or l_r == 'L':
            return self.implant_now[0]
        else:
            return self.implant_now[1]

    def has_cancer(self, l_r=None):
        if l_r == 'l' or l_r == 'L':
            return self.l_has_cancer
        elif l_r == 'r' or l_r == 'R':
            return self.r_has_cancer
        else:
            if self.l_has_cancer == CANCER_POSITIVE or self.r_has_cancer == CANCER_POSITIVE:
                return CANCER_POSITIVE
            elif self.l_has_cancer == CANCER_NEGATIVE and self.r_has_cancer == CANCER_NEGATIVE:
                return CANCER_NEGATIVE
            else:
                return CANCER_UNKNOWN

    def features_are_from_prediction(self, l_r=None):
        for image in self.exam_images:
            if l_r is not None:
                if l_r.lower() != image.l_r:
                    continue
            if not image.features_are_from_prediction():
                return False
        return True

    def __repr__(self):
        return "id={} pid={} l_cancer={} r_cancer={}".format(self.exam_id, self.patient_id, self.l_has_cancer, self.r_has_cancer)

class Patient(object):
    def __init__(self, patient_id):
        super(Patient, self).__init__()
        self.patient_id = patient_id
        self.exams = {}

    def has_features(self):
        exam = self.get_latest_exam()
        if exam:
            images = exam.exam_images
            if images:
                return len(images[0].features) > 0
            else:
                return False
        else:
            return False

    def get_latest_exam(self):
        if self.exams:
            return self.exams[max(self.exams)]
        else:
            return None

    def get_previous_exam(self, target_exam_id):
        ret_exam_id = -1
        for exam_id in self.exams:
            if exam_id > ret_exam_id and exam_id < target_exam_id:
                ret_exam_id = exam_id
        if ret_exam_id == -1:
            return None
        else:
            return self.exams[ret_exam_id]

    def has_cancer(self, l_r=None, use_latest=True):
        ret = CANCER_NEGATIVE
        if use_latest:
            if self.exams:
                exam_id = max(self.exams)
                return self.exams[exam_id].has_cancer(l_r)
            else:
                logger.error("not exam for patient %s, CANCER_UNKNOWN", self.patient_id)
                return CANCER_UNKNOWN
        else:
            for exam_id in self.exams:
                r = self.exams[exam_id].has_cancer(l_r)
                if r == CANCER_POSITIVE:
                    return r
                elif r == CANCER_UNKNOWN:
                    ret = r
            return ret


    @property
    def lr_has_cancer(self):
        return self.has_cancer(l_r=None)

    @property
    def l_has_cancer(self):
        return self.has_cancer(l_r='l')

    @property
    def r_has_cancer(self):
        return self.has_cancer(l_r='r')

    def update_exam_result(self, exam_id, l_has_cancer, r_has_cancer, raw_row, implant_now):
        assert exam_id not in self.exams, "{} should not in {}".format(exam_id, self.exams)
        try:
            exam_id = int(exam_id)
        except Exception:
            exam_id = DEFAULT_EXAM_ID
        self.exams[exam_id] = Exam(self.patient_id, exam_id, l_has_cancer, r_has_cancer, raw_row=raw_row, implant_now=implant_now)

    def update_image(self, exam_image):
        assert exam_image.patient_id == self.patient_id
        if exam_image.exam_id in self.exams:
            exam = self.exams[exam_image.exam_id]
        else:
            exam = Exam(self.patient_id, exam_image.exam_id, CANCER_UNKNOWN, CANCER_UNKNOWN, raw_row='', implant_now=(False, False))
            self.exams[exam_image.exam_id] = exam
            #logger.error("unexpected add exam %s for %s (no csv2?)", exam_image.exam_id, self.patient_id)

        exam.add_exam_image(exam_image)
        exam_image.update_implant_now(exam.has_implant_now(exam_image.l_r))

    def get_exam_result(self, exam_id, l_r):
        return self.exams[exam_id].has_cancer(l_r)

    def get_all_images(self, cancer=None):
        ret = []
        for exam_id in self.exams:
            ret = ret + self.exams[exam_id].get_all_images(cancer)
        return ret

    def features_are_from_prediction(self, l_r=None):
        for exam_id in self.exams:
            exam = self.exams[exam_id]
            if not exam.features_are_from_prediction(l_r):
                return False
        return True

    def __repr__(self):
        return "pid={} lr_has_cancer={}, l_has_cancer={}, r_has_cancer={}, exams={}".format(self.patient_id, self.lr_has_cancer,
                                                                               self.l_has_cancer, self.r_has_cancer, self.exams)

    @staticmethod
    def str_to_cancer(s):
        try:
            n = int(s.strip())
        except:
            return CANCER_UNKNOWN

        if n == 1:
            return CANCER_POSITIVE
        elif n == 0:
            return CANCER_NEGATIVE
        else:
            assert False, "bad cancer string {} {}".format(s, n)

    @staticmethod
    def str_to_implantNow(s):
        default = False, False
        try:
            n = int(s.strip())
        except:
            return default
        if n == 1:
            return False, True
        elif n == 2:
            return True, False
        elif n == 4:
            return True, True
        else:
            return default

    @staticmethod
    def get_patients_from_csv(path_csv_crosswalk, path_csv_metadata=None,
                              dcm_json_file=None, features_json_file=None,
                              sample=0, sample_selection=0):
        patients, _ = Patient.get_patients_and_images_from_csv(path_csv_crosswalk, path_csv_metadata,
                                                               dcm_json_file, features_json_file,
                                                               sample, sample_selection)
        return patients


    @staticmethod
    def get_patients_and_images_from_csv(path_csv_crosswalk, path_csv_metadata=None,
                                         dcm_json_file=None, features_json_file=None,
                                         sample=0, sample_selection=0):
        dict_img_name_to_img_info = {}
        patients = {}
        logger.info("open path_csv_crosswalk %s", path_csv_crosswalk)
        counter = 0
        dcm_info = {}
        features = {}

        if dcm_json_file:
            with open(dcm_json_file) as data_file:
                dcm_info = simplejson.load(data_file)
                logger.info("open %s, len=%s", dcm_json_file, len(dcm_info))

        if features_json_file:
            with open(features_json_file) as data_file:
                features = simplejson.load(data_file)
                logger.info("open %s, len=%s", features_json_file, len(features))

        with open(path_csv_crosswalk, 'r') as file_crosswalk:
            reader_crosswalk = csv.reader(file_crosswalk, delimiter='\t')
            filename_idx = -1
            laterality_idx = -1
            view_idx = -1
            image_idx = -1
            exam_idx = -1
            subject_idx = -1
            for row in reader_crosswalk:
                # logger.info("row[%s]: %s", counter, row)
                counter += 1
                if counter == 1:
                    for i, val in enumerate(row):
                        val = val.strip()
                        if val == 'subjectId':
                            subject_idx = i
                        elif val == 'examIndex':
                            exam_idx = i
                        elif val == 'imageIndex':
                            image_idx = i
                        elif val == 'view':
                            view_idx = i
                        elif val == 'laterality':
                            laterality_idx = i
                        elif val == 'filename':
                            filename_idx = i
                        else:
                            logger.error('%s: unexpected %s', i, val)
                    assert filename_idx != -1
                    assert laterality_idx != -1
                    assert view_idx != -1
                    assert subject_idx != -1
                    continue
                try:
                    image_id = row[filename_idx].strip()
                    patient_id = row[subject_idx].strip()
                    exam_id = row[exam_idx].strip() if exam_idx >= 0 else DEFAULT_EXAM_ID
                    l_r = row[laterality_idx].strip()
                    view_type = row[view_idx].strip()
                    new_image = ExamImage(image_id=image_id,
                                          patient_id=patient_id,
                                          exam_id=exam_id,
                                          l_r=l_r,
                                          view_type=view_type,
                                          dcm_info=dcm_info,
                                          features=features,
                                          raw_row=row)
                    dict_img_name_to_img_info[new_image.image_id] = new_image
                    _ = patients.setdefault(patient_id, Patient(patient_id))
                except Exception:
                    logger.exception("invalid %s", row)

        if path_csv_metadata and os.path.isfile(path_csv_metadata):
            logger.info("open path_csv_metadata %s", path_csv_metadata)
            counter = 0
            with open(path_csv_metadata, 'r') as file_metadata:
                reader_metadata = csv.reader(file_metadata, delimiter='\t')
                patient_row_idx = -1
                exam_row_idx = -1
                cancerL_row_idx = -1
                cancerR_row_idx = -1
                implantNow_row_idx = -1
                for row in reader_metadata:
                    # logger.info("row[%s]: %s", counter, row)
                    counter += 1
                    if counter == 1:
                        for i, val in enumerate(row):
                            val = val.strip()
                            if val == 'subjectId':
                                patient_row_idx = i
                            elif val == 'examIndex':
                                exam_row_idx = i
                            elif val == 'cancerL':
                                cancerL_row_idx = i
                            elif val == 'cancerR':
                                cancerR_row_idx = i
                            elif val == 'implantNow':
                                implantNow_row_idx = i
                            else:
                                logger.error('%s: other row %s', i, val)
                        assert patient_row_idx != -1
                        assert exam_row_idx != -1
                        continue
                    try:
                        if cancerL_row_idx >= 0:
                            l_cancer = Patient.str_to_cancer(row[cancerL_row_idx])
                        else:
                            l_cancer = CANCER_UNKNOWN
                        if cancerR_row_idx >= 0:
                            r_cancer = Patient.str_to_cancer(row[cancerR_row_idx])
                        else:
                            r_cancer = CANCER_UNKNOWN
                        if implantNow_row_idx >= 0:
                            implant_now = Patient.str_to_implantNow(row[implantNow_row_idx])
                        else:
                            implant_now = False, False
                        patient_id = row[patient_row_idx].strip()
                        exam_id = row[exam_row_idx].strip()
                        if patient_id in patients:
                            patient = patients[patient_id]
                        else:
                            logger.error("missing patient %s", patient_id)
                            patient = patients.setdefault(patient_id, Patient(patient_id))
                        patient.update_exam_result(exam_id=exam_id, l_has_cancer=l_cancer, r_has_cancer=r_cancer, raw_row=row, implant_now=implant_now)
                    except Exception:
                        logger.exception("invalid %s", row)
        else:
            for patient_id in patients:
                patient = patients[patient_id]
                patient.update_exam_result(exam_id=DEFAULT_EXAM_ID,
                                           l_has_cancer=CANCER_UNKNOWN,
                                           r_has_cancer=CANCER_UNKNOWN,
                                           raw_row=row,
                                           implant_now=(False, False))

        for img_id in dict_img_name_to_img_info:
            try:
                exam_image = dict_img_name_to_img_info[img_id]
                patient = patients[exam_image.patient_id]
                patient.update_image(exam_image=exam_image)
            except Exception:
                logger.exception("cannot add %s %s", img_id, exam_image)

        if sample:
            to_be_deleted = []
            for patient_id in patients:
                if patients[patient_id].has_cancer() == CANCER_POSITIVE:
                    continue
                if int(patient_id) % sample != sample_selection:
                    to_be_deleted.append(patient_id)
            for patient_id in to_be_deleted:
                logger.info("remove patient %s", patient_id)
                del patients[patient_id]
        return patients, dict_img_name_to_img_info


def rewrite_csv(path_csv_crosswalk, new_crosswalk, path_csv_metadata, new_metadata, patients):
    for file, file_new in [(path_csv_crosswalk,new_crosswalk), (path_csv_metadata, new_metadata)]:
        if file is None:
            continue
        with open(file, 'r') as file_crosswalk:
            with open(file_new, 'w') as file_crosswalk_new:
                reader = csv.reader(file_crosswalk, delimiter='\t')
                csv_writer = csv.writer(file_crosswalk_new, delimiter='\t')
                counter = 0
                for row in reader:
                    counter += 1
                    if counter == 1 or row[0].strip() in patients:
                        csv_writer.writerow(row)

    return


def main(args):
    parser = argparse.ArgumentParser(description = "dream csv parser")
    parser.add_argument("--csv1", dest="csv1", required=True, type=str, default=None) #"/metadata/images_crosswalk.tsv")
    parser.add_argument("--csv2", dest="csv2", type=str, default=None) #"/metadata/exams_metada") #SC2 ONLY
    parser.add_argument("--dcm-json-file", type=str, default=None)
    parser.add_argument("--features-json-file", type=str, default=None)
    parser.add_argument('--list-files-output', type=str, default=None, help='Write a image-file list to file')
    parser.add_argument('--list-files-train', type=str, default=None, help='Write a image-file list used for training')
    parser.add_argument('--list-files-predict', type=str, default=None, help='Write a image-file list used for predict')
    parser.add_argument('--list-files-predict-max-size', type=int, default=1000)
    parser.add_argument('--ignore-json', type=str, default=None, help='ignore patients whose is in the json file')
    parser.add_argument('--filter-json', type=str, default=None, help='ignore patients whose is NOT in the json file')
    parser.add_argument('--max-files', type=int, default=1000000)
    parser.add_argument('--show-stats', action='store_true', default=False, help='Show stats')
    parser.add_argument('--rewrite', action='store_true', default=False, help='Rewrite files')
    parser.add_argument('--train-repeat', default=1, type=int, help='Train repeat ratio')
    parser.add_argument('--train-ratio', type=float, default=1.0, help='train_ratio')
    parser.add_argument('--sample', type=int, default=0)
    parser.add_argument('--sample-selection', type=int, default=0)
    parser.add_argument('--for-histogram-only', action='store_true', default=False)

    opts = parser.parse_args(args[1:])

    if opts.csv2 is not None:
        if os.path.isfile(opts.csv2):
            csv2 = opts.csv2
        else:
            logger.error("csv2 %s is not a file, ignore", opts.csv2)
            csv2 = None

    patients = Patient.get_patients_from_csv(opts.csv1, csv2,
                                             opts.dcm_json_file, opts.features_json_file,
                                             opts.sample, opts.sample_selection)

    if opts.show_stats:
        cancer_patients = 0
        normal_patients = 0
        unknown_patients = 0
        cancer_exams = 0
        normal_exams = 0
        unknown_exams = 0
        cancer_images = 0
        normal_images = 0
        unknown_images = 0

        for patient_id in patients:
            patient = patients[patient_id]
            r = patient.has_cancer()
            if r == CANCER_POSITIVE:
                cancer_patients += 1
            elif r == CANCER_NEGATIVE:
                normal_patients += 1
            else:
                unknown_patients += 1
            for exam in patient.exams:
                r = patient.exams[exam].has_cancer()
                if r == CANCER_POSITIVE:
                    cancer_exams += 1
                elif r == CANCER_NEGATIVE:
                    normal_exams += 1
                else:
                    unknown_exams += 1
            for image in patient.get_all_images():
                r = image.has_cancer()
                if r == CANCER_POSITIVE:
                    cancer_images += 1
                elif r == CANCER_NEGATIVE:
                    normal_images += 1
                else:
                    unknown_images += 1

        logger.info("(cancer/no-cancer/unknown) patients %s/%s/%s, exams %s/%s/%s, images %s/%s/%s",
                    cancer_patients, normal_patients, unknown_patients,
                    cancer_exams, normal_exams, unknown_images,
                    cancer_images, normal_images, unknown_images)

    if opts.list_files_output:
        output = open(opts.list_files_output, 'w')
        output_train = open(opts.list_files_train, 'w')
        output_predict = open(opts.list_files_predict+'_part', 'w')
        output_predict_full = open(opts.list_files_predict, 'w')
        ignore_json = {}
        try:
            if opts.ignore_json and os.path.isfile(opts.ignore_json):
                with open(opts.ignore_json, 'r') as ij:
                    ignore_json = simplejson.load(ij)
        except Exception:
            logger.exception("cannot load %s", opts.ignore_json)
        logger.info("ignore_json size is %s from %s", len(ignore_json), opts.ignore_json)

        filter_json = {}
        try:
            if opts.filter_json and os.path.isfile(opts.filter_json):
                with open(opts.filter_json, 'r') as ij:
                    filter_json = simplejson.load(ij)
        except Exception:
            logger.exception("cannot load %s", opts.filter_json)
        logger.info("filter_json size is %s from %s", len(filter_json), opts.filter_json)

        num_train = 0
        num_predict = 0

        # First decide which patients would be used for predict/train
        predict_patient_dict = {}
        for condition in [CANCER_POSITIVE, CANCER_NEGATIVE, CANCER_UNKNOWN]:
            for patient_id in patients:
                patient = patients[patient_id]
                if patient.has_cancer() == condition:
                    if num_train >= num_predict * opts.train_ratio:
                        pick_for_train = 0
                        num_predict += 1
                        predict_patient_dict[patient_id] = 1
                    else:
                        pick_for_train = 1
                        num_train += 1
                    print("patient", patient_id, pick_for_train, condition)
        tot_images = 0
        print("XXXXXXXX Now showing image informations XXXXXXXXXXX")
        train_pos_list = []
        train_neg_list = []
        list_files_predict_full_size = 0
        list_files_predict_size = 0
        patients_selected_for_pred_cancer = {}
        patients_selected_for_pred_normal = {}
        patients_selected_for_train_cancer = {}
        patients_selected_for_train_normal = {}

        for condition in [CANCER_POSITIVE, CANCER_NEGATIVE, CANCER_UNKNOWN]:
            for patient_id in patients:
                patient = patients[patient_id]
                if patient.has_cancer() == condition:
                    images = patient.get_all_images()
                    for image in images:
                        if tot_images >= opts.max_files:
                            break
                        if opts.for_histogram_only:
                            if image.has_cancer() != CANCER_POSITIVE or image.view_type.lower() != 'cc':
                                continue
                        if image.image_id in ignore_json:
                            logger.info("%s of %s already in ignore_json, skip", image.image_id, patient_id)
                            continue
                        if filter_json and (image.image_id not in filter_json):
                            continue
                        tot_images += 1
                        outs = "{}\t{}\n".format(image.image_id, image.has_cancer())
                        print("patient", patient_id, predict_patient_dict.has_key(patient_id), image.image_id, image.has_cancer())
                        output.write(outs)

                        if condition == CANCER_UNKNOWN:
                            continue
                        if predict_patient_dict.has_key(patient_id):
                            output_predict_full.write(outs)
                            list_files_predict_full_size += 1
                            if condition == CANCER_POSITIVE:
                                patients_selected_for_pred_cancer[patient_id] = 1
                            else:
                                patients_selected_for_pred_normal[patient_id] = 1
                            # zhe, to have clean negatives for training!! XXXXXX
                            if condition == CANCER_POSITIVE and image.has_cancer() != CANCER_POSITIVE:
                                continue
                            if opts.list_files_predict_max_size > list_files_predict_size:
                                list_files_predict_size += 1
                                output_predict.write(outs)
                        else:
                            # zhe, to have clean negatives for training!! XXXXXX
                            if condition == CANCER_POSITIVE and image.has_cancer() != CANCER_POSITIVE:
                                continue
                            if image.has_cancer():
                                patients_selected_for_train_cancer[patient_id] = 1
                                train_pos_list.append((image.image_id, image.has_cancer()))
                            else:
                                patients_selected_for_train_normal[patient_id] = 1
                                train_neg_list.append((image.image_id, image.has_cancer()))

        # Now write out the train data in a balanced way!
        idx = 0
        pidx = 0
        import random
        random.seed(a=1)
        if len(train_pos_list) > 0:
            max_rotate = len(train_neg_list) // len(train_pos_list) * opts.train_repeat
            if max_rotate < 1:
                max_rotate = 1
            for _ in range(opts.train_repeat):
                for image_id, has_cancer in train_neg_list:
                    rand1 = random.randint(0, max_rotate - 1)
                    rand2 = random.randint(0, max_rotate - 1)
                    outs = "{}={}\t{}\n".format(image_id, rand1, has_cancer)
                    output_train.write(outs)

                    if len(train_pos_list) > 0:
                        pos_obj = train_pos_list[idx % len(train_pos_list)]
                        outs = "{}={}\t{}\n".format(pos_obj[0], rand2, pos_obj[1])
                        output_train.write(outs)
                        pidx += 1
                    idx += 1
        else:
            logger.info("no train list")
        logger.info("list_files_train_size=%s real_tranin_positive_size=%s real_train_negative_size=%s list_files_predict_full_size=%s list_files_predict_size=%s",
                    idx + pidx, len(train_pos_list), len(train_neg_list),
                    list_files_predict_full_size, list_files_predict_size)
        logger.info("total_patients=%s patient_pred_cancer=%s patient_pred_normal=%s patient_train_cancer=%s patient_train_normal=%s",
                    len(patients),
                    len(patients_selected_for_pred_cancer),
                    len(patients_selected_for_pred_normal),
                    len(patients_selected_for_train_cancer),
                    len(patients_selected_for_train_normal)
                    )
        output.close()
        output_train.close()
        output_predict.close()
        output_predict_full.close()

    if opts.rewrite:
        rewrite_csv(opts.csv1, opts.csv1 + '.new', csv2, csv2 + '.new', patients)

    return 0

if __name__ == '__main__':
    main(sys.argv)
