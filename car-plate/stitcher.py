#!/usr/bin/env python
import numpy as np

class Stitcher:
    def __init__ (self, image, margin, patch_h, patch_w=0):
        if patch_w == 0:
            patch_w = patch_h
        N, H, W, C = image.shape
        assert N == 1
        grid = []
        grid_1 = []

        h_segments = Stitcher.overlap_split(H, patch_h, margin)
        w_segments = Stitcher.overlap_split(W, patch_w, margin)

        for h in h_segments:
            for w in w_segments:
                grid.append((h, w))

        if margin == 0:
            bh = (H + patch_h - 1)  // patch_h
            bw = (W + patch_w - 1) // patch_w
            for h in range(bh):
                for w in range(bw):
                    grid_1.append(((h * patch_h, min(H, (h+1)*patch_h), 0, 0), (w * patch_w, min(W, (w+1)*patch_w), 0, 0)))
                    assert grid[len(grid_1) - 1] == grid_1[len(grid_1) - 1]

        self.image = image
        self.patch_h = patch_h
        self.patch_w = patch_w
        self.grid = grid
        pass

    @staticmethod
    def overlap_split(whole, segment, margin):
        end = 0
        assert segment > margin * 2
        segmentations = []
        while True:
            if end == 0:
                start = 0
                real_start = 0
            else:
                start = end - margin * 2
                real_start = start + margin
            end = start + segment
            if end >= whole:
                end = whole
                real_end = end
            else:
                real_end = end - margin
            segmentations.append((start, end, real_start - start, end - real_end))
            if real_end == whole:
                break
        return segmentations

    def split (self):
        _, _, _, C = self.image.shape
        patches = np.zeros((len(self.grid), self.patch_h, self.patch_w, C))
        for i, ((h1, h2, ha, hb), (w1, w2, wa, wb)) in enumerate(self.grid):
            patches[i,:(h2-h1),:(w2-w1),:] = self.image[0,h1:h2,w1:w2,:]
        return patches

    def stitch (self, patches):
        _, H, W, _ = self.image.shape
        _, _, _, C = patches.shape
        image = np.zeros((1, H, W, C), dtype=patches.dtype)
        for i, ((h1, h2, ha, hb), (w1, w2, wa, wb)) in enumerate(self.grid):
            image[0,h1+ha:h2-hb,w1+wa:w2-wb,:] = patches[i,ha:(h2-h1-hb),wa:(w2-w1-wb),:]
        return image

if __name__ == '__main__':
    for _ in range(100):
        s = np.random.randint(500, size=2)
        s += 1
        image = np.random.randint(255, size=s)
        if len(image.shape) == 2:
            image = image.reshape((1,) + image.shape + (1,))
        else:
            image = image.reshape((1,) + image.shape)

        for margin in range(31):
            for ps in xrange(margin * 3 + 1, 100):
                stitcher = Stitcher(image, margin=margin, patch_h=ps)
                print("verify size=%s patch=%s margin=%s" % (s, ps, margin))
                assert (image == stitcher.stitch(stitcher.split())).all()

