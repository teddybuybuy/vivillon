import argparse
import errno
import os
import subprocess
import sys
import logging
import cv2
import numpy
import tensorflow as tf
import bounding_box as bb
import numpy as np
import time
import threading
import concurrent.futures
from scipy.stats import threshold

FLAGS = None

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

BHW = 20  # bbox half width
BHH = 20  # bbox half height

class SimpleProcessor(object):
    def __init__(self, sess):
        super(SimpleProcessor, self).__init__()
        self.sess = sess
        if self.sess is None:
            return

        self.X = tf.placeholder(tf.float32, shape=(None, None, None, 1), name="images")
        gaussian = bb.matlab_style_gauss2D((BHH * 2 + 1, BHW * 2 + 1), sigma=5)
        logger.info("gshape %s", gaussian.shape)
        filters = np.ones(shape=gaussian.shape + (1, 1))  # input channel, output channel
        #filters[:, :, 0, 0] = gaussian
        ff = tf.constant(filters, name='filters', dtype="float32")
        mul_const = tf.constant([0.3])
        Y = tf.multiply(self.X, mul_const)
        #Y = tf.nn.relu6(Y)
        self.Z = tf.nn.conv2d(Y, ff, [1, 1, 1, 1], padding="SAME")
        self.lock = threading.Lock()

    def calci(self, filename, write_calci=None, has_cancer=None):
        time_start = time.time()
        image = cv2.imread(filename, -1)
        logger.info("read %s cost %.2f. shape=%s", filename, time.time() - time_start, image.shape)
        # image = cv2.resize(image, None, None, 0.25, 0.25)
        shape = (1,) + image.shape + (1,) # batch 1.
        image = np.reshape(image, shape)
        with self.lock:
            conv = self.sess.run(self.Z, feed_dict={self.X: image})
        score = numpy.max(conv)
        filesize = os.path.getsize(filename)
        if write_calci:
            resize_factor = 4
            BS = BHW * resize_factor
            f, e = os.path.splitext(filename)
            f = f.rsplit('_', 1)[0]
            org_filename = f + '.tiff'
            outf = os.path.split(f)[1]
            if has_cancer == 1:
                outf = 'c_' + outf
            fileout = os.path.join(write_calci, outf + '-bb.jpg')
            origin = cv2.imread(org_filename, -1)
            max_i = np.argmax(conv)
            center = np.unravel_index(max_i, shape)
            logger.info("%s, %s %s", max_i, shape, center)
            a = center[2] * resize_factor
            b = center[1] * resize_factor
            origin /= 16

            cv2.rectangle(origin, (a - BS, b - BS), (a + BS, b + BS), 255, 2)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(origin, "%.2f" % score, (100, 100), font, 4, (255), 3)
            cv2.imwrite(fileout, origin)
            logger.info("write %s, %s", fileout, center)
        return [score, filesize]

    def bravg(self, filename):
        # disable bravg, since it seems doesn't have signal. zheng
        return [0, 0]

        image = cv2.imread(filename, -1)
        th = 10
        sum = (image > th).sum()
        return [numpy.average(image), sum]

def predict_one(simple_processor, item, predict_type, options):
    file = item[0].strip()
    if not os.path.isfile(file):
        d, f = os.path.split(file)
        base, _ = os.path.split(FLAGS.src_csv)
        file = os.path.join(base, f)

    has_cancer = int(item[1].strip())
    if predict_type == 'calci':
        score = simple_processor.calci(file, options['write_calci'], has_cancer)
    elif predict_type == 'bravg':
        score = simple_processor.bravg(file)
    else:
        assert False, 'invalid predict_type {}'.format(predict_type)
    return file, has_cancer, score

def do_predict(src_csv, result_csv, predict_type, sess, options):
    if not result_csv:
        logger.info("no output, use input %s as output", src_csv)
        result_csv = src_csv

    simple_processor = SimpleProcessor(sess)

    with open(src_csv) as f:
        filelist = f.readlines()

    logger.info("parse %s len=%s", result_csv, len(filelist))

    filelist = [x.split('\t') for x in filelist]

    results = []
    total_cancer_n = total_normal_n = 0
    score_cancer = score_normal = None

    with concurrent.futures.ThreadPoolExecutor(max_workers=FLAGS.thread) as executor:
        # Start the load operations and mark each future with its URL
        future_to_predict = {executor.submit(predict_one, simple_processor, item, predict_type, options): (idx, item) for idx, item in enumerate(filelist)}
        for future in concurrent.futures.as_completed(future_to_predict):
            idx, item = future_to_predict[future]
            try:
                file, has_cancer, score = future.result()
                if has_cancer:
                    total_cancer_n += 1
                    if score_cancer is None:
                        score_cancer = np.zeros((len(score))).astype(np.float)
                    score_cancer += score
                else:
                    total_normal_n += 1
                    if score_normal is None:
                        score_normal = np.zeros((len(score))).astype(np.float)
                    score_normal += score
                cancer_average = score_cancer / total_cancer_n if total_cancer_n > 0 else np.ones((len(score)))
                normal_average = score_normal / total_normal_n if total_normal_n > 0 else np.ones((len(score)))
                logger.info("%s %s[%s]: cancer=%s score=%s (%s vs %s = %s)",
                            predict_type, file, idx, has_cancer,
                            score, cancer_average, normal_average, cancer_average / normal_average)
                results.append((file, has_cancer, score))
            except Exception as exc:
                logger.exception('%s generated an exception: %s', item, exc)

    with open(result_csv, "w") as outfile:
        # outfile.write("fname\tcancer\n")
        for item in results:
            s = ""
            for score in item[2]:
                s = s + "\t" + str(score)
            outfile.write("{}\t{}{}\n".format(item[0], item[1], s))
            logger.info("printfeature %s\t%s\t%s%s", predict_type, item[0], item[1], s)
    logger.info("write to %s len=%s", result_csv, len(results))

    return total_normal_n + total_cancer_n

def main(_):
    time_start = time.time()
    options = {'write_calci': FLAGS.write_calci}
    if FLAGS.predict_type in ['calci']:
        logger.info("prepare tf")
        with tf.Graph().as_default() as graph:
            config = tf.ConfigProto()
            config.gpu_options.allow_growth=True
            with tf.Session(config=config) as sess:
                total = do_predict(src_csv=FLAGS.src_csv,  result_csv=FLAGS.result_csv,
                                   predict_type=FLAGS.predict_type, sess=sess, options=options)
    else:
        total = do_predict(src_csv=FLAGS.src_csv, result_csv=FLAGS.result_csv,
                           predict_type=FLAGS.predict_type, sess=None, options=options)
    time_diff = time.time() - time_start
    avg = time_diff / total if total > 0 else 0.0
    logger.info("finish %s. batch-size=%s cost %.2f seconds avg_cost %.3f", FLAGS.predict_type, total, time_diff, avg)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="simple predict")
    parser.add_argument("--src-csv", required=True)
    parser.add_argument("--result-csv", default=None)
    parser.add_argument("--predict-type", required=True)
    parser.add_argument('--thread', default=0, type=int, help='number of threads')
    parser.add_argument("--write-calci", default=None, type=str)

    FLAGS = parser.parse_args()
    if FLAGS.thread == 0:
        import multiprocessing
        if multiprocessing.cpu_count() >= 8:
            FLAGS.thread = multiprocessing.cpu_count() / 2
        else:
            FLAGS.thread = 1
        logger.info("uses threads=%s. cpu_count=%s", FLAGS.thread, multiprocessing.cpu_count())

    tf.app.run()
