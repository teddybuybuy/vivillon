#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os,sys
import numpy as np
import tensorflow as tf
import dicom
import glob
import simplejson
import logging
from patients_csv_parser import Patient, ExamImage
import csv
import random
import xgbooster
import pandas as pd

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

flags = tf.app.flags
FLAGS = flags.FLAGS

flags.DEFINE_string('image_crosswalk_csv', '/metadata/images_crosswalk.tsv', '')
flags.DEFINE_string('exams_metadata_csv', '/metadata/exams_metadata.tsv', '')
flags.DEFINE_string('dcm_info', '/scratch/dcm_info.json', '') #inference_scratch
flags.DEFINE_string('features_result', '/scratch/features_result.json', '') #inference_scratch
flags.DEFINE_string('model', 'xgb_model.dat', '')

def main (_):
  image_crosswalk = FLAGS.image_crosswalk_csv
  exams_metadata = FLAGS.exams_metadata_csv
  dcm_info_file = FLAGS.dcm_info
  features_result_file = FLAGS.features_result

  full_training_set = None
  if exams_metadata == "":  # sc1
      full_training_set = xgbooster.ParseDcmInfo(dcm_info_file, image_crosswalk, exams_metadata, features_result_file, for_sc1=True)
  else:
    per_breast_metadata= xgbooster.ParseExamMeta(exams_metadata)
    #print("per_breast_metadata", len(per_breast_metadata))
    per_breast_dcm = xgbooster.ParseDcmInfo(dcm_info_file, image_crosswalk, exams_metadata, features_result_file, for_sc1=False)
    #print("per_breast_dcm", len(per_breast_dcm))
    full_training_set = pd.merge(per_breast_metadata, per_breast_dcm, on=['subjectId', 'laterality'])

  print("full_training_set", len(full_training_set))
  predictors = [x for x in full_training_set.columns if x not in ['cancer', 'subjectId', 'laterality', 'use_prediction']]
  XTest = full_training_set[predictors]
  print("training samples:%d" % len(XTest))
  xgb = xgbooster.XGBBooster(FLAGS.model)  # new model
  Y, Y_prob = xgb.Predict(XTest)

  try:
    if not os.path.exists('/output'):
      os.makedirs('/output', mode=0777)
    else:
      print("already have /output dir")
  except Exception:
    pass

  with open('/output/predictions.tsv', 'wb') as output:
    w = csv.writer(output, delimiter='\t')
    w.writerow(["subjectId", "laterality", "confidence"])
    for index, row in full_training_set.iterrows():
      w.writerow([row['subjectId'], row['laterality'].upper(), Y_prob[index]])

if __name__ == '__main__':
  tf.app.run()

