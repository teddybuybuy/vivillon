import cv2
import logging
import numpy as np
import os, sys
from scipy.stats import threshold
from scipy import ndimage

logger = logging.getLogger(__name__)

def write_prob_file(file_prefix, file, prob, label, size, loss = None, origin=None, confidences=None,
                    bboxs=None, BHH=None, BHW=None, resize_ratio=None):
    label_values = []
    biggest = (0, 0)
    if label is not None:
        label = label[0]
        c = label
        while True:
            max = np.max(c)
            if max == 0:
                break
            else:
                v1 = np.sum(c)
                # erase max
                c = threshold(c, threshmin=0, threshmax=max-1, newval=0)
                v2 = np.sum(c)
                n = (int(max), (v1 - v2) / max)
                label_values.append(n)
                # find biggest area
                if n[1] > biggest[1]:
                    biggest = n
    # process prob
    if prob.shape[3] != 2:
        prob_sums = []
        for i in range(0, prob.shape[3]):
            a = prob[:,:,:,i]
            prob_sums.append(int(np.sum(a))) 
        logger.debug("multi channels: biggest=%s, prob_sums=%s, shape=%s loss=%.4f", biggest, prob_sums, prob.shape, loss)
        prob = prob[:,:,:,biggest[0]]
        if biggest[0] == 0:
            # if no label, just show 1 - non-active
            prob = (1 - prob)
    else:
        prob = prob[:,:,:,1]
    prob = prob[0]
    prob = cv2.resize(prob, (size[1], size[0]))
    prob = prob * 255
    write_orig = False

    if origin is not None:
        # handle +8bit image
        origin = reduce_image_to_8_bit(origin[0])
        if origin.shape[2] == 1:
            origin = cv2.cvtColor(origin, cv2.COLOR_GRAY2RGB)

        font = cv2.FONT_HERSHEY_SIMPLEX
        if confidences is not None and bboxs is not None:
            for i in range(len(bboxs)):
                cv2.rectangle(origin, (bboxs[i][1]-BHW, bboxs[i][0]-BHH),
                              (bboxs[i][1]+BHW, bboxs[i][0]+BHH), (255,0,0), 1)
                cv2.putText(origin, "%.2f" % confidences[i],
                            (bboxs[i][1] - BHW, bboxs[i][0] - BHH - 20),
                            font, 0.4, (0,0,255), 1)

        try:
            prob = np.concatenate((origin, cv2.cvtColor(prob, cv2.COLOR_GRAY2RGB)), axis=1)
            write_orig = False
        except Exception as e:
            logger.debug('cannot concat orig file %s', str(e))
            write_orig = True

        if loss is not None:
            cv2.putText(prob, "%.4f" % loss,(25,25), font, 1,(255,255,255),1)

        if label is not None:
            try:
                if biggest[0] != 0:
                    # erase everything otherthan biggest[0]
                    label = threshold(label, threshmin=biggest[0], threshmax=biggest[0], newval=0)
                    m = biggest[0]
                else:
                    m = 1
                prob = np.concatenate((prob, cv2.cvtColor(label * (255 / m), cv2.COLOR_GRAY2RGB)), axis=1)
            except Exception:
                logger.exception('cannot concat label file')
    if not resize_ratio is None:
        logger.info("resize prob result image %s", resize_ratio)
        prob = cv2.resize(prob, None, None, resize_ratio, resize_ratio)
    cv2.imwrite(file_prefix + '.png', prob)
    if write_orig and origin is not None:
        cv2.imwrite(file_prefix + '.org.jpg', origin)

def bbox2(img):
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]

    return rmin, rmax, cmin, cmax

def remove_dream_rect_label(image, reduce_size=True, margin=50):
    mask = threshold(image, threshmin=0, threshmax=0, newval=4095)
    #mask = threshold(mask, threshmin=th + 1, threshmax=4095, newval=0)

    # erosion and dilation to get ride of noises
    #mask = mask > 0
    #mask = ndimage.binary_erosion(mask, structure=np.ones((10, 10)))
    #mask = ndimage.binary_dilation(mask, structure=np.ones((10, 10)))

    # find largest connected region.
    s = ndimage.generate_binary_structure(2, 2)  # iterate structure
    labeled_array, numpatches = ndimage.label(mask, s)  # labeling
    if numpatches == 0:
        logger.error("get 0 patches. !!!!")
        return image
    sizes = ndimage.sum(mask, labeled_array, range(1, numpatches + 1))
    # To get the indices of all the min/max patches. Is this the correct label id?
    map = np.where(sizes == sizes.max())[0] + 1

    # inside the largest, respecitively the smallest labeled patches with values
    max_index = np.zeros(numpatches + 1, np.uint8)
    # logger.info("max_idx a shape %s %s", max_index.shape, numpatches)
    max_index[map] = 1
    # logger.info("max_idx b shape %s %s", max_index.shape, map.shape)
    max_feature = max_index[labeled_array]
    # logger.info("max_feature shape %s %s", max_feature.shape, image.shape)
    output = max_feature * image
    if reduce_size:
        rmin, rmax, cmin, cmax = bbox2(output)
        h, w = output.shape
        assert rmax < h and cmax < w, "{} h={}, {}, w={}".format(rmax, h, cmax, w)

        if rmin > margin:
            rmin -= margin
        else:
            rmin = 0

        if cmin > margin:
            cmin -= margin
        else:
            cmin = 0

        if rmax + margin > h:
            rmax = h
        else:
            rmax += margin

        if cmax + margin > w:
            cmax = w
        else:
            cmax += margin
        return output[rmin:rmax, cmin:cmax]
    else:
        return output

def remove_dream_rect_label2(image):
    lines = []
    run_len_thresh = 30
    max_img_size = 10000
    logging.info("try remove dream label")
    for i in xrange(image.shape[0]):
        run_len = -max_img_size
        pre_val = 100
        for j in xrange(image.shape[1]):
            pixel = image.item(i, j)
            if pixel == pre_val:
                run_len += 1
            else:
                if pixel < pre_val:
                    if run_len > run_len_thresh:
                        if pixel <= 10 and pre_val > 3084:
                            # logger.error("%s: [%s %s] %s %s", i, j - run_len, run_len, pixel, pre_val)
                            lines.append((i, j - run_len, run_len, pre_val))
                        # print pre_val, pixel
                        pass
                    run_len = -max_img_size
                else:
                    run_len = 1
                pre_val = pixel
    min_x, max_x, min_y, max_y = (max_img_size, 0, max_img_size, 0)
    if lines:
        for line in lines:
            if min_x > line[1]:
                min_x = line[1]
            if max_x < line[1] + line[2] - 1:
                max_x = line[1] + line[2] - 1
            if min_y > line[0]:
                min_y = line[0]
            if max_y < line[0]:
                max_y = line[0]

        logging.info("remove dream label (%s, %s, %s, %s)", min_x, min_y, max_x - min_x + 1, max_y - min_y + 1)

        fill_pix = 100

        for p in xrange(min_x, max_x + 1):
            for q in xrange(min_y, max_y + 1):
                image[q, p] = fill_pix
                pass

    return image


def reduce_image_to_8_bit(image):
    # handle +8bit image
    origin_max = max(np.amax(image), 255)
    origin_adjust = origin_max / 255
    image /= origin_adjust
    return image

def load_tsv(fname, indexed_by):
    ''' Load a tsv file (fname), output a dictionary keyed on indexed_by,
       and then each entry is keyed by the column name.
    '''
    header_read = False
    out_dict = {}
    for line in open(fname):
        items = line.strip().split('\t')
        if header_read == False:
            table_entries = items
            match_index = table_entries.index(indexed_by)
            header_read = True
        else:
            out_dict[items[match_index]] = {}
            for idx, val in enumerate(items):
                if idx == match_index:
                    continue
                out_dict[items[match_index]][table_entries[idx]] = convertString(val)
    return out_dict

from ast import literal_eval
def convertString(s):
    '''
    This function will try to convert a string literal to a number or a bool
    such that '1.0' and '1' will both return 1.

    The point of this is to ensure that '1.0' and '1' return as int(1) and that
    'False' and 'True' are returned as bools not numbers.

    This is useful for generating text that may contain numbers for diff
    purposes.  For example you may want to dump two XML documents to text files
    then do a diff.  In this case you would want <blah value='1.0'/> to match
    <blah value='1'/>.

    The solution for me is to convert the 1.0 to 1 so that diff doesn't see a
    difference.

    If s doesn't evaluate to a literal then s will simply be returned UNLESS the
    literal is a float with no fractional part.  (i.e. 1.0 will become 1)

    If s evaluates to float or a float literal (i.e. '1.1') then a float will be
    returned if and only if the float has no fractional part.

    if s evaluates as a valid literal then the literal will be returned. (e.g.
    '1' will become 1 and 'False' will become False)
    '''


    if isinstance(s, str):
        # It's a string.  Does it represnt a literal?
        #
        try:
            val = literal_eval(s)
        except:
            # s doesn't represnt any sort of literal so no conversion will be
            # done.
            #
            val = s
    else:
        # It's already something other than a string
        #
        val = s

    ##
    # Is the float actually an int? (i.e. is the float 1.0 ?)
    #
    if isinstance(val, float):
        if val.is_integer():
            return int(val)

        # It really is a float
        return val

    return val

if __name__ == "__main__":
    out_dict = load_tsv(sys.argv[1], sys.argv[2])
    print out_dict
