import argparse
import csv
import errno
import os
import random
import subprocess
import sys
import time
import logging
import simplejson
import json
import psutil

import patients_csv_parser
import pandas

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

#########################################
# do not touch this without ask zheng ###
#########################################
RUN_MODE = None
BATCH_SIZE = 600
REDUCED_BATCH_SIZE=350
MAX_PROCESSED_NUM = 0
RANDOM_CRASH = False

###################
#RUN_MODE = 'QUICK_TEST'
#RUN_MODE = 'PERF'
#RUN_MODE = 'TEST'
###################

try:
    WALLTIME_MINUTES = int(os.environ["WALLTIME_MINUTES"])
    if WALLTIME_MINUTES >= 10 and WALLTIME_MINUTES <= 30:
        RUN_MODE = "EXPRESS_INFER"
        pass
except Exception:
    logger.exception("%s", os.environ)

if RUN_MODE == 'QUICK_TEST':
    BATCH_SIZE = 5
    MAX_PROCESSED_NUM = 12
    RANDOM_CRASH = True
elif RUN_MODE == 'PERF':
    BATCH_SIZE = 100
    MAX_PROCESSED_NUM = 6000
    RANDOM_CRASH = False
elif RUN_MODE == 'TEST':
    BATCH_SIZE = 20
    MAX_PROCESSED_NUM = 40
    RANDOM_CRASH = False
elif RUN_MODE == "EXPRESS_INFER":
    BATCH_SIZE = 600
    MAX_PROCESSED_NUM = 1000
    RANDOM_CRASH = False

MAX_RETRY = 2
base_dir_path = os.path.dirname(os.path.realpath(__file__))
parent_dir_path = os.path.dirname(base_dir_path)
preprocess_sh = os.path.join(parent_dir_path, 'preprocess.sh')
train_sh = os.path.join(parent_dir_path, 'train.sh')

def _mkdir_p(path):
    if not path:
        return
    try:
        os.makedirs(path, mode=0777)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def _cleaning_dir(path):
    import shutil
    try:
        if os.path.exists(path):
            shutil.rmtree(path)
    except Exception:
        logger.exception("cannot rm dir %s", path)
        pass


def _get_feature_from_file(filename, pred_type):
    with open(filename) as f:
        filelist = f.readlines()
        logger.info("use %s len=%s", filename, len(filelist))
        filelist = [x.split('\t') for x in filelist]

    feature = {}
    for item in filelist:
        feature_len = len(item) - 2
        try:
            file = item[0].strip()
            _, file = os.path.split(file)
            filebase, _ = os.path.splitext(file)
            key = filebase.split('_')[0]
            if feature_len > 1:
                value2 = []
                for i in range(feature_len):
                    value2.append(float(item[2 + i].strip()))
                value2 = json.dumps(value2)
            else:
                value2 = str(float(item[2].strip()))
            feature[key] = value2
            logger.info("%s %s %s", pred_type, key, value2)
        except Exception:
            logger.exception("item is %s", item)

    return feature

def _update_features(feature, features, pred_type):
    for image_id in feature:
        r = {pred_type: feature[image_id]}
        if image_id in features:
            features[image_id].update(r)
        else:
            features[image_id] = r
            features[image_id].update({"prediction": True})


def get_features_from_preprocess_list_files(process_list_file_dir):
    features = {}
    for pred_type, predict_list, model in [('calci', 'calci-result.csv', ''),
                                           ('mass', 'crop_meta.tsv', ''),
                                           ('glob', 'slim_glob.json.tsv', ''),
                                           ('bravg', 'bravg.csv', '')]:
        if model:
            pass
        else:
            f = os.path.join(process_list_file_dir, predict_list)
            feature = _get_feature_from_file(filename=f, pred_type=pred_type)

        _update_features(features=features, feature=feature, pred_type=pred_type)
    return features

def _call_nvidia_smi():
    try:
        p = subprocess.Popen('nvidia-smi', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        logger.info("nvidia output: ret=%s out=%s", p.returncode, out)
        logger.info("preprocess done: err=%s", err)
    except Exception:
        logger.exception("cannot popen")

def _process_batch_wrapper2(images_batch, csv_first_row, models_dir,
                            meta_dir, files_dir, process_list_file_dir,
                            preprocess_dir, scratch_dir, result_json, pro):
    _call_nvidia_smi()

    return _process_batch_wrapper(images_batch, csv_first_row, models_dir,
                                  meta_dir, files_dir, process_list_file_dir,
                                  preprocess_dir, scratch_dir, result_json, pro)

def _process_batch_wrapper(images_batch, csv_first_row, models_dir,
                           meta_dir, files_dir, process_list_file_dir,
                           preprocess_dir, scratch_dir, result_json, pro):
    images_batch_len = len(images_batch)
    if images_batch_len == 0:
        return {}, {}
    dcm_info, features = _process_batch(images_batch, csv_first_row, models_dir,
                                        meta_dir, files_dir, process_list_file_dir,
                                        preprocess_dir, scratch_dir, result_json, pro)

    if len(dcm_info) == 0 and len(features) == 0:
        if images_batch_len > 1:
            # split the batch to retry
            logger.info("split batch %s to retry", images_batch_len)
            batch_0 = images_batch[:images_batch_len / 2]
            batch_1 = images_batch[images_batch_len / 2:]
            for ba in [batch_0, batch_1]:
                dcm_info_b, features_b = _process_batch_wrapper(ba, csv_first_row, models_dir,
                                                                meta_dir, files_dir, process_list_file_dir,
                                                                preprocess_dir, scratch_dir, result_json,
                                                                pro)
                dcm_info.update(dcm_info_b)
                features.update(features_b)
                logger.info("got result after retry. batch=%s, feature=%s/%s dcm=%s/%s", len(ba), len(features_b), len(features), len(dcm_info_b), len(dcm_info))
        else:
            logger.warning("no result for %s even after retry", images_batch)

    for dir in [meta_dir, preprocess_dir, scratch_dir]:
        #logger.info("XXXXX Stopping now for debugging.")
        #sys.exit(1)
        _cleaning_dir(dir)

    if images_batch_len != len(features):
        logger.warning("not all images processed successfully batch=%s vs features=%s vs dcm_info=%s", images_batch_len, len(features), len(dcm_info))

    return dcm_info, features

def _process_batch(images_batch, csv_first_row, models_dir,
                   meta_dir, files_dir, process_list_file_dir,
                   preprocess_dir, scratch_dir, result_json, pro):
    if len(images_batch) == 0:
        return {}, {}

    try:
        os.remove(result_json)
    except Exception:
        pass

    for dir in [meta_dir, preprocess_dir, scratch_dir]:
        _mkdir_p(dir)

    crosswalk = os.path.join(meta_dir, "images_crosswalk.tsv")
    images_ids = []
    with open(crosswalk, 'w') as file_crosswalk_new:
        csv_writer = csv.writer(file_crosswalk_new, delimiter='\t')
        csv_writer.writerow(csv_first_row)
        for image in images_batch:
            csv_writer.writerow(image.raw_row)
            images_ids.append(image.image_id)

    logger.info("process batch_size=%s %s in %s", len(images_ids), images_ids, crosswalk)

    # invoke preprocessing
    max_retry = MAX_RETRY
    if len(images_batch) == 1:
        max_retry = max_retry * 4
    retry_left = max_retry
    while retry_left > 0:
        retry_left -= 1
        cmd = [preprocess_sh, meta_dir, files_dir, preprocess_dir, 'no_ignore_patients_file', 'partial']
        logger.info("cmd=%s, batch_size=%s, retry_left=%s, %s", cmd, len(images_batch), retry_left, pro)
        try:
            my_env = os.environ
            try:
                if RANDOM_CRASH:
                    my_env["RANDOM_CRASH"] = '--random-crash'
            except Exception:
                logger.exception("")
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
            out, err = p.communicate()
            logger.info("preprocess done: ret=%s out=%s", p.returncode, out)
            logger.info("preprocess done: err=%s", err)
            if p.returncode == 0:
                break
        except Exception:
            logger.exception("cannot popen")
    else:
        logger.warning("failed after retry, batch_size=%s", len(images_batch))
        return {}, {}

    features = {}
    # invoke trainings
    #for pred_type, predict_list, model in [('calci', 'calci-result.csv', 'calci/train'), ('mass', 'crop_meta.tsv', 'mass/train'), ('overview', 'resize_400.csv', 'resize/train')]:
    for pred_type, predict_list, model in [('calci', 'calci-result.csv', ''),
                                           ('mass', 'crop_meta.tsv', ''),
                                           ('glob', 'slim_glob.json.tsv', ''),
                                           ('bravg', 'bravg.csv', '')]:
        if model:
            cmd = [train_sh, predict_list, meta_dir,
                   'not used',
                   preprocess_dir, scratch_dir, result_json, os.path.join(models_dir, model)]
            logger.info("cmd=%s", cmd)
            import time
            # time.sleep(1000000)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            logger.info("process done: ret=%s out=%s", p.returncode, out)
            logger.info("process done: err=%s", err)
            assert p.returncode == 0

            try:
                with open(result_json, 'r') as data:
                    feature = simplejson.load(data)
            except Exception:
                logger.exception("cannot process %s", result_json)
        else:
            f = os.path.join(process_list_file_dir, predict_list)
            feature = _get_feature_from_file(filename=f, pred_type=pred_type)

        _update_features(features=features, feature=feature, pred_type=pred_type)


    dcm_info_from_prep = os.path.join(preprocess_dir, "dataset", "dcm_info.json")
    with open(dcm_info_from_prep, 'r') as data:
        dcm_info = simplejson.load(data)

    if (len(images_batch) != len(dcm_info) or len(images_batch) != len(features)):
        logger.warning("missing features batch_size=%s, features=%s, dcm_info=%s %s", len(images_batch), len(features), len(dcm_info))

    return dcm_info, features

def main(args):
    parser = argparse.ArgumentParser(description="process_patients_images.py")
    parser.add_argument("--csv1", dest="csv1", type=str, default="/metadata/images_crosswalk.tsv")
    parser.add_argument("--csv2", dest="csv2", type=str, default="/metadata/exams_metadata.tsv")
    parser.add_argument("--inference-data", type=str, default="/inferenceData/")
    parser.add_argument("--result-json", type=str, default=None)
    parser.add_argument("--dcm-info-json", type=str, default=None)
    parser.add_argument("--models-dir", type=str, default=os.path.join(base_dir_path, 'data_stage2'))
    parser.add_argument("--work-dir", type=str, default="/scratch")  #inference_scratch
    parser.add_argument("--batch-size", type=int, default=BATCH_SIZE)
    parser.add_argument('--extract-features-from-dir-only', type=str, default=None)

    opts = parser.parse_args(args[1:])

    if opts.result_json:
        result_json = opts.result_json
    else:
        result_json = os.path.join(opts.work_dir, 'features_result.json')

    if opts.dcm_info_json:
        dcm_info_json = opts.dcm_info_json
    else:
        dcm_info_json = os.path.join(opts.work_dir, 'dcm_info.json')

    work_dir = opts.work_dir

    logger.info("csv1: %s", opts.csv1)
    logger.info("csv2: %s", opts.csv2)
    logger.info("inference-data: %s", opts.inference_data)
    logger.info("model-dir: %s", opts.models_dir)
    logger.info("result-json: %s", result_json)
    logger.info("work_dir: %s", work_dir)
    logger.info("extract-features-from-dir-only: %s", opts.extract_features_from_dir_only)

    if os.path.exists(work_dir):
        logger.info("found work_dir %s", work_dir)
    else:
        logger.info("work_dir %s does not exist!!! will create", work_dir)
        if RUN_MODE == None and opts.batch_size > REDUCED_BATCH_SIZE:
            logger.info("reduce batch_size from %s to %s", opts.batch_size, REDUCED_BATCH_SIZE)
            opts.batch_size = REDUCED_BATCH_SIZE

    logger.info("batch_size: %s", opts.batch_size)

    meta_dir = os.path.join(work_dir, "meta")
    preprocess_dir = os.path.join(work_dir, "preprocess")
    scratch_dir = os.path.join(work_dir, "scratch")
    process_list_file_dir = os.path.join(preprocess_dir, "dataset")

    _mkdir_p(os.path.dirname(result_json))

    if opts.extract_features_from_dir_only is not None:
        process_list_file_dir = opts.extract_features_from_dir_only  #os.path.join(opts.extract_features_from_dir_only, "dataset")
        features = get_features_from_preprocess_list_files(process_list_file_dir)
        with open(result_json, 'w') as data:
            simplejson.dump(features, data)
        return 0


    _mkdir_p(work_dir)

    patients = patients_csv_parser.Patient.get_patients_from_csv(opts.csv1, opts.csv2)

    with open(opts.csv1, 'r') as file_crosswalk:
        i = -1
        for i, l in enumerate(file_crosswalk):
            pass
        target = i + 1

    with open(opts.csv1, 'r') as file_crosswalk:
        reader_crosswalk = csv.reader(file_crosswalk, delimiter='\t')
        csv_first_row = next(reader_crosswalk)

    images_batch = []
    results = {}
    dcm_infos = {}
    total_batch_num = 0
    total_image_num = 0
    time_start = time.time()
    pro = {}

    logger.info("total patients=%s images=%s", len(patients), target)
    for patient_id in patients:
        patient = patients[patient_id]
        images = patient.get_all_images()
        if (len(images_batch) + len(images) >= opts.batch_size) and images_batch:
            time_start_this_round = time.time()
            dcm_info, features = _process_batch_wrapper2(images_batch=images_batch, csv_first_row=csv_first_row,
                                                models_dir=opts.models_dir, meta_dir=meta_dir,
                                                files_dir=opts.inference_data,
                                                process_list_file_dir=process_list_file_dir,
                                                preprocess_dir=preprocess_dir,
                                                scratch_dir=scratch_dir, result_json=result_json, pro=pro)
            logger.info("perf debug: live pids %s", psutil.pids())
            results.update(features)
            dcm_infos.update(dcm_info)
            total_batch_num += 1
            total_image_num += len(images_batch)
            time_diff = time.time() - time_start
            time_diff_this_round = time.time() - time_start_this_round
            time_avg = time_diff / total_image_num if total_image_num > 0 else 0
            time_avg_this_round = time_diff_this_round / len(images_batch) if len(images_batch) > 0 else 0
            progress = float(total_image_num) / target if target > 0 else 0
            pro['p'] = progress
            pro['q'] = total_image_num
            logger.info("[%.4f] processed %s batches, %s/%s items. dcm_info=%s/%s, features=%s/%s, total_time=%.2f/%.2f, avg_cost=%.2f/%.2f",
                        progress, total_batch_num, len(images_batch), total_image_num, len(dcm_info), len(dcm_infos), len(features), len(results),
                        time_diff_this_round, time_diff, time_avg_this_round, time_avg)
            images_batch = []
            try:
                with open("/progress.txt", 'wb') as progress_file:
                    progress_file.write("%.3f\n" % (progress * 100))
            except Exception:
                logger.exception("cannot write progress file")
            if MAX_PROCESSED_NUM and total_image_num >= MAX_PROCESSED_NUM:
                logger.error("########################################reach max: %s >= %s", total_image_num, MAX_PROCESSED_NUM)
                break
        images_batch += images

    if images_batch:
        dcm_info, features = _process_batch_wrapper2(images_batch=images_batch, csv_first_row=csv_first_row,
                                            models_dir=opts.models_dir, meta_dir=meta_dir,
                                            files_dir=opts.inference_data,
                                            process_list_file_dir=process_list_file_dir,
                                            preprocess_dir=preprocess_dir,
                                            scratch_dir=scratch_dir, result_json=result_json, pro=pro)
        results.update(features)
        dcm_infos.update(dcm_info)
        total_batch_num += 1
        total_image_num += len(images_batch)

    time_diff = time.time() - time_start
    time_avg = time_diff / total_image_num if total_image_num > 0 else 0
    logger.info("total processed %s batches, %s images, total_time=%.2f, avg_cost=%.2f",
                total_batch_num, total_image_num, time_diff, time_avg)
    logger.info("result_json %s (len=%s)", result_json, len(results))
    logger.info("dcm_inf %s (len=%s)", dcm_info_json, len(dcm_infos))

    with open(result_json, 'w') as data:
        simplejson.dump(results, data)

    with open(dcm_info_json, 'w') as data:
        simplejson.dump(dcm_infos, data)

    return 0

if __name__ == '__main__':
    main(sys.argv)
