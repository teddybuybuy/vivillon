import argparse
import csv
import os
import sys
import logging
import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn import metrics
import random
import matplotlib.pylab as plt
from xgboost.sklearn import XGBClassifier
from sklearn.model_selection import train_test_split
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV

import pickle
import simplejson
import patients_csv_parser as Patient


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

def safe_float(n, othewise):
    try:
        return float(n)
    except Exception:
        return othewise

def safe_int(n, othewise):
    try:
        return int(n)
    except Exception:
        return othewise

class XGBBooster(object):
  def __init__(self, name):
    if name is None:
      self.model = XGBClassifier(
          learning_rate =0.05,
          n_estimators=100,
          objective= 'binary:logistic',
          #seed=27,
          max_depth=2)

    else:
      self.LoadModel(name)

  def Train(self, X, Y):
    """
    param_test1 = {
     #'max_depth':range(3,10,2),
     #'min_child_weight':range(1,10,2)
     #'n_estimators':range(1,100,3)
     'gamma':[i/10.0 for i in range(0,5)]
    }
    gsearch1 = GridSearchCV(estimator = self.model,
                            param_grid = param_test1,
                            scoring='roc_auc',
                            n_jobs=4,
                            iid=False,
                            cv=5)
    gsearch1.fit(X, Y)
    print (gsearch1.grid_scores_)
    print ("\n", gsearch1.best_params_)
    print("\n", gsearch1.best_score_)
    """

    #Fit the algorithm on the data
    self.model.fit(X, Y, eval_metric='auc', verbose=True)

    #Predict training set:
    Y_ = self.model.predict(X)
    Y_prob = self.model.predict_proba(X)[:,1]

    #Print model report:
    print "Model Report"
    print "Accuracy : %.4g" % metrics.accuracy_score(Y.values, Y_)
    print "AUC Score (Train): %f" % metrics.roc_auc_score(Y, Y_prob)
    print("positive predicted", np.sum(Y_), np.average(Y_prob))

    feat_imp = pd.Series(self.model.booster().get_fscore())
    feat_imp = feat_imp.sort_values(ascending=False)
    print (feat_imp)
    #feat_imp.plot(kind='bar', title='Feature Importances')
    #plt.ylabel('Feature Importance Score')

  def Predict(self, X, Y=None, X_full=None):
    Y_ = self.model.predict(X)
    Y_prob = self.model.predict_proba(X)[:,1]

    print("positive predicted", np.sum(Y_), np.average(Y_prob))
    if Y is not None:
      #Print model report:
      print "Model Report"
      print "Accuracy : %.4g" % metrics.accuracy_score(Y.values, Y_)
      print "AUC Score (Test): %f" % metrics.roc_auc_score(Y, Y_prob)

      fpr, tpr, thresholds = metrics.roc_curve(Y, Y_prob)
      tpr80 = [ 0 if x < 0.8 else 1 for x in tpr ] # which tpr have > 80 sensitivity.
      x = [a*b for a,b in zip(tpr80,fpr)] # mask
      xx = [y for y in x if y > 0] # filter out tpr < 80.
      print("Specificity at Sensitivity 0.8: %.4g" % (1 - np.min(xx)))
      x = [a*b for a,b in zip(tpr80,tpr)]
      xx = [y - 0.8 for y in x if y > 0]
      print("pAUC %.4g" % (np.sum(xx) / len(tpr)))
      if False:
          plt.plot(fpr, tpr, label='ROC curve (area = %0.3f)' % metrics.roc_auc_score(Y, Y_prob))
          plt.plot([0, 1], [0, 1], 'k--')  # random predictions curve
          plt.xlim([0.0, 1.0])
          plt.ylim([0.0, 1.0])
          plt.xlabel('False Positive Rate or (1 - Specifity)')
          plt.ylabel('True Positive Rate or (Sensitivity)')
          plt.title('Receiver Operating Characteristic')
          plt.legend(loc="lower right")
          plt.show()

      if X_full is not None:
        X_full.loc[:, 'prob'] = Y_prob
        X_full.loc[:, 'error'] = Y_prob - X_full['cancer']
        X_full.loc[:, 'error'] = np.abs(X_full['error'])
        X_full = X_full.sort('error', ascending=False)
        X_full.to_csv('error.tsv', sep='\t', index=False, quoting=csv.QUOTE_NONE)

    return Y, Y_prob

  def LoadModel(self, model_name):
    self.model = pickle.load(open(model_name, "rb"))

  def SaveModel(self, model_name):
    print "save", model_name
    pickle.dump(self.model, open(model_name, "wb"))


def ParseExamMeta(exams_metadata_tsv):
    X = pd.read_csv(exams_metadata_tsv, sep='\t', index_col=None,
                    converters={'subjectId': str})
    all_subjects = X.ix[:,'subjectId':'subjectId']
    all_subjects.drop_duplicates(inplace=True) # make it unique
    l_r = pd.DataFrame(["L", "R"], columns=["laterality"])

    #cartesian product
    all_subjects['_tmpkey'] = 1
    l_r['_tmpkey'] = 1

    subject_lr = pd.merge(all_subjects, l_r, on='_tmpkey').drop('_tmpkey', axis=1)
    age = X.groupby(['subjectId'], as_index=False)['age'].max()
    subject_lr = pd.merge(subject_lr, age, on='subjectId')

    #total exam
    exam_count = X.groupby(['subjectId'], as_index=False)['examIndex'].count()
    exam_count=exam_count.rename(columns = {'examIndex':'examCount'})
    subject_lr = pd.merge(subject_lr, exam_count, on='subjectId')

    #days on record
    daysSincePreviousExam = X.groupby(['subjectId'], as_index=False)['daysSincePreviousExam'].sum()
    daysSincePreviousExam=daysSincePreviousExam.rename(columns = {'daysSincePreviousExam':'days_on_record'})
    subject_lr = pd.merge(subject_lr, daysSincePreviousExam, on='subjectId')

    implant_ever = X.ix[:,['subjectId', 'implantEver']]
    implant_ever['implantEver'] = implant_ever['implantEver'].apply(lambda x: float('nan') if x =='.' else safe_int(x, 0))
    implant_ever = implant_ever.groupby(['subjectId'], as_index=False)['implantEver'].max()
    subject_lr = pd.merge(subject_lr, implant_ever, on='subjectId')

    implant_now = X.ix[:,['subjectId', 'implantNow']]
    implant_now['implantNow'] = implant_now['implantNow'].apply(lambda x: float('nan')if x =='.' else safe_int(x, 0))
    implant_now = implant_now.groupby(['subjectId'], as_index=False)['implantNow'].max()
    subject_lr = pd.merge(subject_lr, implant_now, on='subjectId')

    bc_history = X.ix[:,['subjectId', 'bcHistory']]
    bc_history = bc_history.groupby(['subjectId'], as_index=False)['bcHistory'].max()
    subject_lr = pd.merge(subject_lr, bc_history, on='subjectId')

    # note change to 10000 if . for last breast cancer years.
    years_since_prev_bc = X.ix[:,['subjectId', 'yearsSincePreviousBc']]
    years_since_prev_bc['yearsSincePreviousBc'] = years_since_prev_bc['yearsSincePreviousBc'].apply(lambda x: float('nan') if x =='.' else safe_float(x, 10000.0))
    years_since_prev_bc = years_since_prev_bc.groupby(['subjectId'], as_index=False)['yearsSincePreviousBc'].min()
    subject_lr = pd.merge(subject_lr, years_since_prev_bc, on='subjectId')

    redux_history = X.ix[:,['subjectId', 'reduxHistory']]
    redux_history['reduxHistory'] = redux_history['reduxHistory'].apply(lambda x: float('nan') if x =='.' else safe_int(x, 0))
    redux_history = redux_history.groupby(['subjectId'], as_index=False)['reduxHistory'].max()
    subject_lr = pd.merge(subject_lr, redux_history, on='subjectId')

    # TODO(jiesun): it might able to improve this.
    redux_laterality = X.ix[:,['subjectId', 'reduxLaterality']]
    redux_laterality['reduxLaterality'] = redux_laterality['reduxLaterality'].apply(lambda x: float('nan') if x =='.' else safe_int(x, 0))
    redux_laterality = redux_laterality.groupby(['subjectId'], as_index=False)['reduxLaterality'].max()
    subject_lr = pd.merge(subject_lr, redux_laterality, on='subjectId')

    # NOTE(0: No, 1: Yes, 9: Unknown) so I had to change 9 to -1 then do max
    hrt = X.ix[:,['subjectId', 'hrt']]
    hrt['hrt'] = hrt['hrt'].apply(lambda x: float('nan') if x == 9 else safe_int(x, -1))
    hrt = hrt.groupby(['subjectId'], as_index=False)['hrt'].max()
    subject_lr = pd.merge(subject_lr, hrt, on='subjectId')

    antiestrogen = X.ix[:,['subjectId', 'antiestrogen']]
    antiestrogen['antiestrogen'] = antiestrogen['antiestrogen'].apply(lambda x: float('nan') if x == 9 else safe_int(x, -1))
    antiestrogen = antiestrogen.groupby(['subjectId'], as_index=False)['antiestrogen'].max()
    subject_lr = pd.merge(subject_lr, antiestrogen, on='subjectId')

    firstDegreeWithBc = X.ix[:,['subjectId', 'firstDegreeWithBc']]
    firstDegreeWithBc['firstDegreeWithBc'] = firstDegreeWithBc['firstDegreeWithBc'].apply(lambda x: float('nan') if x == 9 else safe_int(x, -1))
    firstDegreeWithBc = firstDegreeWithBc.groupby(['subjectId'], as_index=False)['firstDegreeWithBc'].max()
    subject_lr = pd.merge(subject_lr, firstDegreeWithBc, on='subjectId')

    firstDegreeWithBc50 = X.ix[:,['subjectId', 'firstDegreeWithBc50']]
    firstDegreeWithBc50['firstDegreeWithBc50'] = firstDegreeWithBc50['firstDegreeWithBc50'].apply(lambda x: float('nan') if x == 9 else safe_int(x, -1))
    firstDegreeWithBc50 = firstDegreeWithBc50.groupby(['subjectId'], as_index=False)['firstDegreeWithBc50'].max()
    subject_lr = pd.merge(subject_lr, firstDegreeWithBc50, on='subjectId')

    # TODO(jiesun): figure out a way to do when bmi is missing.
    bmi = X.ix[:,['subjectId', 'bmi']]
    bmi['bmi'] = bmi['bmi'].apply(lambda x: float('nan') if x =='.' else safe_float(x, 30.0))
    bmi = bmi.groupby(['subjectId'], as_index=False)['bmi'].max()
    subject_lr = pd.merge(subject_lr, bmi, on='subjectId')

    race = X.ix[:,['subjectId', 'race']]
    race = race.groupby(['subjectId'], as_index=False)['race'].max()  #hopefully this will not change.
    subject_lr = pd.merge(subject_lr, race, on='subjectId')

    last_exam = X.ix[:,['subjectId', 'examIndex']]
    last_exam = last_exam.groupby(['subjectId'], as_index=False)['examIndex'].max()  #last exam.
    cancerL = X.ix[:,['subjectId', 'examIndex', 'cancerL']]
    cancerL = pd.merge(last_exam, cancerL, how='left', on=['subjectId', 'examIndex'])
    cancerL['laterality'] = "L";
    cancerL = cancerL.drop('examIndex', axis=1)
    cancerL = cancerL.rename(columns = {'cancerL':'cancer'})
    cancerR = X.ix[:,['subjectId', 'examIndex', 'cancerR']]
    cancerR = pd.merge(last_exam, cancerR, how='left', on=['subjectId', 'examIndex'])
    cancerR['laterality'] = "R";
    cancerR = cancerR.drop('examIndex', axis=1)
    cancerR = cancerR.rename(columns = {'cancerR':'cancer'})
    cancer = pd.concat([cancerL, cancerR]);
    subject_lr = pd.merge(subject_lr, cancer, on=['subjectId', 'laterality'])
    # make sure ground truth is 0 or 1
    subject_lr.assign(cancer = lambda x: safe_int(x.cancer, 0))

    for col in subject_lr.columns:
      print(col, subject_lr[col].dtype)
    if subject_lr['cancer'].dtype == np.dtype('O'):
        subject_lr = subject_lr[subject_lr.cancer != '.']
    return subject_lr

def ParseDcmInfo(dcm_json_file, image_crosswalk, exam_metadata, features_result, for_sc1=False):
    patients = Patient.Patient.get_patients_from_csv(image_crosswalk,
                                             None if exam_metadata == "" else exam_metadata,
                                             None if dcm_json_file == "" else dcm_json_file,
                                             None if features_result == "" else features_result)
    logging.info("Num Patients %d" % len(patients))
    feature_columns = ('subjectId', 'laterality',
                       # 'CompressionForce', 'BodyPartThickness',
                       'use_prediction')
    for view_type in ['MLO', 'CC']:
        for feature_type in ['glob']:
            for i in ['']:
                feature_columns += ('{view_type}_{feature_type}{o}'.format(view_type=view_type, feature_type=feature_type,o=i), )
        for feature_type in ['calci', 'bravg_1', 'bravg', 'mass']:
            for i in ['', '_delta', '_side_delta']:
                feature_columns += ('{view_type}_{feature_type}{o}'.format(view_type=view_type, feature_type=feature_type,o=i), )

    df = pd.DataFrame(columns=feature_columns)
    feature_dict = {}
    for col in feature_columns:
        feature_dict[col] = [];
    count_image = 0
    for patient_id, patient in patients.iteritems():
        all_images = patient.get_all_images()
        count_image += len(all_images)
        sum_compression_force = [0.0, 0.0]
        sum_body_part_thickness = [0.0, 0.0]
        count_compression_force = [0, 0]
        count_body_part_thickness = [0, 0]
        for image in all_images:
            laterality = 0 if (image.l_r == 'l' or image.l_r == 'L') else 1
            if 'CompressionForce' in image.dcm_info.keys():
                sum_compression_force[laterality] += image.dcm_info['CompressionForce']
                count_compression_force[laterality] += 1
            if 'BodyPartThickness' in image.dcm_info.keys():
                sum_body_part_thickness[laterality] += image.dcm_info['BodyPartThickness']
                count_body_part_thickness[laterality] += 1

        avg_compression_force = [0.0, 0.0]
        avg_body_part_thickness = [0.0, 0.0]

        latest_exam = patient.get_latest_exam()
        prev_exam = None if latest_exam is None else patient.get_previous_exam(latest_exam.exam_id)
        def score(feature):
            try:
                return float('nan') if feature is None else float(feature)
            except Exception:
                return float('nan')
        def diff_score(f1, f2):
            try:
                return float('nan') if f1 is None or f2 is None else float(f1) - float(f2)
            except Exception:
                return float('nan')
        for l in range(2):
            l_r = "L" if l==0 else "R"
            avg_compression_force[l] = float('nan') if count_compression_force[l] == 0 else sum_compression_force[l]/count_compression_force[l]
            avg_body_part_thickness[l] = float('nan') if count_body_part_thickness[l] == 0 else sum_body_part_thickness[l]/count_body_part_thickness[l]
            feature_dict['subjectId'].append(patient_id)
            feature_dict['laterality'].append("L" if l==0 else "R")
            # feature_dict['CompressionForce'].append(avg_compression_force[l])
            # feature_dict['BodyPartThickness'].append(avg_body_part_thickness[l])
            feature_dict['use_prediction'].append(1 if patient.has_features() else 0)

            if latest_exam is not None:
                for view_type in ['MLO', 'CC']:
                    image = latest_exam.get_image_by_type_l_r(view_type, l_r)
                    image2 = latest_exam.get_image_by_type_l_r(view_type=view_type, l_r=l_r, reverse_lr=True)
                    for feature_type in ['glob']:
                        feature_name = '{view_type}_{feature_type}'.format(view_type=view_type, feature_type=feature_type)
                        feature_dict[feature_name].append(float('nan') if image is None else score(image.get_feature(feature_type)))

                    for feature_type in ['calci', 'bravg_1', 'bravg', 'mass']:
                        # main
                        feature_name = '{view_type}_{feature_type}'.format(view_type=view_type, feature_type=feature_type)
                        feature_dict[feature_name].append(float('nan') if image is None else score(image.get_feature(feature_type)))

                        # delta to pre exam
                        if prev_exam is not None:
                            prev_image = prev_exam.get_image_by_type_l_r(view_type=view_type, l_r=l_r)
                            delta = float('nan') if image is None or prev_image is None else diff_score(image.get_feature(feature_type), prev_image.get_feature(feature_type))
                        else:
                            delta = float('nan')
                        feature_dict[feature_name + '_delta'].append(delta)

                        # delta to other side
                        delta = float('nan') if image is None or image2 is None else diff_score(image.get_feature(feature_type), image2.get_feature(feature_type))
                        feature_dict[feature_name + '_side_delta'].append(delta)

            #df.loc[i+l] = [int(patient_id), "L" if l==0 else "R", avg_compression_force[l], avg_body_part_thickness[l], 1 ]  #way too slow

    df = df.from_dict(feature_dict);

    if for_sc1: # sc1 don't allow previous exam information
        excluded_columns = [ 'CC_mass_delta', 'CC_calci_delta', 'CC_bravg_1_delta', 'CC_bravg_delta',
                             'MLO_mass_delta', 'MLO_calci_delta', 'MLO_bravg_1_delta', 'MLO_bravg_delta' ]
        predictors = [x for x in df.columns if x not in excluded_columns]
        df = df[predictors]

    print("total images:%d" % count_image)
    return df

def main(args):
    # use $0 --phase=0 --exam_metadata=/metadata/exams_metadata.tsv --output=per_breast_metadata.tsv to convert to per breast data format
    # use $0 --phase=1 --dcm_info=/metadata/dcm_info.json --features_result=features_result.json --image_crosswalk=/metadata/images_crosswalk.tsv --output=per_breast_dcminfo.tsv to convert to per breast data format
    # use $0 --phase=2 --exam_metadata=per_breast_metadata.tsv --dcm_info=per_breast_dcminfo.tsv --output=per_breast_training_data.tsv
    # use $0 --phase=3 --exam_metadata=per_breast_training_data.tsv --model=xgb_model.dat [--for_sc1] :to train and save model to output.
    # use $0 --phase=4 --exam_metadata=xgb_test_data.tsv --model=xgb_model.dat to load model and predict on the data.
    parser = argparse.ArgumentParser(description = "dream csv parser")
    parser.add_argument("--phase", dest="phase", type=int, default=0)
    parser.add_argument("--exam_metadata", dest="exam_metadata", type=str, default="")
    parser.add_argument("--dcm_info", dest="dcm_info", type=str, default="")
    parser.add_argument("--features_result", dest="features_result", type=str, default="")
    parser.add_argument("--image_crosswalk", dest="image_crosswalk", type=str, default="")
    parser.add_argument("--model", dest="model", type=str, default="xgb_model.dat")
    parser.add_argument("--output", dest="output", type=str, default="")
    parser.add_argument("--seed", dest="seed", type=int, default=888)
    parser.add_argument("--testpart", dest="testpart", type=float, default=0.33)
    parser.add_argument("--for_sc1", action='store_true', default=False)
    opts = parser.parse_args(args[1:])

    if opts.phase == 0:
        exam_metadata= ParseExamMeta(opts.exam_metadata)
        print("training samples:%d" % len(exam_metadata))
        print("positive samples:%d" % len(exam_metadata[exam_metadata.cancer==1]))
        exam_metadata.to_csv(opts.output, sep='\t', index=False, quoting=csv.QUOTE_NONE)
    elif opts.phase == 1:
        dcm_info = ParseDcmInfo(opts.dcm_info, opts.image_crosswalk, opts.exam_metadata, opts.features_result, for_sc1=opts.for_sc1)
        dcm_info.to_csv(opts.output, sep='\t', index=False, quoting=csv.QUOTE_NONE)
    elif opts.phase == 2:
        per_breast_metadata = pd.DataFrame.from_csv(opts.exam_metadata, sep='\t', index_col=None)
        if opts.for_sc1:
            keep_columns = ['subjectId', 'laterality', 'cancer']
            per_breast_metadata = per_breast_metadata[keep_columns]
        per_breast_dcm = pd.DataFrame.from_csv(opts.dcm_info, sep='\t', index_col=None)

        full_training_set = pd.merge(per_breast_metadata, per_breast_dcm, on=['subjectId', 'laterality'])
        full_training_set = full_training_set[full_training_set.use_prediction==1]
        print(len(full_training_set))
        full_training_set.to_csv(opts.output, sep='\t', index=False, quoting=csv.QUOTE_NONE)
    elif opts.phase == 3:
            # read in the training data
            training_set = pd.DataFrame.from_csv(opts.exam_metadata, sep='\t', index_col=None)
            print("training samples:%d" % len(training_set))
            # remove the negative cases from cancer patients, Jie doesn't agree, but zheng want to try it
            if True:
                for index, row in training_set.iterrows():
                    subject_it = row['subjectId']
                    laterality_it = row['laterality']
                    laterality_op = "R" if laterality_it == "L" else "L"
                    cancer = row['cancer']
                    if cancer == '1' or cancer == 1:
                        training_set.set_value(
                            (training_set.subjectId == subject_it) & (training_set.laterality == laterality_op),
                            'use_prediction', 0)
                training_set = training_set[training_set.use_prediction == 1]
            print("training samples after drop:%d" % len(training_set))

            predictors = [x for x in training_set.columns if x not in ['cancer', 'subjectId', 'laterality', 'use_prediction']]
            #predictors = [x for x in training_set.columns if x.find("calci") >= 0]
            #predictors = [x for x in training_set.columns if x.find("mass") >= 0]
            #predictors = [x for x in training_set.columns if x not in ['cancer', 'subjectId', 'laterality', 'use_prediction'] and not x.find("calci") >= 0 and not x.find("mass")>=0]

            # only undersample negative training set
            label = training_set.ix[:, ['cancer']]
            X_train, X_test, y_train, y_test = train_test_split(training_set, label, test_size=opts.testpart, random_state=opts.seed)
            X_train = X_train[predictors]
            X_full = X_test
            X_test = X_test[predictors]
            #undersampling
            Positive = y_train[y_train.cancer==1].index.values
            Negative = y_train[y_train.cancer!=1].index.values
            np.random.seed(opts.seed)
            np.random.shuffle(Negative)
            # Negative = Negative[0:len(Positive)]
            select_row_indice = np.concatenate((Positive, Negative))
            np.random.shuffle(select_row_indice)
            X_train = X_train.ix[select_row_indice,:]
            y_train = y_train.ix[select_row_indice,:]
            print("split train %d (%d positive) and test %d (%d positive)"
                  % (len(X_train), len(y_train[y_train.cancer==1]),
                     len(X_test), len(y_test[y_test.cancer==1])))

            xgb1 = XGBBooster(None)  # new model
            print("==== training report ====")
            xgb1.Train(X_train, y_train['cancer'])
            print("==== test report ====")
            xgb1.Predict(X_test, y_test['cancer'], X_full)
            xgb1.SaveModel(opts.model)

            # verify saved model correctly.
            #xgb2 = XGBBooster("xgb_model.dat")
            #xgb2.Predict(XTrain, YTrain['cancer'])
    else:
            X = pd.DataFrame.from_csv(opts.exam_metadata, sep='\t', index_col=None)
            predictors = [x for x in X.columns if x not in ['cancer', 'subjectId', 'laterality', 'use_prediction']]
            XTrain = X[predictors]
            print("training samples:%d" % len(XTrain))
            #YTrain = X.ix[:, ['cancer']]
            xgb1 = XGBBooster(opts.model)  # new model
            Y, Y_prob = xgb1.Predict(XTrain)
            print(Y_prob)


if __name__ == '__main__':
    main(sys.argv)
