#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os,sys
import cv2
import time
import concurrent.futures
import simplejson

import argparse
import logging
import numpy as np
import dicom
import dc_utils
import histogram as his
import multiprocessing

OUT_CHANNELS = 2
FLAGS = None

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)


def get_dcm_dcm_info(dcm):
    # disable dcm info for now
    return {}
    dcm_info = {}
    try:
        for i in dcm.dir():
            try:
                a = getattr(dcm, i)
                if i in ['PixelData'] or isinstance(a, dicom.sequence.Sequence):
                    continue

                if isinstance(a, str) or isinstance(a, int) or isinstance(a, list) \
                        or isinstance(a, dict) or isinstance(a, float):
                    dcm_info[i] = a
                else:
                    logger.error("skip %s: %s", i, a)
                    pass
            except Exception:
                logger.exception("bad i %s", i)
                continue
    except Exception:
        pass

    return dcm_info

def _histogram_output(raw_image, filename):
    from scipy.stats import threshold
    from scipy import ndimage
    image = threshold(raw_image, threshmin=0, threshmax=4095, newval=4095)
    height, width = image.shape
    m = 200
    image[0:height - 1, 0:m] = 0
    image[0:height - 1, width - m - 1:width - 1] = 0
    mask = threshold(image, threshmin=0, threshmax=0, newval=1)

    # find largest connected region.
    s = ndimage.generate_binary_structure(2, 2)  # iterate structure
    labeled_array, numpatches = ndimage.label(mask, s)  # labeling
    if numpatches == 0:
        return []
    sizes = ndimage.sum(mask, labeled_array, range(1, numpatches + 1))
    # To get the indices of all the min/max patches. Is this the correct label id?
    map = np.where(sizes == sizes.max())[0] + 1
    # inside the largest, respecitively the smallest labeled patches with values
    max_index = np.zeros(numpatches + 1, np.uint8)
    # logger.info("max_idx a shape %s %s", max_index.shape, numpatches)
    max_index[map] = 1
    # logger.info("max_idx b shape %s %s", max_index.shape, map.shape)
    max_feature = max_index[labeled_array]
    # logger.info("max_feature shape %s %s", max_feature.shape, image.shape)
    output = max_feature * image

    image_1 = threshold(output, threshmin=0, threshmax=1, newval=1)
    sum = np.sum(image_1)
    sum_total = image_1.shape[0] * image_1.shape[1]
    ratio = sum/sum_total
    logger.error("%s ratio %s", filename, ratio)

    if ratio < 0.25 or ratio > 0.42:
        return []

    image = raw_image * max_feature
    if True:
        output256 = image // 16
        d, f = os.path.split(filename)
        f = 'tmp/'+ f + '.jpg'
        cv2.imwrite(f, output256)

    image = threshold(image, threshmin=0, threshmax=4094, newval=0)
    h = his.compute_histogram(image)
    return h

def _read_and_process_dcm(filebase_with_path, file_extension, contrast_expand, remove_dream_rect_label, main_resize, dcm_info_only, histogram_output):
    dcm_info = {}
    histogram = []
    image = None
    image2 = None
    file = filebase_with_path + file_extension
    import time
    start_time = time.time()
    logger.info("begin read %s", file)
    if file_extension == '.dcm':
        if os.path.isfile(file):
            ds = dicom.read_file(file, stop_before_pixels=dcm_info_only)
            if not dcm_info_only:
                image = ds.pixel_array
            dcm_info = get_dcm_dcm_info(dcm=ds)
        else:
            logger.error('%s is not file, try tiff', file)
            file_extension = '.tiff'
            file = filebase_with_path + file_extension

    if (image is None) and (not (dcm_info_only and not histogram_output)):
        if file_extension == '.tiff':
            image = cv2.imread(file, -1)
    logger.info("done read %s, cost %.2f second", file, time.time() - start_time)

    assert (image is not None) or dcm_info_only, 'cannot process {} {}'.format(filebase_with_path, file_extension)

    if image is not None:
        if main_resize is not None:
            image = cv2.resize(image, None, None, main_resize, main_resize, interpolation=cv2.INTER_LINEAR)
        if remove_dream_rect_label:
            logger.info("%s: remove_dream_rect_label", file)
            image = dc_utils.remove_dream_rect_label(image)

        if contrast_expand != 'no':
            logger.info("%s: before contrast expansion: type=%s shape=%s max=%s min=%s",
                        file, image.dtype, image.shape, np.max(image),
                        np.min(image))
            histogram = his.compute_histogram(image)
            image = his.histogram_expansion(image, histogram, saturate=contrast_expand=='sat')
            image2 = image
            logger.info("%s: after contrast expansion: type=%s shape=%s max=%s min=%s",
                        file, image.dtype, image.shape, np.max(image), np.min(image))
        if histogram_output:
            histogram = _histogram_output(image, filebase_with_path)

    return image, image2, dcm_info, histogram

def _get_resize_file_name(dest_dir, filebase, size):
    return os.path.join(dest_dir, '{}_resize_{}.png'.format(filebase, size))

def _save_tiff(image, image2, filebase, dest_dir, main_resize, resize):
    filename = os.path.join(dest_dir, filebase + '.tiff')
    logger.info("%s: save", filename)
    start_time = time.time()
    cv2.imwrite(filename, image)
    logger.info("done write %s, cost %.2f second", filename, time.time() - start_time)
    if isinstance(resize, list):
        for sz in resize:
            center = False
            try:
                ratio = int(sz) / max(image.shape[0], image.shape[1])
            except Exception:
                if sz[0] == 'r':
                    continue # xxx todo zheng hack disable rxxx to write real file
                    ratio = float(sz[1:])
                    ratio = ratio / 1000.0
                    if main_resize is not None:
                        ratio = ratio / main_resize
                elif sz[0] == 'c':
                    ratio = int(sz[1:]) / max(image.shape[0], image.shape[1])
                    center = True
                else:
                    assert 0, sz
            resized_image = cv2.resize(image2, None, None, ratio, ratio,
                                       interpolation=cv2.INTER_CUBIC) #cv2.INTER_LINEAR) # zheng, since mass use it now, for backward compatibility
            filename = _get_resize_file_name(dest_dir=dest_dir, filebase=filebase, size=sz)
            if center:
                num_rows, num_cols = resized_image.shape[:2]
                if num_rows > num_cols:
                    diff = (num_rows - num_cols) // 2
                    resized_image = cv2.copyMakeBorder(resized_image, 0, 0, diff, num_rows - num_cols - diff, value=0,
                                                       borderType=cv2.BORDER_CONSTANT)
                else:
                    diff = (num_cols - num_rows) // 2
                    resized_image = cv2.copyMakeBorder(resized_image, diff, num_cols - num_rows - diff, 0, 0, value=0,
                                                       borderType=cv2.BORDER_CONSTANT)
            cv2.imwrite(filename, resized_image)

def _process_file_args(args):
    return _process_file(args['file'], args['dest_dir'], args['contrast_expand'],
                         args['remove_dream_rect_label'],
                         args['dcm_info_only'],
                         args['main_resize'],
                         args['resize'],
                         args['histogram_output'])

def _process_file(file, dest_dir, contrast_expand, remove_dream_rect_label, dcm_info_only, main_resize, resize, histogram_output):
    try:
        filebase_with_path, file_extension = os.path.splitext(file)
        filebase = os.path.basename(filebase_with_path)
        file_extension = file_extension.lower()
        assert file_extension in ['.dcm', '.tiff'] , "not a dcm file {}".format(file)
        image, image2, dcm_info, histogram = _read_and_process_dcm(filebase_with_path=filebase_with_path,
                                                                   file_extension=file_extension,
                                                                   contrast_expand=contrast_expand,
                                                                   remove_dream_rect_label=remove_dream_rect_label,
                                                                   main_resize=main_resize,
                                                                   dcm_info_only=dcm_info_only,
                                                                   histogram_output=histogram_output)
        if not dcm_info_only:
            _save_tiff(image=image, image2=image2, filebase=filebase, dest_dir=dest_dir, main_resize=main_resize, resize=resize)
        else:
            pass
        import simplejson
        logger.info("after_processed: %s", simplejson.dumps({filebase: dcm_info}))
        return {filebase: {'dcm': dcm_info, 'histogram': histogram}}
    except Exception as e:
        logger.exception("cannot process %s", file)
        raise e


def _process_files(src_filelist, src_filelist_base,
                   dest_dir, contrast_expand, remove_dream_rect_label,
                   thread_num, dcm_info_only, main_resize, resize, histogram_output, max_files_process):
    with open(src_filelist) as f:
        filelist = f.readlines()
    filelist = [x.split('\t')[0].strip() for x in filelist]
    l = []
    for i in filelist:
        _, file_extension = os.path.splitext(i)
        if not file_extension:
            i = i + '.dcm'
        if src_filelist_base is not None:
            i = os.path.join(src_filelist_base, i)
        item = {'file': i,
                'dest_dir': dest_dir,
                'contrast_expand': contrast_expand,
                'remove_dream_rect_label': remove_dream_rect_label,
                'dcm_info_only': dcm_info_only,
                'main_resize': main_resize,
                'resize': resize,
                'histogram_output': histogram_output}
        l.append(item)
        if (not dcm_info_only) and max_files_process and len(l) >= max_files_process:
            dcm_info_only = True

    results = {}
    logger.info("dcm_to_tiff uses threads=%s. cpu_count=%s", thread_num, multiprocessing.cpu_count())
    with concurrent.futures.ProcessPoolExecutor(max_workers=thread_num) as executor:
        for task, ret in zip(l, executor.map(_process_file_args, l)):
            # logger.info('%s return %s', task, ret)
            results.update(ret)

    return results, len(filelist)

def _write_dcm_info_file(dcm_info_json, dcm_info_output):
    if dcm_info_output is not None:
        logger.info("write %s", dcm_info_output)
        with open(dcm_info_output, 'w') as outfile:
            import simplejson
            simplejson.dump(dcm_info_json, outfile)
    return

def _write_resize_info_files(resize_info_output, resize, src_filelist, dest_dir):
    for sz in resize:
        with open(src_filelist) as f:
            filelist = f.readlines()
        filelist = [x.split('\t') for x in filelist]
        resize_info_files = "{}_{}.csv".format(resize_info_output, sz)
        logger.info("write %s", resize_info_files)
        with open(resize_info_files, "w") as outfile:
            for item in filelist:
                file = item[0].strip()
                has_cancer = int(item[1].strip())
                file_id, file_extension = os.path.splitext(file)
                filename = _get_resize_file_name(dest_dir=dest_dir, filebase=file_id, size=sz)
                outfile.write("{}\t{}\n".format(filename, has_cancer))

def main(args):
    #  python dream_dcm_to_tiff.py --src-filelist list --threads 10 --contrast-expand --remove-dream-rect-label --dcm-info-output output.json
    cpu_count = multiprocessing.cpu_count()
    if cpu_count <= 2:
        default_process_count = cpu_count
    else:
        default_process_count = max(cpu_count - 1, 1)

    parser = argparse.ArgumentParser(description="dream dsm to tiff preprocessor")
    parser.add_argument("--src-filelist", default=None, help='file that contains a list of files need to be processed')
    parser.add_argument("--src-filelist-base", default=None, help='base dir of the files in the list')
    parser.add_argument("--src", default=None, help='file need to be processed')
    parser.add_argument("--dest-dir", default=os.getcwd())
    parser.add_argument("--dcm-info-output", default=None, help='dcm file info output file')
    parser.add_argument('--histogram-output', default=None, help='get average histogram')
    parser.add_argument('--contrast-expand', default='no', choices=['no', 'yes', 'sat'])
    parser.add_argument('--dcm-info-only', action='store_true', default=False, help='get dcm file info only')
    parser.add_argument("--max-files-process", type=int, default=0, help='max number of full files process')
    parser.add_argument('--remove-dream-rect-label', action='store_true', default=False,
                        help='Remove dream rectangle label')
    parser.add_argument("--threads", type=int, default=default_process_count, help='number of threads')
    parser.add_argument("--main-resize", type=float, default=None, help='resize ratio to main image (< 1 means shrink)')
    parser.add_argument('--resize', nargs='*', default=[], help='resize images and save as id_resize_xxx.tiff')
    parser.add_argument('--resize-info-output', default=None)
    args = parser.parse_args(args[1:])
    time_start = time.time()
    results = None
    if args.src:
        results = _process_file(file=args.src, dest_dir=args.dest_dir,
                                contrast_expand=args.contrast_expand,
                                remove_dream_rect_label=args.remove_dream_rect_label,
                                dcm_info_only=args.dcm_info_only,
                                main_resize=args.main_resize,
                                resize=args.resize,
                                histogram_output=args.histogram_output)
    elif args.src_filelist:
        results, total = _process_files(src_filelist=args.src_filelist,
                                        src_filelist_base=args.src_filelist_base,
                                        dest_dir=args.dest_dir,
                                        contrast_expand=args.contrast_expand,
                                        remove_dream_rect_label=args.remove_dream_rect_label,
                                        thread_num=args.threads,
                                        dcm_info_only=args.dcm_info_only,
                                        main_resize=args.main_resize,
                                        resize=args.resize,
                                        histogram_output=args.histogram_output,
                                        max_files_process=args.max_files_process)
    if results is not None and args.dcm_info_output is not None:
        r = {a: results[a]['dcm'] for a in results}
        _write_dcm_info_file(dcm_info_json=r,
                             dcm_info_output=args.dcm_info_output)
    if args.histogram_output:
        hist_total = []
        for a in results:
            hist_total = his.merge_histogram(hist_total, results[a]['histogram'])
        output_name = args.histogram_output
        if output_name:
            outfile = output_name
            np.save(outfile, hist_total)
        try:
            import matplotlib.pylab as plt
            hist_total[0] = 0
            plt.bar(range(len(hist_total)), hist_total)
            #plt.show()
            plt.savefig(outfile + '.png')
        except Exception:
            logger.exception("cannot save")

    if args.resize and args.resize_info_output:
        _write_resize_info_files(resize_info_output=args.resize_info_output, resize=args.resize,
                                 src_filelist=args.src_filelist, dest_dir=args.dest_dir)

    total_time = time.time() - time_start
    avg_time = total_time / total if total > 0 else 0.0
    logger.info("dcm_to_tiff done: batch_size=%s, total_time=%.2f, avg_cost %.2f second", total, total_time, avg_time)

if __name__ == '__main__':
    main(sys.argv)

