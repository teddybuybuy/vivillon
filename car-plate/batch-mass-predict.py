#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os,sys
import cv2
import argparse
import numpy as np
import tensorflow as tf
import fcn32_vgg
import fcn32_vgg2
import dc_utils
import dicom
import time
import glob
import histogram as his
import logging
import simplejson
from patients_csv_parser import Patient, CANCER_POSITIVE, CANCER_NEGATIVE, CANCER_UNKNOWN
import bounding_box as bb

BHW = 30  # bbox half width
BHH = 30  # bbox half height

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

flags = tf.app.flags
FLAGS = flags.FLAGS

#flags.DEFINE_string('input_dir', '', 'directory to dicom files.')
flags.DEFINE_string('input_list_file', '', 'input list file for the files to be preprocessed')
flags.DEFINE_string('train_data', '/trainingData', 'file directory for input training image.')
flags.DEFINE_string('output_dir', '', 'directory to output cropped files.')
flags.DEFINE_string('tsv_output_dir', '', 'directory to output tsv.')
flags.DEFINE_string('model', '', 'load existing model')
flags.DEFINE_string('src_file_postfix', '.dcm', 'source file extension')
flags.DEFINE_bool('use_resized_400_img', False, 'Use the existing resized image at 400 pixels max')
flags.DEFINE_bool('write_cropped_img', True, 'whether or not to dump out the cropped img')
flags.DEFINE_float('mass_confidence_thresh', 5.0, 'only use confidence level.')
flags.DEFINE_boolean('write_prob_file', False, 'If true, write out the prob file for debugging purpose')
flags.DEFINE_integer('max_cropped_images', 10000000, 'Number of maximum cropped image to generate.')
flags.DEFINE_bool('contrast_expand', False, 'expand/shrink to 12 bits')
flags.DEFINE_integer('thread', 0, 'Number of thread.')

INPUT_SIZE = 400.0  # we resize the input image so the longer edge is this size.

def pre_process(fname, input_channels):
    base, ext = os.path.splitext(fname)
    if ext.lower() == ".dcm":
        dcm = dicom.read_file(fname)
        source = dcm.pixel_array
        rows = int(dcm.data_element('Rows').value)
        cols = int(dcm.data_element('Columns').value)
        #depth = int(dcm.data_element('BitsStored').value)
    else:
        source = cv2.imread(fname, -1)
        cols, rows = source.shape
    if INPUT_SIZE == max(rows, cols):
        ratio = 1.0
        image = source
    else:
        ratio = INPUT_SIZE / max(rows, cols)
        logger.info("resize from (%d, %d) to (%d, %d)", rows, cols, rows*ratio, cols*ratio)
        image = cv2.resize(source, None, None, ratio, ratio,
                           interpolation = cv2.INTER_CUBIC)

    if FLAGS.contrast_expand:
        histogram = his.compute_histogram(image)
        image = his.histogram_expansion(image, histogram)


    w, h = image.shape
    if input_channels == 3:
        cimage = np.empty((w, h, 3), dtype=image.dtype)
        cimage[:, :, 0] = image
        cimage[:, :, 1] = image
        cimage[:, :, 2] = image
    else:
        assert input_channels == 1, str(input_channels)
        cimage = np.empty((w, h, 1), dtype=image.dtype)
        cimage[:, :, 0] = image

    shape = (1,) + cimage.shape  # batch 1.
    cimage = np.reshape(cimage, shape)
    sz = image.shape[:2]
    return cimage, sz

def load_model(model, graph, sess):
    start_time = time.time()
    saver = tf.train.import_meta_graph(model + '.meta')
    X = graph.get_tensor_by_name("images:0")
    Y = graph.get_tensor_by_name("predict:0") #tf.reshape(prob, shape)
    dropout_update_placeholder = graph.get_tensor_by_name("dropout_placeholder:0")
    dropout_update_op = graph.get_tensor_by_name("Assign:0")
    init = tf.global_variables_initializer()
    sess.run(init)
    saver.restore(sess, model)
    sess.run(dropout_update_op, {dropout_update_placeholder: 1})

    # compute a tensor for bounding box.
    gaussian = bb.matlab_style_gauss2D((BHH*2+1,BHW*2+1), sigma = 5)
    filters = np.zeros(shape=gaussian.shape + (2, 1)) # input channel, output channel
    filters[:,:,1,0] = gaussian;
    ff = tf.constant(filters, name='filters', dtype="float32")
    Z = tf.nn.conv2d(Y, ff, [1, 1, 1, 1], padding="SAME")
    end_time = time.time()
    logger.info("load model %s. cost %s seconds", model, end_time - start_time)
    return X, Y, Z

meta_file_inited = False
def append_meta_data(out_fname, x_low, x_high, y_low, y_high, confidence, in_fname, has_cancer):
    global meta_file_inited
    meta_fname = os.path.join(FLAGS.tsv_output_dir, "crop_meta.tsv")
    if not meta_file_inited:
        with open(meta_fname, "w") as outfile:
            # create an empty file
            pass
        #    outfile.write("cropped_fname\tconfidence\tx_low\tx_high\ty_low\ty_high\toriginal_fname\tcancer\n")
        meta_file_inited = True
    with open(meta_fname, 'a') as outfile:
        #outs = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%d\n" % (os.path.basename(out_fname), confidence, x_low, x_high, y_low, y_high, in_fname, has_cancer)
        outs = "%s\t%d\t%s\n" % (os.path.basename(out_fname), has_cancer, str(confidence))
        outfile.write(outs)

meta_nega_file_inited = False
def append_meta_nega_data(in_fname, has_cancer, confidences):
    global meta_nega_file_inited
    meta_fname = os.path.join(FLAGS.tsv_output_dir, "crop_nega_meta.tsv")
    if not meta_nega_file_inited:
        # with open(meta_fname, "w") as outfile:
        #    outfile.write("original_fname\tcancer\tconfidences\n")
        meta_nega_file_inited = True
    with open(meta_fname, 'a') as outfile:
        outs = "%s\t%s" % (in_fname, has_cancer)
        for confidence in confidences:
            outs += "\t%s" % confidence
        outs += "\n"
        outfile.write(outs)

def write_cropped_img(fname, bbox, confidence, is_cancer, lock_stats):
    fname_base, _ = os.path.splitext(os.path.basename(fname))
    out_fname = os.path.join(FLAGS.output_dir, fname_base + "_masscrop.png")
    if FLAGS.write_cropped_img:
        base, ext = os.path.splitext(fname)
        if ext.lower() == ".dcm":
            dcm = dicom.read_file(fname)
            source = dcm.pixel_array
            rows = int(dcm.data_element('Rows').value)
            cols = int(dcm.data_element('Columns').value)
            #depth = int(dcm.data_element('BitsStored').value)
        else:
            source = cv2.imread(fname, -1)
            rows, cols = source.shape

        ratio = INPUT_SIZE / max(rows, cols)
        crop_bbox = [(int)(v / ratio) for v in list(bbox) + [BHH, BHW]]
        logger.info("Before crop: %s %s %s %s %s", fname, np.average(source), np.min(source), np.max(source), source.shape);
        cy, cx, bhh, bhw = crop_bbox
        y_low = max(cy - bhh, 0)
        y_high = min(cy + bhh, rows)
        x_low = max(cx - bhw, 0)
        x_high = min(cx + bhw, cols)
        image = source[y_low : y_high, x_low : x_high]
        logger.info("crop_bbox: %s %s %s %s %s %s %s %s", fname, crop_bbox, rows, cols, x_low, x_high, y_low, y_high)
        logger.info("After crop: %s %s %s %s %s", fname, np.average(image), np.min(image), np.max(image), image.shape)

        # XXX: Do not adjust the image to full 16bits
        # image *= (int)(65536 / (max(np.max(image), 4096))
        cv2.imwrite(out_fname, image)
    else:
        x_low = x_high = y_low = y_high = 0

    with lock_stats:
        append_meta_data(out_fname, x_low, x_high, y_low, y_high, confidence, os.path.basename(fname), is_cancer)

def predict_one(lock_tf, lock_stats, line, sess, X, Y, Z, input_channels):
    fname, is_cancer = line.strip().split('\t')
    fname_id = os.path.basename(fname).split('_')[0]
    is_cancer = int(is_cancer)  # possible value: 1, 0, -1

    if is_cancer == -1:
        logger.info("cancer unknown %s", fname)

    if FLAGS.use_resized_400_img:
        filepath_img = os.path.join(FLAGS.train_data, fname + "_resize_400.png")
    else:
        filepath_img = os.path.join(FLAGS.train_data, fname + FLAGS.src_file_postfix)

    input_img, sz = pre_process(filepath_img, input_channels)
    with lock_tf:
        predict, conv = sess.run((Y, Z), feed_dict={X: input_img})
    prob = predict[:, :, :, 1]
    prob = prob[0]
    prob = cv2.resize(prob, (sz[1], sz[0]))
    prob *= 255
    # print ("<<<<<<<<<<< USE CPU RESULT")
    # bboxs, confidences = bb.find_boundingbox(prob, BHW, BHH, conv=None)
    # print (">>>>>>>>>>> USE GPU RESULT")
    bboxs, confidences = bb.find_boundingbox(prob, BHW, BHH, conv=conv)
    logger.info("output %d candidates for file %s: %s %s", len(bboxs), filepath_img, bboxs, confidences)
    if FLAGS.write_prob_file:
        dc_utils.write_prob_file(file_prefix='result_',
                                 file=filepath_img,
                                 prob=predict,
                                 origin=input_img,
                                 label=None,
                                 size=sz,
                                 confidences=confidences,
                                 bboxs=bboxs,
                                 BHH=BHH, BHW=BHW)

    global tot_cropped_cnt, score_dict
    if len(confidences) > 0 and confidences[0] >= FLAGS.mass_confidence_thresh:
        write_cropped_img(filepath_img, bboxs[0], confidences[0], is_cancer=is_cancer, lock_stats=lock_stats)
        with lock_stats:
            tot_cropped_cnt += 1
            score_dict[fname_id] = str(confidences[0])
            if tot_cropped_cnt >= FLAGS.max_cropped_images:
                logger.info("WARNING: Exceeded target amount of cropped images")
                return
            pass
    else:
        with lock_stats:
            score_dict[fname_id] = str(0)
            # skipping the rest!
            if len(confidences) > 0:
                # not reaching threshold
                append_meta_nega_data(filepath_img, is_cancer, confidences)
                pass
            else:
                append_meta_nega_data(filepath_img, is_cancer, [])
            pass


score_dict = {}
tot_cropped_cnt = 0

def main (_):
    logger.info("start mass predict")

    import threading
    import concurrent.futures
    if FLAGS.thread == 0:
        import multiprocessing
        if multiprocessing.cpu_count() >= 8:
            FLAGS.thread = multiprocessing.cpu_count() / 2
        logger.info("uses threads=%s. cpu_count=%s", FLAGS.thread, multiprocessing.cpu_count())
    lock_tf = threading.Lock()
    lock_stats = threading.Lock()

    with tf.Graph().as_default() as graph:
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True

        with tf.Session(config=config) as sess:
            time_start = time.time()
            X, Y, Z = load_model(FLAGS.model, graph, sess)
            input_channels = X.get_shape()[3]
            logger.info("load model %s cost %.2f", FLAGS.model, time.time() - time_start)
            total_count = 0

            if FLAGS.thread > 1:
                with concurrent.futures.ThreadPoolExecutor(max_workers=FLAGS.thread) as executor:
                    # Start the load operations and mark each future with its URL
                    future_to_predict = {executor.submit(predict_one, lock_tf, lock_stats, line, sess, X, Y, Z, input_channels): line for line in open(FLAGS.input_list_file).readlines()}
                    for future in concurrent.futures.as_completed(future_to_predict):
                        total_count += 1
                        line = future_to_predict[future]
                        try:
                            result = future.result()
                            logger.debug("%s is done", total_count)
                        except Exception as exc:
                            logger.exception('%s generated an exception: %s', line, exc)
                pass
            else:
                for line in open(FLAGS.input_list_file).readlines():
                    total_count += 1
                    predict_one(lock_tf=lock_tf,lock_stats=lock_stats, line=line, sess=sess, X=X, Y=Y, Z=Z, input_channels=input_channels)

        time_end = time.time()
        if total_count:
            avg = (time_end - time_start) / total_count
        else:
            avg = 0

        logger.info("total mass done batch_size=%s. total=%.2f avg_cost %.2f second", total_count, time_end - time_start, avg)

        with open(os.path.join(FLAGS.tsv_output_dir, "crop_mass.json"), 'w') as outfile:
            outfile.write(simplejson.dumps(score_dict))

        
if __name__ == '__main__':
    tf.app.run()
