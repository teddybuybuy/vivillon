#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division

import math
import os
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
from scipy import signal

import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

RELATIVE_THRESHOLD = 3.0 # drop all the boxes less 1/N of intensity of the first one.

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_boolean('show2', False, '')
flags.DEFINE_string('image_dir2', '', 'directory to dicom files.')
flags.DEFINE_string('image2', '', 'image to histogram')

def check_nearby(input, candidate, BH, BW):
    for c in candidate:
        if abs(c[0] - input[0]) <= BH or abs(c[1] - input[1]) <= BW :
            return True
    return False

def matlab_style_gauss2D(shape=(3,3),sigma=0.5):
    """
    2D gaussian mask - should give the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma])
    """
    m,n = [(ss-1.)/2. for ss in shape]
    y,x = np.ogrid[-m:m+1,-n:n+1]
    h = np.exp( -(x*x + y*y) / (2.*sigma*sigma) )
    h[ h < np.finfo(h.dtype).eps*h.max() ] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h

def find_boundingbox(im, BHW, BHH, conv=None):
  BW  = 2 * BHW + 1  # odd because convolution
  BH  = 2 * BHH + 1

  logger.info("find bounding box: input image shape - %s dtype %s", im.shape, im.dtype)

  #ones = np.ones((K,K))
  if conv is None:
    gaussian = matlab_style_gauss2D((BH,BW), sigma = 5)
    conv = signal.convolve2d(im, gaussian)
    conv = conv[BHH:-BHH,BHW:-BHW]
  else:
    #print("conv shape:", conv.shape, np.max(conv), np.min(conv), np.average(conv))
    conv = conv[0,:,:,0]
    conv *= 255
  max_intensity = np.max(conv);

  #out = np.concatenate((im, conv), axis=1)
  #plt.imshow(out, cmap="Greys_r")

  def intensity(idx):
      return conv[idx[0]][idx[1]]
  positions = [(x,y) for x in range(im.shape[0]) for y in range(im.shape[1])]
  positions = [x for x in positions if conv[x] >= max_intensity / RELATIVE_THRESHOLD]
  positions = sorted(positions, key=intensity, reverse=True)

  logger.info("sorted max intensity: %s", max_intensity)

  candidate = []
  for bb in positions:
    if conv[bb] < max_intensity / RELATIVE_THRESHOLD:
        break
    if check_nearby(bb, candidate, BH,BW): #excluding overlapping boxes.
        continue
    candidate.append(bb)
    logger.info("Append candidate intensity: %s, bb %s", conv[bb], bb)

  confidence = [conv[x] for x in candidate] # TODO: use average from im?
  return candidate, confidence

def main (_):
    BHW = 20  # bbox half width
    BHH = 20  # bbox half height
    if FLAGS.image_dir2:
        logger.info(FLAGS.image_dir2)
        for filepath_img in glob.glob(FLAGS.image_dir2 + "*.dcm"):
            logger.info("processing image %s:", filepath_img)
            dicom_content = dicom.read_file(filepath_img)
            images = dicom_content.pixel_array
    else:
        logger.info(FLAGS.image2)
        filename, file_extension = os.path.splitext(FLAGS.image2)
        if file_extension == '.dcm':
            dicom_content = dicom.read_file(FLAGS.image2)
            images = dicom_content.pixel_array
        elif file_extension in ['.tiff', '.tif', '.jpg', '.png']:
            images = cv2.imread(FLAGS.image2, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
        else:
            print('unknown image type')
            return

        candidate, confidence = find_boundingbox(images, BHW, BHH)
        logger.info("output %d candidates: %s, confidence: %s", len(candidate), candidate, confidence)
        if FLAGS.show2:
            fig, ax = plt.subplots(1)
            ax.imshow(im,  cmap="Greys_r")

            for bb in candidate:
                # Create a Rectangle patch
                rect = patches.Rectangle((bb[1] - BHW, bb[0] - BHH),
                                         BW, BH, linewidth=1,
                                         edgecolor='r', facecolor='none')

                # Add the patch to the Axes
                ax.add_patch(rect)

                ax.annotate('%.4f' % conv[bb],
                            xy = (bb[1], bb[0] - BHH),
                            xytext=(bb[1], bb[0] - BHH - 30),
                            arrowprops = dict(facecolor='red', shrink=0.05),
                            color='yellow')

            plt.show()

if __name__ == '__main__':
    tf.app.run()

