#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os,sys
import cv2
os.environ['LD_LIBRARY_PATH']='/opt/cuda/lib64'
import argparse
import logging
import numpy as np
import tensorflow as tf
import dicom
import time
import concurrent.futures
import threading

import fcn32_vgg
import dc_utils
import histogram as his
from stitcher import Stitcher

OUT_CHANNELS = 2
FLAGS = None

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)


def sigmoid_v1(image):
    c, k, x0, y0 = [2674.38285022, 0.00734754245424, 1532.51097978, 291.341664686]
    return c / (1 + np.exp(-k * (image - x0))) + y0

def do_adjust_color_profile(image, adjust_color_profile):
    logger.debug("do_adjust_color_profile %s", adjust_color_profile)
    if adjust_color_profile == 'v1':
        image = sigmoid_v1(image)
    else:
        assert False, 'invalid color_profile {}'.format(adjust_color_profile)
    image = image.astype(np.int16)
    return image

def pre_process(file, adjust_color_profile, contrast_expand, remove_dream_rect_label, sp_tiff):
    _, file_extension = os.path.splitext(file)
    file_extension = file_extension.lower()
    if file_extension == '.dcm':
        dicom_content = dicom.read_file(file)
        image = dicom_content.pixel_array
        if remove_dream_rect_label:
            image = dc_utils.remove_dream_rect_label(image)
        # image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        # shape = image.shape + (1,)
        # image = np.reshape(image, shape)
    elif file_extension == '.tiff':
        image = cv2.imread(file, -1)
        logger.info("before %s", np.max(image))
        if sp_tiff:
            image = np.right_shift(image, 4)
            logger.info("after %s", np.max(image))
        if remove_dream_rect_label:
            image = dc_utils.remove_dream_rect_label(image)
    else:
        image = cv2.imread(file, -1)
    # logger.info("shape = %s", image.shape)

    if adjust_color_profile:
        image = do_adjust_color_profile(image, adjust_color_profile)

    if contrast_expand != 'no':
        histogram = his.compute_histogram(image)
        equalized_image = his.histogram_expansion(image, histogram, saturate=contrast_expand=='sat')
        image = equalized_image
        logger.info("contrast expansion: type=%s shape=%s max=%s min=%s", image.dtype, image.shape, np.max(image), np.min(image))

    sz = image.shape[:2]
    # image = image.astype(np.float32) ask wei
    if len(image.shape) == 2:
        if FLAGS.real_channels == 3:
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
            shape = (1,) + image.shape
            image = np.reshape(image, shape)
        elif FLAGS.real_channels == 1:
            shape = (1,) + image.shape + (1, )
            image = np.reshape(image, shape)
        else:
            assert False, FLAGS.real_channels
    elif len(image.shape) == 3:
        if image.shape[2] == 3:
            if FLAGS.real_channels == 3:
                shape = (1,) + image.shape
                image = np.reshape(image, shape)
            elif FLAGS.real_channels == 1:
                image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                shape = (1,) + image.shape + (1,)
                image = np.reshape(image, shape)
            else:
                assert False, FLAGS.real_channels
            pass
        else:
            assert False, str(image.shape)
    else:
        assert False, str(image.shape)

    # image = cv2.resize(image, None, None, 1.7, 1.7)
    return image, sz

def predict_one(lock, sess, predict, X, idx, file):
    time_start = time.time()
    image, sz = pre_process(file, FLAGS.adjust_color_profile, FLAGS.contrast_expand, FLAGS.remove_dream_rect_label, FLAGS.sp_tiff)
    if FLAGS.resize or FLAGS.resize_ratio:
        if FLAGS.resize:
            ratio = FLAGS.resize / max(image.shape[1], image.shape[2])
        elif FLAGS.resize_ratio:
            ratio = float(FLAGS.resize_ratio)
        else:
            assert False, str(FLAGS.resize) + str(FLAGS.resize_ratio)
        resized_image = cv2.resize(image[0], None, None, ratio, ratio,
                                   interpolation=cv2.INTER_LINEAR)
        if len(resized_image.shape) == 2:
            new_shape = (1,) + resized_image.shape + (1,)
        else:
            assert len(resized_image.shape) == 3, str(resized_image.shape)
            new_shape = (1,) + resized_image.shape
        image = np.reshape(resized_image, new_shape)
        sz = resized_image.shape
    time_after_preprocess = time.time()

    if FLAGS.patch or FLAGS.patch_size:
        if FLAGS.patch:
            patch_w = FLAGS.patch
            patch_h = FLAGS.patch
        else:
            _, _, patch_w, _ = image.shape
            patch_h = (FLAGS.patch_size + 1) // patch_w + 1
        logger.debug("before stitcher split %s. shape=%s", file, image.shape)
        stch = Stitcher(image=image, margin=16, patch_h=patch_h, patch_w=patch_w)
        splitted = stch.split()
        time_after_splitter = time.time()
        logger.debug("after stitcher split %s", file)
        processed_patches = np.zeros(splitted.shape[:3] + (2,))
        i = 0
        for i, patch in enumerate(splitted):
            shape = (1,) + patch.shape
            patch = np.reshape(patch, shape)
            logger.info("patch %s ...", i)
            with lock:
                processed_patch = sess.run(predict, feed_dict={X: patch})
            processed_patches[i, :] = processed_patch[0, :]
            # processed_patches.append(processed_patch)
        time_after_predict = time.time()
        logger.debug("before stitcher stitch %s", file)
        result = stch.stitch(processed_patches)
        del stch
        del processed_patches
        time_after_stitch = time.time()
        t_preprocess = time_after_preprocess - time_start
        t_splitter = time_after_splitter - time_after_preprocess
        t_predict = time_after_predict - time_after_splitter
        t_stitch = time_after_stitch - time_after_predict
        logger.debug("[%s] after stitcher stitch %s", idx, file)
        if FLAGS.random_crash:
            if int(time_start) % 500 == 0:
                assert False, 'random crash: {}'.format(time_start)
                pass
    else:
        result = sess.run(predict, feed_dict={X: image})
        time_after_predict = time.time()
        time_after_stitch = time_after_predict
        t_preprocess = time_after_preprocess - time_start
        t_splitter = 0
        t_predict = time_after_predict - time_after_preprocess
        t_stitch = 0
        logger.debug("[%s] after predict %s",
                     idx, file)

    filebase, _ = os.path.splitext(os.path.basename(file))
    result_file = os.path.join(FLAGS.result_dir, filebase + FLAGS.result_postfix)
    origin = image if FLAGS.write_original else None
    dc_utils.write_prob_file(file_prefix=result_file, file=file, prob=result, origin=origin, label=None, size=sz,
                             resize_ratio=FLAGS.result_resize_ratio)
    time_done = time.time()
    t_write_prob = time_done - time_after_stitch
    t_total = time_done - time_start

    return t_preprocess, t_splitter, t_predict, t_splitter, t_write_prob, t_total

def run_predict(files):
    def update_stats(idx, file, result, tt):
        t_preprocess = result[0]
        t_splitter = result[1]
        t_predict = result[2]
        t_stitch = result[3]
        t_write_prob = result[4]
        t_total = result[5]
        for i in range(len(tt)):
            tt[i] += result[i]
        logger.debug("[%s] done %s. cost %.2f second (prep=%.2f, split=%.2f, pred=%.2f, stitch=%.2f, write=%.2f)",
                     idx, file, t_total, t_preprocess, t_splitter, t_predict, t_stitch, t_write_prob)

    logger.info("start fcn-predict")
    model = FLAGS.model  #'data/model-229999'
    tt_time_start = time.time()
    with tf.Graph().as_default() as graph:
        start_time = time.time()
        saver = tf.train.import_meta_graph(model + '.meta')
        X = graph.get_tensor_by_name("images:0") #tf.placeholder(tf.float32, shape=(BATCH, None, None, FLAGS.channels), name="images")
        logger.info("input X shape = %s", X.get_shape())
        input_channels = X.get_shape()[3]
        if FLAGS.real_channels != input_channels:
            logger.info("force real input channel to %s from %s", input_channels, FLAGS.real_channels)
            FLAGS.real_channels = input_channels
            logger.info("real input channel is %s now", FLAGS.real_channels)
        dropout_update_placeholder = graph.get_tensor_by_name("dropout_placeholder:0")
        dropout_update_op = graph.get_tensor_by_name("Assign:0")
        # dropout_update_op = graph.get_tensor_by_name("dropout_rate/Assign:0") # this is worng, just for test, ask zheng

        if False: # xxx todo: don't understand why this doesn't work
            if FLAGS.use_vgg:
                print('use vgg')
                fcn32 = fcn32_vgg.FCN32VGG(vgg16_npy_path="vgg16.npy")
                fcn32.build(X, train=False, num_classes=2, random_init_fc8=False, debug=False)
                logits = graph.get_tensor_by_name("up/conv2d_transpose_1:0")
                #logits = fcn32.upscore
            else:
                logits = graph.get_tensor_by_name("upscale/upscale:0")
            shape = tf.shape(logits)
            logits = tf.reshape(logits, (-1, OUT_CHANNELS))
            predict = tf.nn.softmax(logits)
            predict = tf.reshape(predict, shape)
        else:
            predict = graph.get_tensor_by_name("predict:0") #tf.reshape(prob, shape)

        init = tf.initialize_all_variables()
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        logger.info("load model %s, cost %.2f seconds", model, time.time() - start_time)

        with tf.Session(config=config) as sess:
            sess.run(init)
            saver.restore(sess, model)
            if FLAGS.print_var:
                for n in graph.as_graph_def().node:
                    print('node: %s' % (n.name))

            # no dropout for predict
            new_dropout_rate = 1 # 1 means no dropout
            sess.run(dropout_update_op, {dropout_update_placeholder: new_dropout_rate})
            tt = [0, 0, 0, 0, 0]

            lock = threading.Lock()
            if FLAGS.thread:
                with concurrent.futures.ThreadPoolExecutor(max_workers=FLAGS.thread) as executor:
                    # Start the load operations and mark each future with its URL
                    future_to_predict = {executor.submit(predict_one, lock, sess, predict, X, idx, file): (idx, file) for idx, file in enumerate(files)}
                    for future in concurrent.futures.as_completed(future_to_predict):
                        idx, file = future_to_predict[future]
                        try:
                            result = future.result()
                            update_stats(idx, file, result, tt)
                        except Exception as exc:
                            logger.exception('%s generated an exception: %s', file, exc)
            else:
                for idx, file in enumerate(files):
                    result = predict_one(lock, sess, predict, X, idx, file)
                    update_stats(idx, file, result, tt)

            if len(files):
                l = len(files)
                tt_time = time.time() - tt_time_start
                logger.debug("total done batch_size=%s. total=%.2f avg_cost %.2f second (prep=%.2f, split=%.2f, pred=%.2f, stitch=%.2f, write=%.2f)",
                             l, tt_time, tt_time/l, tt[0]/l, tt[1]/l, tt[2]/l, tt[3]/l, tt[4]/l)

def dry_run(files):
    fgbg = cv2.BackgroundSubtractorMOG()
    for file in files:
        images, sz = pre_process(file, FLAGS.contrast_expand, FLAGS.remove_dream_rect_label, FLAGS.sp_tiff)
        logger.info("%s, sz=%s", file, sz)
        if True:
            #image_8bit = dc_utils.reduce_image_to_8_bit(image=images[0])
            image = images[0]
            fgmask = fgbg.apply(image)
            cv2.imwrite("1.tiff", fgmask)

def main (_):
    files = []
    for _ in range(FLAGS.repeat):
        files = files + FLAGS.filename
    if FLAGS.dry_run:
        dry_run(files)
    else:
        run_predict(files)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='PROG')
    parser.add_argument('filename', nargs='+', help='filename')
    parser.add_argument('model', help='model')
    parser.add_argument('--patch', default=0, type=int, help='split image to X by X patches')
    parser.add_argument('--patch-size', default=0, type=int, help='split image to X sized patches')
    parser.add_argument('--resize', default=0, type=int, help='resize image to X (long size)')
    parser.add_argument('--resize-ratio', default=None, type=float, help='resize image to ratio (i.e. 0.5)')
    parser.add_argument('--result-resize-ratio', default=None, type=float, help='resize result to ratio (i.e. 0.5)')
    parser.add_argument('--thread', default=0, type=int, help='number of threads')
    parser.add_argument('--repeat', default=1, type=int, help='repeat N times for performance test')
    parser.add_argument('--use_vgg', action='store_true', help='Use vgg')
    parser.add_argument("--adjust-color-profile", default=None, type=str, help='adjust color profile, e.g. v0')
    parser.add_argument('--contrast-expand', default='no', choices=['no', 'yes', 'sat'])
    parser.add_argument('--print-var', action='store_true', help='Print all variables')
    parser.add_argument('--sp-tiff', action='store_true', default=False)
    parser.add_argument('--dry-run', action='store_true', default=False, help='Dry run')
    parser.add_argument('--random-crash', action='store_true', default=False, help='Random crash')
    parser.add_argument('--remove-dream-rect-label', action='store_true', default=False, help='Remove dream rectangle label')
    parser.add_argument("--result-dir", default=os.getcwd())
    parser.add_argument("--result-postfix", default='_result')
    parser.add_argument('--write-original', action='store_true', default=False, help='Write original file')
    parser.add_argument('--real-channels', default=1, type=int, help='number of input channels')

    FLAGS = parser.parse_args()

    if FLAGS.thread == 0:
        import multiprocessing
        if multiprocessing.cpu_count() >= 8:
            FLAGS.thread = multiprocessing.cpu_count() / 2
        logger.info("uses threads=%s. cpu_count=%s", FLAGS.thread, multiprocessing.cpu_count())
    tf.app.run()

