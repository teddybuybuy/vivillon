import argparse
import logging
import json
import os
import sys

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

def _get_feature_from_file(filename, merge_with):
    with open(filename) as f:
        lines = f.readlines()

    # logger.info("printfeature %s\t%s\t%s%s", predict_type, item[0], item[1], s)
    features = merge_with
    for line in lines:
        l = line.split("printfeature ", 1)
        if len(l) <= 1:
            # mass log is special
            l = line.split('_masscrop.png\t', 1)
            if len(l) <= 1:
                continue
            # l is ['STDOUT: 2017-03-12T10:00:35.755118685Z 374537', '0\t67.4716\r\n']
            s = l[0].split(' ')
            subject_id = s[-1]
            items = ['mass', subject_id]
            items += l[1].split("\t")
        else:
            l = l[1]
            items = l.split("\t")
        predict_type = items[0]
        pid = items[1]
        _, filename = os.path.split(items[1])
        pid = filename.split('_')[0]
        cancer = items[2]
        if len(items) == 4:
            value = str(float(items[3]))
        else:
            value = []
            for i, _ in enumerate(items):
                if i < 3:
                    continue
                value.append(float(items[i]))
            value = json.dumps(value)
        if pid not in features:
            features[pid] = {}
        features[pid].update({predict_type: value})

    return features

def main(args):
    parser = argparse.ArgumentParser(description = "get features json from logfile")
    parser.add_argument("--input", required=True, type=str)
    parser.add_argument("--merge-with", default=None, type=str)
    parser.add_argument("--features-result", default="features_result.json", type=str)

    opts = parser.parse_args(args[1:])

    if opts.merge_with:
        with open(opts.merge_with, 'r') as m:
            merge_with = json.load(m)
    else:
        merge_with = {}

    features = _get_feature_from_file(opts.input, merge_with)

    with open(opts.features_result, 'w') as data:
        json.dump(features, data)

    return 0

if __name__ == '__main__':
    main(sys.argv)
