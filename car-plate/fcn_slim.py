import math
import numpy as np
import tensorflow as tf


class FcnSlim:
    def __init__(self):
        pass

    def build(self, rgb, train, dropout_rate, inchannels, num_classes):
        logits, score, params = FcnSlim._inference(images=rgb, train=train, dropout_rate=dropout_rate,
                                                   in_channels=inchannels, out_channels=num_classes)
        return logits

    @staticmethod
    def _cp_layer(bottom, scope, params, ksize, kstride, psize, pstride, ch_in, ch_out, relu=True):
        with tf.name_scope(scope):
            filters = tf.Variable(
                tf.truncated_normal(
                    [ksize, ksize, ch_in, ch_out],
                    dtype=tf.float32,
                    stddev=0.01),
                name='filters')
            out = tf.nn.conv2d(bottom, filters, [1, kstride, kstride, 1], padding="SAME")
            biases = tf.Variable(
                tf.constant(0.0, shape=[ch_out], dtype=tf.float32),
                trainable=True,
                name='bias')
            out = tf.nn.bias_add(out, biases)
            if relu:
                out = tf.nn.relu(out, name=scope)
            if psize:
                out = tf.nn.max_pool(out, ksize=[1, psize, psize, 1],
                                     strides=[1, pstride, pstride, 1],
                                     padding='SAME',
                                     name='pool')
            params.extend([filters, biases])
            return out
        pass

    @staticmethod
    def _get_deconv_filter(f_shape):
        width = f_shape[0]
        heigh = f_shape[0]
        f = math.ceil(width / 2.0)
        c = (2 * f - 1 - f % 2) / (2.0 * f)
        bilinear = np.zeros([f_shape[0], f_shape[1]])
        for x in range(width):
            for y in range(heigh):
                value = (1 - abs(x / f - c)) * (1 - abs(y / f - c))
                bilinear[x, y] = value
        weights = np.zeros(f_shape)
        for i in range(f_shape[2]):
            weights[:, :, i, i] = bilinear

        init = tf.constant_initializer(value=weights,
                                       dtype=tf.float32)
        return tf.get_variable(name="up_filter", initializer=init,
                               shape=weights.shape)

    @staticmethod
    def _inference(images, in_channels, out_channels, train, dropout_rate):
        params = []
        out = FcnSlim._cp_layer(images, "layer1", params, 5, 2, 2, 2, in_channels, 100)
        out = FcnSlim._cp_layer(out, "layer2", params, 5, 2, 2, 2, 100, 200)
        out = FcnSlim._cp_layer(out, "layer3", params, 3, 1, None, None, 200, 300)
        out = FcnSlim._cp_layer(out, "layer4", params, 3, 1, None, None, 300, 300)
        out = FcnSlim._cp_layer(out, "layer5", params, 3, 1, None, None, 300, 300)
        if train:
            out = tf.nn.dropout(out, dropout_rate, name='dropout')
        out = FcnSlim._cp_layer(out, "score", params, 1, 1, None, None, 300, out_channels, relu=False)
        score = out
        with tf.name_scope('upscale'):
            shape = tf.unpack(tf.shape(images))
            # print(shape.__class__)
            shape.pop()
            shape.append(tf.constant(out_channels, dtype=tf.int32))
            # print(len(shape))
            filters = FcnSlim._get_deconv_filter([31, 31, out_channels, out_channels])
            """
            weights = tf.truncated_normal(
                [31, 31, out_channels, out_channels],
                dtype=tf.float32,
                stddev=0.01)
            filters = tf.Variable(weights, name='filters')
            """
            logits = tf.nn.conv2d_transpose(out, filters, tf.pack(shape),
                                            [1, 16, 16, 1], padding='SAME', name='upscale')
            # do we want to add bias?
        return logits, score, params
