#!/usr/bin/env python

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import cv2
import os,sys
import numpy as np
import operator
import dicom
import glob
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('db', '', 'picpac db to histogram.')
flags.DEFINE_string('image_dir', '', 'directory to dicom files.')
flags.DEFINE_string('image', '', 'image to histogram')
flags.DEFINE_boolean('show', False, '')
flags.DEFINE_string('spec', '', 'target histogram')

def compute_histogram(image):
    #print(image.shape)
    #print(image.dtype)
    max_brightness = np.max(image)
    min_brightness = np.min(image)
    logger.debug("avearge=%s, min=%s, max=%s", np.average(image), min_brightness, max_brightness)

    # both range(), and histogram bins are exclusive for end, so +2.
    histogram = np.histogram(image, bins=range(0, np.max(image)+2))
    #histogram = np.histogram(image, bins='auto')
    return histogram[0].tolist()

def merge_histogram(h1, h2):
    if len(h1) > len(h2):
        h2 = h2 + ([0] * (len(h1) - len(h2)))
    elif len(h1) < len(h2):
        h1 = h1 + ([0] * (len(h2) - len(h1)))
    return map(operator.add, h1, h2)

def show_histogram(histogram):
    import matplotlib.pyplot as plt
    logger.debug("len=%s, histo=%s", len(histogram), histogram)
    if FLAGS.show:
        histogram[0] = 0 # too much 0
        plt.bar(range(len(histogram)), histogram)
        plt.show()

def linear_expansion(input, data_min, data_max, desired_min, desired_max):
    if input <= data_min:
        return desired_min
    if input >= data_max:
        return desired_max
    return int(1.0 * (input - data_min) * (desired_max - desired_min) / (data_max - data_min) + desired_min)

def histogram_expansion(images, histogram, saturate=True):
    # we don't consider channels, all channel use same scale.
    orig_max_brightness = np.max(images)
    origin_max = np.max(histogram)
    origin_min = np.min(histogram)
    num_pixels = images.size
    #print(np.sum(histogram), num_pixels)
    histogram[0] = 0;
    histogram[-1] = 0;
    num_pixels = np.sum(histogram)  # not saturate one.
    new_max = len(histogram) - 1
    try:
        while histogram[new_max] == 0:
            new_max -= 1;
        new_min = 0
        while histogram[new_min] == 0:
            new_min += 1;
        logger.debug("1. new_max=%s, new_min=%s, real_max=%s, num_pixels=%s",
                     new_max, new_min, orig_max_brightness, num_pixels)
        if saturate:
            t = 0;
            while t < num_pixels/500.0:
                t += histogram[new_max];
                new_max -= 1
            t = 0;
            while t < num_pixels/500.0:
                t += histogram[new_min];
                new_min += 1
            logger.debug("2. new_max=%s, new_min=%s", new_max, new_min)

        lut = np.asarray([linear_expansion(x, new_min, new_max, 0, 4095) for x in range(orig_max_brightness + 1)])
        """
        new_images = images
        new_images.astype('int16')

        for x in np.nditer(new_images, op_flags=['readwrite']):
            x[...] = lut[x]
        return new_images
        """
        return lut[images].astype('uint16')
    except Exception:
        logger.exception("cannot do expansion")
        return images.astype('uint16')

def histogram_specification(image, histogram):
    histogram[0] = 0 # too much 0
    h = compute_histogram(image)
    return histogram_specification_2(image, h, histogram)

def histogram_specification_2(image, histogram_src, histogram):
    histogram_src[0] = 0
    histogram[0] = 0 # too much 0
    # compute target cdf
    t_quantiles = np.cumsum(histogram).astype(np.float64)
    t_quantiles /= t_quantiles[-1]
    return histogram_specification_3(image, histogram_src, t_quantiles)

def histogram_specification_3(image, histogram_src, t_quantiles):
    histogram_src[0] = 0
    # compute source cdf
    s_quantiles = np.cumsum(histogram_src).astype(np.float64)
    s_quantiles /= s_quantiles[-1]

    # interpolate linearly to find the pixel values in the template image
    # that correspond most closely to the quantiles in the source image
    interp_t_values = np.interp(s_quantiles, t_quantiles, range(len(t_quantiles)))
    image = interp_t_values[image]
    image = image.astype('uint16')
    return image

def main (_):
    total = []
    equalized = []
    if FLAGS.db:
        seed = 1996
        config = dict(seed=seed,
                      loop=False,
                      shuffle=False,
                      reshuffle=False,
                      batch=1,
                      split=1,
                      split_fold=0,
                      annotate='json',
                      channels=3,
                      stratify=False,
                      #max_size=400,
                      channel_first=False # this is tensorflow specific
                                          # Caffe's dimension order is different.
                    )
        print("extracting histogram distribution from picpac db %s" % FLAGS.db)
        import picpac
        tr_stream = picpac.ImageStream(FLAGS.db,
                                       negate=False,
                                       perturb=False,
                                       **config)

        for images, labels, _ in tr_stream:
            histogram = compute_histogram(images.astype(int))
            total = merge_histogram(total, histogram)
            #equalized_img = cv2.equalizeHist(images)
            #histogram = compute_histogram(equalized_img.astype(int))
            #equalized = merge_histogram(equalized, histogram)

    elif FLAGS.image_dir:
        print(FLAGS.image_dir)
        for filepath_img in glob.glob(FLAGS.image_dir + "*.dcm"):
            print("processing image %s:" % filepath_img)
            dicom_content = dicom.read_file(filepath_img)
            images = dicom_content.pixel_array
            histogram = compute_histogram(images)
            total = merge_histogram(total, histogram)
    else:
        print(FLAGS.image)
        filename, file_extension = os.path.splitext(FLAGS.image)
        if file_extension == '.dcm':
            dicom_content = dicom.read_file(FLAGS.image)
            images = dicom_content.pixel_array
        elif file_extension == '.tiff' or file_extension == '.tif':
            images = cv2.imread(FLAGS.image, cv2.IMREAD_ANYCOLOR | cv2.IMREAD_ANYDEPTH)
        else:
            print('unknown image type')
            return

        if FLAGS.spec:
            histogram = np.load(FLAGS.spec)
            images = histogram_specification(images, histogram)
            output = filename +'_x.npy'
            np.save(output, images)
            #cv2.imwrite(output, images)

        else:
            #images = images >> 4
            total = compute_histogram(images)
            #equalized_image = images
            equalized_image = histogram_expansion(images, total)
            print('equal done')
            #cv2.normalize(images, equalized_image, 0, 4095, cv2.NORM_MINMAX);
            #print(equalized_image)
            equalized = compute_histogram(equalized_image)
            output = np.concatenate((images, equalized_image), axis=1)
            output = output << 4
            output = output.astype('uint16')
            print(output.dtype)
            print(output.shape)
            cv2.imwrite('output.tiff', output)
            show_histogram(equalized)
            show_histogram(total)

if __name__ == '__main__':
    tf.app.run()

