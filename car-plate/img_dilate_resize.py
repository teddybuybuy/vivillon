import cv2, os
import numpy as np

from optparse import OptionParser

def main(opts, args):
    #target_size = 1024.0
    target_size = 512.0
    kernel = np.ones((9,9), np.uint8)
    for fname in args:
        print fname
        img = cv2.imread(fname, 0)
        ori_height, ori_width = img.shape
        ratio = float(target_size) / max(ori_height, ori_width)

        height = int(ori_height * ratio + 0.5)
        width = int(ori_width * ratio + 0.5)
        img_dilation = cv2.dilate(img, kernel, iterations=1)
        #both = np.concatenate((img, img_dilation), axis=1)

        resized_img = cv2.resize(img_dilation, (width, height))

        #make a square image with background filled with 0
        if height > width:
            padding = (height - width) / 2
            
            square_img = cv2.copyMakeBorder(resized_img, top=0, bottom=0, left=padding,
                                            right= (height-width - padding), borderType=cv2.BORDER_CONSTANT, value=0)
        else:
            # unlikely, do nothing for now..
            square_img = resized_img
        target_fname = os.path.join(opts.target_dir, fname)
        #target_fname = "dil_" + fname
        #cv2.imwrite(target_fname, img_dilation)
        cv2.imwrite(target_fname, square_img)
        #cv2.imwrite(target_fname, img)
        
if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-d", "--target-dir", dest="target_dir", default="converted",
                      help="", metavar="FILE")
    (options, args) = parser.parse_args()
    main(options, args)
