import argparse
import errno
import os
import subprocess
import sys
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)


import patients_csv_parser
from patients_csv_parser import CANCER_NEGATIVE, CANCER_POSITIVE, CANCER_UNKNOWN

BATCH_SIZE = 320

def call_prediction_command(files, result_dir, result_postfix, model, predict_bin, random_crash):
    cmd = ['python', predict_bin]
    if random_crash:
        cmd += ['--random-crash']
    cmd += ['--patch', '1152']
    cmd += ['--result-resize-ratio', '0.25']
    cmd += ['--result-postfix', result_postfix, '--result-dir', result_dir]
    # cmd += ['--contrast-expand']
    cmd += [x[1] for x in files]
    cmd += [model]
    logger.info("cmd=%s", cmd)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    rc = p.returncode
    logger.info("ret: %s out: %s", rc, out)
    logger.info("error: %s", err)
    if rc != 0:
        sys.exit(rc)

def do_predict(src_filelist, src_filelist_base, src_file_postfix,
               result_dir, result_csv, result_postfix, model, predict_bin, random_crash, max_predict):

    with open(src_filelist) as f:
        filelist = f.readlines()
    filelist = [x.split('\t') for x in filelist]

    with open(result_csv, "w") as outfile:
        # outfile.write("fname\tcancer\n")
        l = []
        predicted = 0
        for item in filelist:
            file = item[0].strip()
            has_cancer = int(item[1].strip())
            file_id, file_extension = os.path.splitext(file)
            file = file_id + src_file_postfix
            if src_filelist_base is not None:
                file = os.path.join(src_filelist_base, file)
            l.append((file_id, file))
            result_file = os.path.abspath(os.path.join(result_dir, file_id + result_postfix + '.png'))
            outfile.write("{}\t{}\n".format(result_file, has_cancer))

            predicted += 1
            if max_predict and predicted >= max_predict:
                logger.info("reach max_predict %s, exit.", max_predict)
                break
            if len(l) >= BATCH_SIZE:
                call_prediction_command(files=l, result_dir=result_dir,
                                        result_postfix=result_postfix, model=model,
                                        predict_bin=predict_bin,
                                        random_crash=random_crash)
                l = []
        if len(l) > 0:
            call_prediction_command(files=l, result_dir=result_dir,
                                    result_postfix=result_postfix, model=model,
                                    predict_bin=predict_bin,
                                    random_crash=random_crash)
            l = []

def main(args):
    parser = argparse.ArgumentParser(description="calci predict")
    parser.add_argument("--src-filelist", required=True, help='file that contains a list of files need to be processed')
    parser.add_argument("--src-filelist-base", required=True, help='base dir of the files in the list')
    parser.add_argument("--src-file-postfix", required=True)
    parser.add_argument("--result-dir", required=True)
    parser.add_argument("--result-csv", required=True)
    parser.add_argument("--result-postfix", required=True)
    parser.add_argument("--model", required=True)
    parser.add_argument("--predict-bin", required=True)
    parser.add_argument('--random-crash', action='store_true', default=False, help='Random crash')
    parser.add_argument("--max-predict", type=int, default=0)

    opts = parser.parse_args(args[1:])

    do_predict(src_filelist=opts.src_filelist, src_filelist_base=opts.src_filelist_base,
               src_file_postfix=opts.src_file_postfix,
               result_dir=opts.result_dir, result_csv=opts.result_csv,
               result_postfix=opts.result_postfix, model=opts.model,
               predict_bin=opts.predict_bin, random_crash=opts.random_crash, max_predict=opts.max_predict)

    return 0


if __name__ == '__main__':
    main(sys.argv)
