#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os,sys
import argparse
import logging
import subprocess

import time
import concurrent.futures
import threading
import importlib
import simplejson

patients_csv_parser = importlib.import_module("car-plate.patients_csv_parser")

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)


def xg_test(filename, cmd_sh, type):
    test_score_total = 0
    train_score_total = 0
    count = 0
    with open(os.path.join(filename, "features_result.json")) as data:
        p = simplejson.load(data)
        testset_size = len(p)
    for seed in range(FLAGS.seed_min, FLAGS.seed_max):
        count += 1
        tmp_model = '/tmp/xgbttt'
        if FLAGS.output:
            tmp_model = "{output}/xgb-{type}-{seed}-{testpart}.model".format(output=FLAGS.output, type=type, seed=seed, testpart=FLAGS.testpart)
        cmd = [cmd_sh, filename, tmp_model, str(seed), '/tmp', str(FLAGS.testpart)]
        try:
            my_env = os.environ
            # logger.error("cmd=%s", cmd)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=my_env)
            out, err = p.communicate()
            if p.returncode != 0:
                if FLAGS.show_error:
                    logger.info("preprocess done: cmd=%s err=%s", cmd, err)
                    break
                pass
            test_score = 0
            train_score = 0
            for line in out.splitlines():
                s = line.split("AUC Score (Test): ")
                if len(s) == 2:
                    test_score = float(s[1])
                s = line.split("AUC Score (Train): ")
                if len(s) == 2:
                    train_score = float(s[1])
            test_score_total += test_score
            train_score_total += train_score
            print("seed={} size={} testpart={}  train-score={} test-score={}".format(seed, testset_size, FLAGS.testpart, train_score, test_score))
        except Exception:
            logger.exception("cannot popen")
        try:
            if not FLAGS.output:
                try:
                    os.remove(tmp_model)
                except Exception:
                    pass
        except:
            logger.exception("cannot remove %s", tmp_model)
    print("======file={} cmd={} size={} testpart={} train-score={} test-score={}".format(filename, cmd_sh, testset_size, FLAGS.testpart, train_score_total/count, test_score_total/count))

def prepare_tsv(path):
    # images_crosswalk.tsv
    # exams_metadata.tsv
    crosswalk = os.path.join(path, "images_crosswalk.tsv")
    metadata = os.path.join(path, "exams_metadata.tsv")
    if os.path.exists(crosswalk) or os.path.exists(metadata):
        return
    crosswalk_total = crosswalk + "-full"
    metadata_total = metadata + "-full"
    if not (os.path.exists(crosswalk_total) and os.path.exists(metadata_total)):
        return

    patient_ids = set()

    patients, img_dict = patients_csv_parser.Patient.get_patients_and_images_from_csv(crosswalk_total, metadata, None, None)

    with open(os.path.join(path, "features_result.json")) as data:
        p = simplejson.load(data)
        for k in p:
            pid = img_dict[k].patient_id
            patient_ids.add(pid)

    patients_csv_parser.rewrite_csv(crosswalk_total, crosswalk, metadata_total, metadata, list(patient_ids))


def main():
    cmds = []
    if not FLAGS.no_sc1:
        cmds.append(("./xgb-train-sc1.sh", "sc1"))
    if not FLAGS.no_sc2:
        cmds.append(("./xgb-train-sc2.sh", "sc2"))
    for cmd in cmds:
        print("######### ", cmd[0])
        for filename in FLAGS.filename:
            full = os.path.join(filename, FLAGS.postfix)
            if not os.path.exists(full):
                full = filename
            print("#### ", full)
            prepare_tsv(full)
            xg_test(full, cmd[0], cmd[1])

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='PROG')
    parser.add_argument('filename', nargs='+', help='filename')
    parser.add_argument('--seed-min', default=880, type=int)
    parser.add_argument('--seed-max', default=900, type=int)
    parser.add_argument('--testpart', default=0.33, type=float)
    parser.add_argument('--no-sc1', action='store_true', default=False)
    parser.add_argument('--no-sc2', action='store_true', default=False)
    parser.add_argument('--show-error', action='store_true', default=False)
    parser.add_argument('--postfix', default='DreamPreprocessedData/dataset', type=str)
    parser.add_argument('--output', default=None, type=str)

    FLAGS = parser.parse_args()

    main()

