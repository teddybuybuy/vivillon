#!/bin/bash
set -x
set -e
#
# This script is modified from the slim/scripts/finetune_inceptionv3_on_flowers.sh
# Where the pre-trained InceptionV3 checkpoint is saved to.
# XXX: Also need to pack data/model-203999 to the container!

HOME_BASE_DIR=/src

# Where the training (fine-tuned) checkpoint and logs will be saved to.
#PREPROCESS_BASE_DIR=/preprocessedData

# Where the dataset is saved to.
MY_ID=$1
FILE_LIST=$2
FILE_SRC_DIR=$3
DATASET_DIR=$4
DATASET_DIR_PRIVATE=$4_${MY_ID}
PREPROCESS_BASE_DIR=$5
SLIM_FILE_LIST=$6

export CUDA_VISIBLE_DEVICES=${MY_ID}

# Run once
mkdir -p ${DATASET_DIR_PRIVATE}

if [ ! -z ${DREAM_TEST} ]; then
    echo "DREAM_TEST is set"
    MAX_CROP_IMGS=20
else
    # no limit
    MAX_CROP_IMGS=10000000
fi

echo "sanity check"
echo "debugging: output from ${FILE_LIST}"
head -10 ${FILE_LIST}

cd ${HOME_BASE_DIR}/car-plate/
time python batch-mass-predict.py --use_resized_400_img true --write_cropped_img false --input_list_file ${FILE_LIST} --tsv_output_dir ${DATASET_DIR_PRIVATE} --output_dir ${DATASET_DIR} --train_data ${DATASET_DIR} --model data_mass/model-399999 --mass_confidence_thresh 1.0 --max_cropped_images ${MAX_CROP_IMGS}
echo mass done
time python batch-calci-predict.py --src-file-postfix .tiff --src-filelist ${FILE_LIST} ${RANDOM_CRASH} --src-filelist-base ${DATASET_DIR} --result-dir ${DATASET_DIR} --result-postfix _calci --model data_calci/model-399999 --predict-bin fcn-predict.py --result-csv ${DATASET_DIR_PRIVATE}/calci-result.csv --max-predict ${MAX_CROP_IMGS}
echo calci done
time python batch-simple-predict.py --src-csv ${DATASET_DIR_PRIVATE}/calci-result.csv --predict-type calci
echo calci_simple done
cd -

echo "debugging: use inception model to do prediction"
DATASET_IMPORT_DIR=${DATASET_DIR_PRIVATE}/import_data
mkdir -p ${DATASET_DIR}
MODEL_DIR=${HOME_BASE_DIR}/car-plate/data_slim
META_FULL=${SLIM_FILE_LIST}
TRAIN_SPLIT_FULL=${DATASET_DIR_PRIVATE}/train_zero.tsv
PREDICT_FULL=${DATASET_DIR_PRIVATE}/slim_glob.json

cd ${HOME_BASE_DIR}/slim/

# Needed to create an empty training file.
touch ${TRAIN_SPLIT_FULL}
python download_and_convert_data.py --dataset_name=dreamcancer --dataset_dir=${DATASET_IMPORT_DIR} --meta_fname=${META_FULL} \
       --input_data_dir=${DATASET_DIR} --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${META_FULL}

echo "Done download_and_convert_data"

python eval_image_classifier.py \
  --checkpoint_path=${MODEL_DIR} \
  --eval_dir=${MODEL_DIR} \
  --dataset_name=dreamcancer \
  --dataset_split_name=validation \
  --dataset_dir=${DATASET_IMPORT_DIR} \
  --num_preprocessing_threads=1 \
  --meta_fname=${META_FULL} \
  --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${META_FULL} \
  --predict_fname=${PREDICT_FULL} \
  --model_name=inception_v4
cd -

echo sub done
