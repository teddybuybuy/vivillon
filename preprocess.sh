#!/bin/bash
set -x
set -e

if [ -z "$1" ]; then
    META_DIR=/metadata
else
    META_DIR=$1
fi

if [ -z "$2" ]; then
    DATASOURCE=/trainingData
else
    DATASOURCE=$2
fi

if [ -z "$3" ]; then
    PREPROCESS_BASE_DIR=/preprocessedData
else
    PREPROCESS_BASE_DIR=$3
fi

if [ -z "$4" ]; then
    IGNORE_PATIENTS_JSON=ignore_patients_json
else
    IGNORE_PATIENTS_JSON=$4
fi

HOME_BASE_DIR=/src
DATASET_DIR=${PREPROCESS_BASE_DIR}/dataset

df

mkdir -p ${DATASET_DIR}

cd ${HOME_BASE_DIR}/car-plate

# just to make xgboost easier
cp ${META_DIR}/images_crosswalk.tsv ${DATASET_DIR}/images_crosswalk.tsv-full
if [ -f ${META_DIR}/exams_metadata.tsv ]; then
    cp ${META_DIR}/exams_metadata.tsv ${DATASET_DIR}/exams_metadata.tsv-full
fi

MAX_PREPROCESSING=4000000
echo MAX_PREPROCESSING=${MAX_PREPROCESSING}
time python patients_csv_parser.py --csv1 ${META_DIR}/images_crosswalk.tsv --csv2 ${META_DIR}/exams_metadata.tsv --ignore-json ${IGNORE_PATIENTS_JSON} --list-files-output ${PREPROCESS_BASE_DIR}/prep.tsv --list-files-train ${PREPROCESS_BASE_DIR}/prep_train.tsv --list-files-predict ${PREPROCESS_BASE_DIR}/prep_predict.tsv  --max-files ${MAX_PREPROCESSING}
#echo "stop now to re-generate the prep_train and prep_predict.tsv!"
#exit 0
head -${MAX_PREPROCESSING} ${PREPROCESS_BASE_DIR}/prep.tsv > ${PREPROCESS_BASE_DIR}/prep_head.tsv
time python dream_dcm_to_tiff.py --src-filelist ${PREPROCESS_BASE_DIR}/prep_head.tsv --src-filelist-base ${DATASOURCE} ${MAIN_RESIZE_OPT} --resize 400 r100 c400 --resize-info-output ${DATASET_DIR}/resize --contrast-expand sat --remove-dream-rect-label --dest-dir ${DATASET_DIR} --dcm-info-output ${DATASET_DIR}/dcm_info.json
cd -

if [ -z ${NUM_GPU_DEVICES} ]; then
    MAX_GPU_ID=0
else
    MAX_GPU_ID=1  # better to use the real number.
fi
echo MAX_GPU_ID=${MAX_GPU_ID}

# disable multi GPU path
# MAX_GPU_ID=0

# XXX very hacky, only handle max of 2 GPU.
cd ${HOME_BASE_DIR}
if [ "$MAX_GPU_ID" == "0" ]; then
    python split_csv.py --input ${PREPROCESS_BASE_DIR}/prep.tsv --output ${PREPROCESS_BASE_DIR}/prep.tsv --max-gpu 0
    python split_csv.py --input ${PREPROCESS_BASE_DIR}/dataset/resize_c400.csv --output ${PREPROCESS_BASE_DIR}/resize_c400.csv --max-gpu 0
fi
if [ "$MAX_GPU_ID" == "1" ]; then
    python split_csv.py --input ${PREPROCESS_BASE_DIR}/prep.tsv --output ${PREPROCESS_BASE_DIR}/prep.tsv --max-gpu 1
    python split_csv.py --input ${PREPROCESS_BASE_DIR}/dataset/resize_c400.csv --output ${PREPROCESS_BASE_DIR}/resize_c400.csv --max-gpu 1
fi
cd -

echo "MAX_GPU_ID: $MAX_GPU_ID"

for i in `seq 0 ${MAX_GPU_ID}`; do
    mkdir -p ${DATASET_DIR}_$i/
    touch ${DATASET_DIR}_$i/crop_meta.tsv
    touch ${DATASET_DIR}_$i/calci-result.csv
    touch ${DATASET_DIR}_$i/slim_glob.json.tsv
    /src/preprocess_sub.sh $i ${PREPROCESS_BASE_DIR}/prep_$i.tsv ${DATASOURCE} ${DATASET_DIR} ${PREPROCESS_BASE_DIR} ${PREPROCESS_BASE_DIR}/resize_c400_$i.csv&
    pids="$pids $!"
done

for pid in $pids; do
    wait $pid || let "RESULT=1"
    echo `date` $pid done
done

if [ "$RESULT" == "1" ];
then
    exit 1
fi

# merge them back here.
for i in `seq 0 ${MAX_GPU_ID}`; do
    echo "Merging dataset $i"
    ls -l ${DATASET_DIR}_$i/
    cat ${DATASET_DIR}_$i/crop_meta.tsv >> ${DATASET_DIR}/crop_meta.tsv
    cat ${DATASET_DIR}_$i/calci-result.csv >> ${DATASET_DIR}/calci-result.csv
    cat ${DATASET_DIR}_$i/slim_glob.json.tsv >> ${DATASET_DIR}/slim_glob.json.tsv
done

cat ${DATASET_DIR}/crop_meta.tsv
cat ${DATASET_DIR}/calci-result.csv
cat ${DATASET_DIR}/slim_glob.json.tsv

cd ${HOME_BASE_DIR}/car-plate

ppids=
time python batch-simple-predict.py --src-csv ${DATASET_DIR}/resize_r100.csv --result-csv ${DATASET_DIR}/bravg.csv --predict-type bravg&
ppids="$ppids $!"


for pid in $ppids; do
    wait $pid || let "RESULT=1"
    echo `date` $pid done
done

if [ "$RESULT" == "1" ];
then
    exit 1
fi

cd -

ls -l ${DATASET_DIR}
df

if [ -z "$5" ]; then
    # full processing, get meta
    echo full processing
    touch ${DATASET_DIR}/dcm_info.json
    cd ${HOME_BASE_DIR}/car-plate
    python process_patients_images.py --extract-features-from-dir-only ${DATASET_DIR} --result-json ${DATASET_DIR}/features_result.json
    cd -
    cd ${HOME_BASE_DIR}/
    python xgb-testloop.py --testpart 0.33 ${DATASET_DIR}
    python xgb-testloop.py --testpart 0.10 ${DATASET_DIR}
    python xgb-testloop.py --testpart 0.01 ${DATASET_DIR}
    cd -
else
    echo $5
fi
