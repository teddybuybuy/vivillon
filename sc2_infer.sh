#!/bin/bash
set -x
set -e
#
# This script is modified from the slim/scripts/finetune_inceptionv3_on_flowers.sh
# Where the pre-trained InceptionV3 checkpoint is saved to.
# XXX: Also need to pack data/model-203999 to the container!

HOME_BASE_DIR=/src

cd ${HOME_BASE_DIR}/car-plate
python process_patients_images.py

python inference.py \
  --image_crosswalk_csv="/metadata/images_crosswalk.tsv" \
  --exams_metadata_csv="/metadata/exams_metadata.tsv" \
  --model="xgb_model_sc2.dat"

cat /output/predictions.tsv
