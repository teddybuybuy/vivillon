#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import sys
import glob
import logging
import simplejson as json
import subprocess
import picpac
import cv2
import numpy as np
import ljpeg
import argparse
import time

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)
RESIZE = 2

from optparse import make_option

# set DOWNSIZE = None
# to generate training data
# DOWNSIZE is for generating data to be browsed online
DOWNSIZE = 0.5


LABEL_PATHO_NONE = 0
LABEL_PATHO_UNPROVEN = 1
LABEL_PATHO_BENIGN = 2
LABEL_PATHO_MALIGNANT = 3
LABEL_PATHO_MAX = 4

LABEL_LESION_NONE = 0
LABEL_LESION_OTHER = 1
LABEL_LESION_MASS = 2
LABEL_LESION_DISTORTION = 3
LABEL_LESION_CALCI_OTHER = 4
LABEL_LESION_CALCI_CLUSTER = 5
LABEL_LESION_CALCI_SEGMENT = 6
LABEL_LESION_CALCI_REGIONAL = 7
LABEL_LESION_CALCI_LINEAR = 8
LABEL_LESION_MAX = 9

D_X = [0, 1, 1, 1, 0, -1, -1, -1]
D_Y = [-1, -1, 0, 1, 1, 1, 0, -1]

def _mkdir_recursive(path):
    sub_path = os.path.dirname(path)
    if not os.path.exists(sub_path):
        _mkdir_recursive(sub_path)
    if not os.path.exists(path):
        os.mkdir(path)

def make_poly (line, H, W, label):
    E = []
    for x in line.strip().split(' '):
        try:
            E.append(int(x))
        except:
            pass
        pass
    x, y = E[:2]
    cc = []
    for d in E[2:-1]:
        cc.append({'x':1.0 * float(x) / W, 'y':1.0 * y / H})
        if d == '#':
            break
        x += D_X[d]
        y += D_Y[d]
        pass
    return {'type':'polygon', 'label': label, 'geometry':{'points': cc}}


label_stats = []
for li in [LABEL_PATHO_BENIGN, LABEL_PATHO_MALIGNANT, LABEL_PATHO_UNPROVEN, LABEL_PATHO_NONE]:
    a = []
    for lj in [LABEL_LESION_NONE, LABEL_LESION_OTHER, LABEL_LESION_MASS, LABEL_LESION_DISTORTION,
               LABEL_LESION_CALCI_OTHER, LABEL_LESION_CALCI_CLUSTER,
               LABEL_LESION_CALCI_SEGMENT, LABEL_LESION_CALCI_REGIONAL, LABEL_LESION_CALCI_LINEAR]:
        a.append(0)
    label_stats.append(a)

def labels_to_label(label_patho, label_lesion):
    return label_lesion * LABEL_PATHO_MAX + label_patho

def label_to_labels(label):
    label_patho = label % LABEL_PATHO_MAX
    label_lesion = label // LABEL_PATHO_MAX
    return label_patho, label_lesion

def get_label(patho, lesion):
    if patho == 'BENIGN' or patho == 'BENIGN_WITHOUT_CALLBACK':
        label_patho = LABEL_PATHO_BENIGN
    elif patho == 'MALIGNANT':
        label_patho = LABEL_PATHO_MALIGNANT
    elif patho == 'UNPROVEN':
        label_patho = LABEL_LESION_OTHER
    else:
        assert False, patho
    if len(lesion) != 1:
        label_lesion = LABEL_LESION_OTHER
    else:
        lesion_type = lesion[0].split(' ')
        assert lesion_type[0] == 'LESION_TYPE', str(lesion_type)
        assert len(lesion_type) > 1, str(lesion_type)
        if lesion_type[1] == 'CALCIFICATION':
            '''
            TYPE:
            AMORPHOUS
            PLEOMORPHIC
            PUNCTATE
            FINE_LINEAR_BRANCHING
            ROUND_AND_REGULAR
            SKIN
            LARGE_RODLIKE
            VASCULAR
            COARSE
            LUCENT_CENTER
            EGGSHELL
            '''
            label_lesion = None
            for i, c in enumerate(lesion_type):
                logger.debug("i=%s, c=%s", i, c)
                if c == 'DISTRIBUTION':
                    if len(lesion_type) <= i + 1:
                        label_lesion = LABEL_LESION_CALCI_OTHER
                    else:
                        l = lesion_type[i + 1]
                        if l == 'CLUSTERED':
                            label_lesion = LABEL_LESION_CALCI_CLUSTER
                        elif l == 'SEGMENTAL':
                            label_lesion = LABEL_LESION_CALCI_SEGMENT
                        elif l == 'REGIONAL':
                            label_lesion = LABEL_LESION_CALCI_REGIONAL
                        elif l == 'LINEAR':
                            label_lesion = LABEL_LESION_CALCI_LINEAR
                        elif l == 'CLUSTERED-SEGMENTAL':
                            label_lesion = LABEL_LESION_CALCI_OTHER
                        elif l == 'REGIONAL-REGIONAL':
                            label_lesion = LABEL_LESION_CALCI_OTHER
                        elif l == 'DIFFUSELY_SCATTERED':
                            label_lesion = LABEL_LESION_CALCI_OTHER
                        elif l == 'CLUSTERED-LINEAR':
                            label_lesion = LABEL_LESION_CALCI_OTHER
                        elif l == 'LINEAR-SEGMENTAL':
                            label_lesion = LABEL_LESION_CALCI_OTHER
                        elif l == 'N/A':
                            label_lesion = LABEL_LESION_CALCI_OTHER
                        else:
                            assert False, str(l)
                    break
            if label_lesion is None:
                assert False, str(lesion_type)
        elif lesion_type[1] == 'MASS':
            '''
            MARGINS:
            CIRCUMSCRIBED
            SPICULATED
            MICROLOBULATED
            ILL_DEFINED
            OBSCURED

            SHAPE:
            ARCHITECTURAL_DISTORTION
            LOBULATED
            OVAL
            ROUND
            IRREGULAR
            '''
            label_lesion = LABEL_LESION_MASS
            for i, c in enumerate(lesion_type):
                logger.debug("i=%s, c=%s", i, c)
                if c == 'SHAPE':
                    if len(lesion_type) <= i + 1:
                        label_lesion = LABEL_LESION_MASS
                    else:
                        l = lesion_type[i + 1]
                        if l == 'ARCHITECTURAL_DISTORTION':
                            label_lesion = LABEL_LESION_DISTORTION
        elif lesion_type[1] == 'OTHER':
            label_lesion = LABEL_LESION_OTHER
        else:
            assert False, str(lesion_type)

    global label_stats
    label_stats[label_patho][label_lesion] += 1
    return labels_to_label(label_patho=label_patho, label_lesion=label_lesion)

def import_overlay (path, rows, cols):
    #image_patho = NORMAL
    total_labels = 0
    n = 0
    with open(path, 'r') as f:
        total = None
        while True:
            l = f.readline()
            if not l:
                break
            l = l.strip().split(' ')
            if len(l) < 2: 
                continue
            if l[0] == 'TOTAL_ABNORMALITIES':
                total = int(l[1])
            elif l[0] == 'ABNORMALITY':
                break
        assert total >= 1
        all_shapes = []
        for xx in range(total):
            # read one abnormality
            lesion = []
            assessment = None
            subtlety = None
            patho = None
            outlines = None
            while True:
                line = f.readline()
                if not line:
                    break
                line = line.strip()
                l = line.split(' ', 1)
                if len(l) == 0:
                    continue
                if l[0] == 'BOUNDARY':
                    n += 1
                    break
                if len(l) < 2:
                    continue
                if l[0] == 'ASSESSMENT':
                    assessment = float(l[1])
                elif l[0] == 'SUBTLETY':
                    subtlety = float(l[1])
                elif l[0] == 'PATHOLOGY':
                    patho = l[1]
                elif l[0] == 'TOTAL_OUTLINES':
                    outlines = int(l[1])
                elif l[0] == 'LESION_TYPE':
                    lesion.append(line)
                pass
            assert outlines != None
            # read boundaries
            m = 0   # outlines actually found
            shapes = []
            label = 0
            if patho == 'BENIGN' or patho == 'BENIGN_WITHOUT_CALLBACK':
                label = LABEL_PATHO_BENIGN
            elif patho == 'MALIGNANT':
                label = LABEL_PATHO_MALIGNANT
            elif patho == 'UNPROVEN':
                label = LABEL_PATHO_UNPROVEN
            else:
                assert False, '{} is not handled yet'.format(patho)    # handle new pathology
            label = get_label(patho, lesion)
            while True:
                # read one outline
                l = f.readline().strip()
                assert l
                shapes.append(make_poly(l, rows, cols, label))
                m += 1
                l = f.readline()
                if (not l) or (not "CORE" in l):
                    break
                pass
            count = len(shapes)
            all_shapes.append(shapes)
            #shape = json.dumps({'shapes': shapes})
            if m != outlines:
                logging.warning('missing outline: %s', path)
            logger.debug('    assess:%s, subtlety:%s, pathology:%s, shapes:%d lesion:%s',
                         assessment, subtlety, patho, count, ','.join(lesion))
            total_labels += 1
            # read till next abnormality
            if 'ABNORMALITY' in l:
                continue
            while True:
                l = f.readline()
                if not l:
                    break
                if 'ABNORMALITY' in l:
                    break
                pass
            pass # for each abnormalty
        if total != n:
            logging.warning('missing abnormalty: %s', path)
        pass # with open

    shapes = []
    for s in all_shapes:
        shapes.extend(s)
    return {'shapes': shapes}, total_labels

def filter_calci(anno, total_labels, calci):
    ret = []
    t = 0
    shapes = anno['shapes']
    for a in shapes:
        label = a['label']
        label_patho, label_lesion = label_to_labels(label)
        keep = False
        if calci is None:
            keep = True
        else:
            if label_lesion in [LABEL_LESION_OTHER, LABEL_LESION_MASS, LABEL_LESION_DISTORTION, LABEL_LESION_NONE]:
                keep = not calci
            else:
                keep = calci
        if keep:
            ret.append(a)
            t += 1
        else:
            logger.debug("drop %s %s because calci=%s", label, label_lesion, calci)
    return {'shapes':ret}, t

def import_image (db, base, data_base, name, meta, db_path, relative_path_base,
                  dry_run, down_size, save_label_jpg, calci):
    start_time = time.time()
    overlay = (meta[-1] == 'OVERLAY')

    name = name + '.' + meta[0]

    path = os.path.join(data_base, name + '.LJPEG')
    opath = os.path.join(base, name + '.OVERLAY')
    if not os.path.exists(path):
        path = os.path.join(data_base, name + '.tiff')
        if not os.path.exists(path):
            logging.warn('missing TIFF and LJPEG %s' % path)
            return 0
    if overlay and not os.path.exists(opath):
        logging.warn('missing OVERLAY %s' % opath)
        overlay = False
    image = None
    nc = meta[0].split('_')
    side = nc[0]
    view = nc[1]
    rows = int(meta[2])
    cols = int(meta[4])
    depth = int(meta[6])
    resolution = float(meta[8])
    logger.debug('  side:%s view:%s size:%dx%d depth:%d resolution:%f', side, view, rows, cols, depth, resolution)
    if depth != 12:
        logging.warning('special: DEPTH %s != 12: %s', depth, path)
    else:
        # logging.debug('12bits: DEPTH %s == 12: %s' , depth, path)
        pass
    if dry_run:
        pass
    else:
        try:
            if path.endswith('.LJPEG'):
                logger.debug("load ljpeg %s", path)
                image = ljpeg.read(path) #cv2.imread(path)
            else:
                logger.debug("load tiff %s", path)
                image = cv2.imread(path, -1)
            if image.shape == (rows, cols):
                pass
            elif image.shape == (cols, rows):
                #logging.warn("transposing image (%dx%d): %s" % (rows, cols, path))
                image = image.reshape((rows, cols))
                pass
            else:
                logging.warning("skipping bad image shape=%s (%dx%d): %s", image.shape, rows, cols, path)
                image = None
                pass
            if not image is None:
                if down_size > 0:
                    logger.debug("resize to %s", down_size)
                    image = cv2.resize(image, None, fx=down_size, fy=down_size)
                # sys.stdout.flush()
            # sys.stdout.flush()
            if image is not None:
                logger.debug("encode to tiff string")
                image_str = cv2.imencode('.tiff', image)[1].tostring()
        except Exception as e:
            logger.exception("cannot read %s", path)
            pass
    anno = '{}'
    if overlay:
        logger.debug("import_overlay")
        anno, total_labels = import_overlay(opath, rows, cols)
        anno, total_labels = filter_calci(anno, total_labels, calci)

        if save_label_jpg:
            filebase, ext = os.path.splitext(path)
            jpeg1 = filebase + '.jpg'
            jpeg2 = filebase + '.jpeg'
            image1 = (image / 16).astype('int32')
            image2 = (image / 16).astype('int32')
            if down_size == 0:
                down_size = 1
            for shape in anno['shapes']:
                if shape['type'] == 'polygon':
                    points = shape['geometry']['points']
                    pts = np.array([[point['x'] * cols * down_size, point['y'] * rows * down_size] for point in points], np.int32)
                    pts = pts.reshape((-1, 1, 2))
                    cv2.polylines(image2, pts=[pts], isClosed=True, color=(255, 255, 155), thickness=4)
                elif shape['type'] == 'point':
                    point = shape['geometry']
                    cv2.circle(image2, center=(int(point['x'] * cols), int(point['y'] * rows)), radius=10,
                               color=(255, 0, 0), thickness=1)
                else:
                    assert False, str(shape)
            if len(anno['shapes']):
                cv2.imwrite(jpeg1, image1)
                cv2.imwrite(jpeg2, image2)
        anno = json.dumps(anno)

    else:
        total_labels = 0
    if (not image_str is None) and (not dry_run):
        if (not calci is None) and (total_labels > 0):
            logger.debug("db.append")
            db.append(image_str, anno)
        else:
            logger.debug("no db.append")
    else:
        logger.debug("no db.append")
    end_time = time.time()
    logger.info("total_labels=%s cost time=%.2f", total_labels, end_time - start_time)

    return total_labels

def import_case (db, case, data_case, db_path, relative_path, dry_run, down_size, save_label_jpg, calci):
    base = os.path.dirname(case)
    data_base = os.path.dirname(data_case)
    relative_path_base = os.path.dirname(relative_path)
    with open(case, 'r') as f:
        name = None
        age = None
        density = None
        for l in f:
            try:
                l = l.strip().split(' ')
                if len(l) == 0:
                    continue
                if 'SEQUENCE' in l[0]:
                    break
                if l[0] == 'filename':
                    name = l[1]
                if l[0] == 'PATIENT_AGE':
                    if len(l) < 2:
                        logging.warn('%s: missing age' % case)
                    else:
                        age = float(l[1])
                elif l[0] == 'DENSITY':
                    if len(l) < 2:
                        logging.warn('%s: missing density' % case)
                        density = float('nan')
                    else:
                        density = float(l[1])
            except:
                logging.warn('%s: bad line' % case)
            pass
        assert name != None
        name = name.replace('-', '_')
        # images and overlays
        patient = None
        logger.debug('patient: %s age:%s density:%.4f', name, age, density)
        processed = 0
        total_labels = 0
        for l in f:
            l = l.strip().split(' ')
            if len(l) < 9:
                if len(l) > 0:
                    logging.warn('bad image line: %s' % base)
                continue
            processed += 1
            total_labels += import_image(db, base, data_base, name, l, db_path,
                                         relative_path_base, dry_run=dry_run,
                                         down_size=down_size, save_label_jpg=save_label_jpg,
                                         calci=calci)
        pass
    return processed, total_labels


def scan_patho (db_path, root, data_root, max_cases, dry_run, down_size, save_label_jpg, calci):
    db_file = db_path + '.db'
    if not max_cases:
        max_cases = None
    try:
        if not dry_run and not down_size:
            # os.remove(db_file)
            pass
    except:
        pass
    if dry_run:
        db = None
    else:
        db = picpac.Writer(db_file)
    real_root = os.path.realpath(root)
    pattern = os.path.join(root, '*/*/*.ics')
    processed_cases = 0
    processed_images = 0
    processed_labels = 0
    for case in glob.glob(pattern)[:max_cases]:
        real_case = os.path.realpath(case)
        relative_path = os.path.relpath(real_case, real_root)
        data_case = os.path.join(data_root, relative_path)
        pi, label_n = import_case(db, case, data_case, db_path,
                                  relative_path, dry_run=dry_run,
                                  down_size=down_size, save_label_jpg=save_label_jpg, calci=calci)
        processed_cases += 1
        processed_images += pi
        processed_labels += label_n
        pass
    return processed_cases, processed_images, processed_labels

def scan(input_dir, input_data_dir, output_dir, max_cases, dry_run, down_size, save_label_jpg, calci):
    # there must be the sub-directories
    # input_dir/{benigns  benign_without_callbacks  cancers  normals}
    PATHOS = [('cancers', 'cancers'),
              ('benign_without_callbacks', 'benign_without_callbacks'),
              ('benigns', 'benigns')]
    if calci is None:
        PATHOS.append(('normals', 'normals'))
    results = {}
    for patho, db_name in PATHOS:
        input = os.path.join(input_dir, patho)
        input_data = os.path.join(input_data_dir, patho)
        if not calci is None:
            if calci:
                db_name = db_name + '_calci'
            else:
                db_name = db_name + '_non_calci'
        output = os.path.join(output_dir, db_name)
        logger.info('process %s(%s), write to %s', input, input_data, output)
        processed_cases, processed_images, processed_labels = scan_patho(db_path=output, root=input,
                                                                         data_root=input_data,
                                                                         max_cases=max_cases, dry_run=dry_run,
                                                                         down_size=down_size, save_label_jpg=save_label_jpg,
                                                                         calci=calci)
        results[patho] = (processed_cases, processed_images, processed_labels)
        pass

    for i, j in results.items():
        logger.info("============%s: %s cases, %s images, %s labels ==============", i, j[0], j[1], j[2])

    logger.info("negative:%s", label_stats[0])
    logger.info("unknown: %s", label_stats[1])
    logger.info("benign:  %s", label_stats[2])
    logger.info("positive:%s", label_stats[3])

def main(args):
    parser = argparse.ArgumentParser(description="import ddsm")
    parser.add_argument("--input", required=True, type=str)
    parser.add_argument("--output", required=True, type=str)
    parser.add_argument("--input-data", default=None, type=str)
    parser.add_argument("--max-cases", default=0, type=int)
    parser.add_argument("--dry-run", action='store_true', default=False)
    parser.add_argument("--down-size", type=float, default=0, help='0.5 means shrink to half')
    parser.add_argument("--save-label-jpg", action='store_true', default=False)
    parser.add_argument("--calci", default=None, type=str, help="only or exclude")


    opts = parser.parse_args(args[1:])

    if opts.calci is not None:
        if opts.calci == 'only':
            opts.calci = True
        elif opts.calci == 'exclude':
            opts.calci = False
        else:
            assert False, opts.calci
    scan(input_dir=opts.input,
         output_dir=opts.output,
         input_data_dir=(opts.input_data if opts.input_data is not None else opts.input),
         max_cases=opts.max_cases,
         dry_run=opts.dry_run,
         down_size=opts.down_size, save_label_jpg=opts.save_label_jpg,
         calci=opts.calci)

if __name__ == '__main__':
    main(sys.argv)
