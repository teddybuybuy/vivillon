import os,sys

def main():
    lines = open("metadata/images_crosswalk_pilot_20160906.tsv").readlines()
    all_patients = {}
    old_patient_id = -1
    per_batch_count = 1

    for line in lines[1:]:
        patient_id = line.strip().split()[0]
        if patient_id != old_patient_id:
            all_patients[patient_id] = [line]
        else:
            all_patients[patient_id].append(line)
        old_patient_id = patient_id
    keys = all_patients.keys()
    batch_keys_list = [keys[x:x+per_batch_count] for x in range(0, len(keys), per_batch_count)]
    for batch_keys in batch_keys_list:
        fout = open("metadata/images_crosswalk.tsv", "w")
        fout.write(lines[0])
        for key in batch_keys:
            fout.write("".join(all_patients[key]))
        fout.close()
        print "".join(open("metadata/images_crosswalk.tsv").readlines())
        full_key = "_".join(batch_keys)

        cmd = "./run-sc1-infer.sh"
        print "".join(os.popen(cmd).readlines())
        cmd = "grep -v subjectId /space/data/mimic-out3/Output/predictions.tsv >> out_split_prediction.tsv"
        os.popen(cmd).readlines()
        cmd = "cp /space/data/mimic-out3/Output/predictions.tsv /space/data/split/predictions_%s.tsv; cp /space/data/mimic-out3/DreamScratch/features_result.json /space/data/split/features_results_%s.tsv" % (full_key, full_key)
        os.popen(cmd).readlines()
        pass
    
main()
                   
        
