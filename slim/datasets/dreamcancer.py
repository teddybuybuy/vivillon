"""Provides data for the dreamcancer
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import logging
import tensorflow as tf

from datasets import dataset_utils
from datasets.download_and_convert_dreamcancer import get_image_dict

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

slim = tf.contrib.slim

_FILE_PATTERN = 'dreamcancer_%s_*.tfrecord'

#SPLITS_TO_SIZES = {'train': 3320, 'validation': 350}

_NUM_CLASSES = 2

_ITEMS_TO_DESCRIPTIONS = {
    'image': 'A color image of varying size.',
    'label': 'A single integer between 0 and 1',
}

FLAGS = tf.app.flags.FLAGS

def get_image_dict_xxx(fname, strip=True):
  logger.info("process %s", fname)
  if not fname or not fname.strip():
    return {}
  out_dict = {}
  for line in open(fname):
    items = line.strip().split('\t')
    if strip:
        _, filename = os.path.split(items[0])
        filebase, _ = os.path.splitext(filename)
        out_dict[filebase.split('_')[0]] = 1
    else:
        out_dict[items[0]] = 1
  return out_dict


def get_split(split_name, dataset_dir, file_pattern=None, reader=None):
  """Gets a dataset tuple with instructions for reading cifar10.

  Args:
    split_name: A train/validation split name.
    dataset_dir: The base directory of the dataset sources.
    file_pattern: The file pattern to use when matching the dataset sources.
      It is assumed that the pattern contains a '%s' string so that the split
      name can be inserted.
    reader: The TensorFlow reader type.

  Returns:
    A `Dataset` namedtuple.

  Raises:
    ValueError: if `split_name` is not a valid train/validation split.
  """

  all_fnames = get_image_dict(FLAGS.meta_fname, strip=False)
  image_cnt = len(all_fnames.keys())

  if hasattr(FLAGS, 'train_split_fname'):
      train_dict = get_image_dict(FLAGS.train_split_fname)
  else:
      logger.info("no train_split_fname")
      train_dict = {}
  if hasattr(FLAGS, 'predict_split_fname'):
      predict_dict = get_image_dict(FLAGS.predict_split_fname)
  else:
      logger.info("no predict_split_fname")
      predict_dict = {}
  logger.info("total images: %s, train: %s, predict: %s", image_cnt, len(train_dict), len(predict_dict))

  training_filenames = []
  validation_filenames = []

  # filter through prediction/training
  if False:
      for photo_fname in all_fnames.keys():
        image_id = os.path.basename(photo_fname).split('_')[0]
        if train_dict.has_key(image_id):
          training_filenames.append(image_id)
        elif predict_dict.has_key(image_id):
          validation_filenames.append(image_id)
        else:
          # XXX TMP disable
          #logger.error("Sth is wrong with this file: %s %s", str(photo_fname), image_id)
          pass
  else:
      for f in train_dict:
          for k in train_dict[f]:
              training_filenames.append('{}#{}'.format(f, k))
      for f in predict_dict:
          for k in predict_dict[f]:
              validation_filenames.append('{}#{}'.format(f, k))

  SPLITS_TO_SIZES = {'train': len(training_filenames), 'validation': len(validation_filenames)}
  #, 'prediction': image_cnt}

  if split_name not in SPLITS_TO_SIZES:
    raise ValueError('split name %s was not recognized.' % split_name)

  if not file_pattern:
    file_pattern = _FILE_PATTERN
  file_pattern = os.path.join(dataset_dir, file_pattern % split_name)

  logger.info("SPLITS_TO_SIZES %s", SPLITS_TO_SIZES)

  if len(training_filenames) == 0 and len(validation_filenames) == 0:
      logger.info("empty dataset, return None")
      return None
  # Allowing None in the signature so that dataset_factory can use the default.
  if reader is None:
    reader = tf.TFRecordReader

  keys_to_features = {
      'image/encoded': tf.FixedLenFeature((), tf.string, default_value=''),
      'image/format': tf.FixedLenFeature((), tf.string, default_value='png'),
      'image/class/label': tf.FixedLenFeature(
          [], tf.int64, default_value=tf.zeros([], dtype=tf.int64)),
  }

  items_to_handlers = {
      'image': slim.tfexample_decoder.Image(),
      'label': slim.tfexample_decoder.Tensor('image/class/label'),
  }

  decoder = slim.tfexample_decoder.TFExampleDecoder(
      keys_to_features, items_to_handlers)

  labels_to_names = None
  if dataset_utils.has_labels(dataset_dir):
    labels_to_names = dataset_utils.read_label_file(dataset_dir)

  return slim.dataset.Dataset(
      data_sources=file_pattern,
      reader=reader,
      decoder=decoder,
      num_samples=SPLITS_TO_SIZES[split_name],
      items_to_descriptions=_ITEMS_TO_DESCRIPTIONS,
      num_classes=_NUM_CLASSES,
      labels_to_names=labels_to_names)
