from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math
import os
import random
import sys
import logging

import tensorflow as tf

from datasets import dataset_utils

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

#sys.path.append(os.path.join(os.path.dirname(__file__), '../../car-plate'))
#from patients_csv_parser import Patient

# The number of images in the validation set.
_MIN_NUM_VALIDATION = 1 # minimum, to avoid 0 validate image.

# Seed for repeatability.
_RANDOM_SEED = 1

# The number of shards per dataset split.
_NUM_SHARDS = 1


class ImageReader(object):
  """Helper class that provides TensorFlow image coding utilities."""

  def __init__(self, target_size):
    # Initializes function that decodes RGB PNG data.
    self.target_size = target_size
    self._decode_png_data = tf.placeholder(dtype=tf.string)
    self._decode_png = tf.image.decode_png(self._decode_png_data, channels=3)

    self.img_holder =  tf.placeholder(tf.float32, shape=[None, None, None, 3])
    resized_img_holder = tf.image.resize_images(self.img_holder, [target_size, target_size])
    resized_img_holder2 = tf.reshape(resized_img_holder, [target_size, target_size, 3])

    padding = False
    if padding:
      pad_img_holder = tf.image.resize_image_with_crop_or_pad(resized_img_holder2, target_size, target_size)
      resized_img_op = tf.image.convert_image_dtype(pad_img_holder, tf.uint16)
    else:
      resized_img_op = tf.image.convert_image_dtype(resized_img_holder2, tf.uint16)
    self.png_encoded_image = tf.image.encode_png(resized_img_op)

  def read_image_dims(self, sess, image_data):
    image = self.decode_png(sess, image_data)
    return image.shape[0], image.shape[1]

  def decode_png(self, sess, image_data):
    image = sess.run(self._decode_png,
                     feed_dict={self._decode_png_data: image_data})
    assert len(image.shape) == 3
    assert image.shape[2] == 3
    return image

  def resize_img(self, sess, image_data):
    image = self.decode_png(sess, image_data)
    height, width = image.shape[0], image.shape[1]

    # XXX Using tensorflow to do resize/crop and image write, it is pain, need to be careful!
    image = image.reshape(-1, height, width, 3)


    #logger.debug("Resized img: %s", resized_img)

    image_data_str = sess.run(self.png_encoded_image,
                              feed_dict={self.img_holder: image})

    return image_data_str, self.target_size, self.target_size

FLAGS = tf.app.flags.FLAGS

def _get_filenames_and_classes(dataset_dir):
  """Returns a list of filenames and inferred class names.

  Args:
    dataset_dir: A directory containing a set of subdirectories representing
      class names. Each subdirectory should contain PNG or JPG encoded images.

  Returns:
    A list of image file paths, relative to `dataset_dir` and the list of
    subdirectories, representing class names.
  """
  logger.info("get filenames and classes from %s", FLAGS.meta_fname)
  lines = open(FLAGS.meta_fname).readlines()
  basedir, _ = os.path.split(FLAGS.meta_fname)
  if FLAGS.input_data_dir:
    logger.info("use input_data_dir as the input dir: %s", FLAGS.input_data_dir)
    basedir = FLAGS.input_data_dir
  photo_filenames = []
  for v in lines:
    # filename (could be full path or relative path!) is_cancer
    items = v.strip().split("\t")
    if False:
      # FLAGS.do_prediction:
      photo_filenames.append((os.path.join(basedir, os.path.basename(items[0])), "0"))
    else:
      photo_filenames.append((os.path.join(basedir, os.path.basename(items[0])), items[1]))

  return photo_filenames, ["nocancer", "cancer"]

def _get_dataset_filename(dataset_dir, split_name, shard_id):
  output_filename = 'dreamcancer_%s_%05d-of-%05d.tfrecord' % (
      split_name, shard_id, _NUM_SHARDS)
  return os.path.join(dataset_dir, output_filename)

def _convert_dataset(split_name, filenames, class_names_to_ids, dataset_dir):
  """Converts the given filenames to a TFRecord dataset.

  Args:
    split_name: The name of the dataset, either 'train' or 'validation'.
    filenames[0]: A list of absolute paths to png or jpg images.
    class_names_to_ids: A dictionary from class names (strings) to ids
      (integers).
    dataset_dir: The directory where the converted datasets are stored.
  """
  #assert split_name in ['train', 'validation', 'prediction']
  assert split_name in ['train', 'validation']

  num_per_shard = int(math.ceil(len(filenames) / float(_NUM_SHARDS)))

  logger.info('\n>> Converting images: %s [len=%s]', split_name, len(filenames))

  with tf.Graph().as_default():

    with tf.Session('') as sess:
      image_reader = ImageReader(299)
      for shard_id in range(_NUM_SHARDS):
        output_filename = _get_dataset_filename(
            dataset_dir, split_name, shard_id)

        with tf.python_io.TFRecordWriter(output_filename) as tfrecord_writer:
          start_ndx = shard_id * num_per_shard
          end_ndx = min((shard_id+1) * num_per_shard, len(filenames))
          from random import shuffle
          #x = [i for i in range(start_ndx, end_ndx)]
          if split_name == 'train':
            logger.info('oversample cancer and shuffling train data only')
            # oversample the cancer ones.
            cancers = [v for v in filenames if v[1] == "1"]
            others = [v for v in filenames if v[1] != "1"]

            # take same amount of cancer and others
            if len(cancers) == 0:
              more_cancers = cancers
            else:
              more_cancers = (cancers * int(math.ceil(len(others) / len(cancers))))[:len(others)]
            filenames = more_cancers + others
            shuffle(filenames)
            
          else:
            logger.info('dont shuffling validation data')
            pass

          for idx, filename in enumerate(filenames):
            # Read the filename:
            crop_fname = filename[0]
            if False:
              height = 299
              width = 299
              import cv2
              image = cv2.imread(crop_fname, -1)
              image = cv2.resize(image, (height, width))
              tfile = '/tmp/ttt.png'
              cv2.imwrite(tfile, image)
              image_data_str = tf.gfile.FastGFile(tfile, 'r').read()
            else:
              image_data = tf.gfile.FastGFile(crop_fname, 'r').read()
              #image_data_str, height, width = image_reader.resize_img(sess, image_data)
              height, width = image_reader.read_image_dims(sess, image_data)

            is_cancer = filename[1]

            if is_cancer == "1":
              class_name = 'cancer'
            elif is_cancer == "0":
              class_name = 'nocancer'
            else:
              logger.info("%s is %s, change to 0", crop_fname, is_cancer)
              class_name = 'nocancer'

            class_id = class_names_to_ids[class_name]

            if idx % 100 == 0:
              logger.info('  Converting image %d/%d shard %d, %s %s %s [%sx%s]',
                          idx, len(filenames), shard_id, crop_fname, is_cancer, class_id, width, height)
            #sys.stdout.flush()

            example = dataset_utils.image_to_tfexample(
                image_data, 'png', height, width, class_id)
            #example = dataset_utils.image_to_tfexample(
            #    image_data_str, 'png', height, width, class_id)

            tfrecord_writer.write(example.SerializeToString())
  logger.info("\n")


def _clean_up_temporary_files(dataset_dir):
  """Removes temporary files used to create the dataset.

  Args:
    dataset_dir: The directory where the temporary files are stored.
  """
  pass

def _dataset_exists(dataset_dir):
  for split_name in ['train', 'validation']:
    for shard_id in range(_NUM_SHARDS):
      output_filename = _get_dataset_filename(
          dataset_dir, split_name, shard_id)
      if not tf.gfile.Exists(output_filename):
        return False
  return True

def get_image_dict(fname, strip=True):
  if not fname or not fname.strip():
    return {}
  out_dict = {}
  logger.info("load image dict from %s", fname)
  for line in open(fname):
    items = line.strip().split('\t')
    _, filename = os.path.split(items[0])
    filebase, _ = os.path.splitext(filename)
    filebase = filebase.split('_')[0]
    fs = filebase.split('=', 1)
    filebase = fs[0]
    try:
      ver = int(fs[1])
    except Exception:
      ver = 0

    if filebase not in out_dict:
      out_dict[filebase] = {}
    out_dict[filebase][ver] = 1

  return out_dict

import cv2
def make_entries(entry, image_id, versions, dataset_dir, no_degree=False):
  image_in = entry[0]
  ret = []
  out_dir = os.path.join(dataset_dir, 'versions')
  if not os.path.exists(out_dir):
    os.mkdir(out_dir)
  for v in versions:
    image = cv2.imread(image_in, -1)
    num_rows, num_cols = image.shape[:2]
    max_degree = 8
    # degree is between -max_degree - 0.5 and max_degree + 0.5
    v1 = v // 2
    degree = float(v1 % max_degree) + ((v1 // max_degree) % 2) * 0.5
    if v % 2 == 1:
      degree = -degree
    rotation_matrix = cv2.getRotationMatrix2D((num_cols / 2, num_rows / 2), degree, 1)
    image = cv2.warpAffine(image, rotation_matrix, (num_cols, num_rows))

    if not no_degree:
      f = '{}={}.png'.format(image_id, v)
    else:
      assert len(versions) == 1, str(versions)
      f = '{}.png'.format(image_id)
    image_out = os.path.join(out_dir, f)
    logger.info("write %s, rotate %s", image_out, degree)
    cv2.imwrite(image_out, image)
    ret.append((image_out, entry[1]))
  return ret


def run(dataset_dir):
  """Runs the download and conversion operation.

  Args:
    dataset_dir: The dataset directory where the dataset is stored.
  """
  if not tf.gfile.Exists(dataset_dir):
    tf.gfile.MakeDirs(dataset_dir)

  if _dataset_exists(dataset_dir):
    #print('Dataset files already exist. Exiting without re-creating them.')
    logger.info('Dataset files already exist. Re-creating them for the moment to try different things.')
    #return

  photo_filenames, class_names = _get_filenames_and_classes(dataset_dir)
  class_names_to_ids = dict(zip(class_names, range(len(class_names))))

  if False:
    # assume the whole file is prediction file.
    _convert_dataset('prediction', photo_filenames, class_names_to_ids,
                     dataset_dir)
  else:
    # Divide into train and test:

    train_dict = get_image_dict(FLAGS.train_split_fname)
    predict_dict = get_image_dict(FLAGS.predict_split_fname)
    training_filenames = []
    validation_filenames = []

    # filter through prediction/training
    for entry in photo_filenames:
      photo_fname = entry[0]
      image_id = os.path.basename(photo_fname).split('_')[0]
      if train_dict and train_dict.has_key(image_id):
        logger.info("%s in training", image_id)
        training_filenames += make_entries(entry, image_id, train_dict[image_id], dataset_dir)
      elif predict_dict and predict_dict.has_key(image_id):
        logger.info("%s in validation", image_id)
        validation_filenames += make_entries(entry, image_id, predict_dict[image_id], dataset_dir, no_degree = True)
      else:
        logger.info("this file is not in list: %s", str(photo_fname))

    # First, convert the training and validation sets.
    _convert_dataset('train', training_filenames, class_names_to_ids,
                     dataset_dir)
    _convert_dataset('validation', validation_filenames, class_names_to_ids,
                     dataset_dir)

    # Finally, write the labels file:
    labels_to_class_names = dict(zip(range(len(class_names)), class_names))
    dataset_utils.write_label_file(labels_to_class_names, dataset_dir)

  _clean_up_temporary_files(dataset_dir)
  logger.info('===========Finished converting the DreamCancer dataset!============')

