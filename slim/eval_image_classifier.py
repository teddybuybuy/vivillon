# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Generic evaluation script that evaluates a model using a given dataset."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import math, logging, os
import tensorflow as tf
import simplejson

from datasets import dataset_factory
from nets import nets_factory
from preprocessing import preprocessing_factory

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

slim = tf.contrib.slim

tf.app.flags.DEFINE_integer(
    'batch_size', 100, 'The number of samples in each batch.')

tf.app.flags.DEFINE_integer(
    'max_num_batches', None,
    'Max number of batches to evaluate by default use all.')

tf.app.flags.DEFINE_string(
    'master', '', 'The address of the TensorFlow master to use.')

tf.app.flags.DEFINE_string(
    'checkpoint_path', '/tmp/tfmodel/',
    'The directory where the model was written to or an absolute path to a '
    'checkpoint file.')

tf.app.flags.DEFINE_string(
    'eval_dir', '/tmp/tfmodel/', 'Directory where the results are saved to.')

tf.app.flags.DEFINE_integer(
    'num_preprocessing_threads', 4,
    'The number of threads used to create the batches.')

tf.app.flags.DEFINE_string(
    'dataset_name', 'imagenet', 'The name of the dataset to load.')

tf.app.flags.DEFINE_string(
    'dataset_split_name', 'test', 'The name of the train/test split.')

tf.app.flags.DEFINE_string(
    'dataset_dir', None, 'The directory where the dataset files are stored.')

tf.app.flags.DEFINE_integer(
    'labels_offset', 0,
    'An offset for the labels in the dataset. This flag is primarily used to '
    'evaluate the VGG and ResNet architectures which do not use a background '
    'class for the ImageNet dataset.')

tf.app.flags.DEFINE_string(
    'model_name', 'inception_v3', 'The name of the architecture to evaluate.')

tf.app.flags.DEFINE_string(
    'preprocessing_name', None, 'The name of the preprocessing to use. If left '
    'as `None`, then the model_name flag is used.')

tf.app.flags.DEFINE_float(
    'moving_average_decay', None,
    'The decay to use for the moving average.'
    'If left as None, then moving averages are not used.')

tf.app.flags.DEFINE_integer(
    'eval_image_size', None, 'Eval image size')

tf.app.flags.DEFINE_string(
    'meta_fname', None, 'The meta file nama full path for cancer fname and info.')

tf.app.flags.DEFINE_string(
    'predict_fname', None, 'The meta file nama full path for resulting image_id and prediction.')

tf.app.flags.DEFINE_string(
  'predict_split_fname', None,
    'The fname containing image id used for predicting/evaluation')
tf.app.flags.DEFINE_string(
    'train_split_fname', None,
    'The fname containing image id used for training, XXX not useful for me, but input this anyway to suppress complaint')

FLAGS = tf.app.flags.FLAGS

def get_image_dict(fname, strip=True):
  logger.info("process %s", fname)
  if not fname or not fname.strip():
    return {}
  out_dict = {}
  for line in open(fname):
    items = line.strip().split('\t')
    if strip:
        _, filename = os.path.split(items[0])
        filebase, _ = os.path.splitext(filename)
        filebase = filebase.split('_')[0]
        f = filebase.split('=', 1)
        try:
            a = int(f[1])
        except Exception:
            a = 0
        assert a == 0, str(f)
        out_dict[f[0]] = 1
    else:
        out_dict[items[0]] = 1
  return out_dict


def main(_):
  if not FLAGS.dataset_dir:
    raise ValueError('You must supply the dataset directory with --dataset_dir')

  #tf.logging.set_verbosity(tf.logging.INFO)
  tf.logging.set_verbosity(tf.logging.DEBUG)
  with tf.Graph().as_default():
    tf_global_step = slim.get_or_create_global_step()

    ######################
    # Select the dataset #
    ######################
    dataset = dataset_factory.get_dataset(
        FLAGS.dataset_name, FLAGS.dataset_split_name, FLAGS.dataset_dir)

    if dataset is None:
        return

    ####################
    # Select the model #
    ####################
    num_classes = dataset.num_classes - FLAGS.labels_offset
    network_fn = nets_factory.get_network_fn(
        FLAGS.model_name,
        num_classes=num_classes,
        is_training=False)

    ##############################################################
    # Create a dataset provider that loads data from the dataset #
    ##############################################################
    provider = slim.dataset_data_provider.DatasetDataProvider(
        dataset,
        shuffle=False,
        common_queue_capacity=2 * FLAGS.batch_size,
        common_queue_min=FLAGS.batch_size)
    [image, label] = provider.get(['image', 'label'])
    label -= FLAGS.labels_offset

    #####################################
    # Select the preprocessing function #
    #####################################
    preprocessing_name = FLAGS.preprocessing_name or FLAGS.model_name
    image_preprocessing_fn = preprocessing_factory.get_preprocessing(
        preprocessing_name,
        is_training=False)

    eval_image_size = FLAGS.eval_image_size or network_fn.default_image_size

    image = image_preprocessing_fn(image, eval_image_size, eval_image_size)

    images, labels = tf.train.batch(
        [image, label],
        batch_size=FLAGS.batch_size,
        num_threads=FLAGS.num_preprocessing_threads,
        capacity=5 * FLAGS.batch_size)

    ########## ZZZZ add my accumulator ###########
    with tf.name_scope("zzz_vars"):
      all_prob_results = tf.Variable(tf.zeros([0,num_classes]), name="zzz_counter", validate_shape=False, dtype=tf.float32,
                                     collections=[tf.GraphKeys.LOCAL_VARIABLES])
      all_labels = tf.Variable(tf.zeros([0], tf.int64), name="zzz_labels", validate_shape=False, dtype=tf.int64,
                                     collections=[tf.GraphKeys.LOCAL_VARIABLES])
    #tf.contrib.framework.add_model_variable(all_prob_results)
    ####################
    # Define the model #
    ####################
    logits, _ = network_fn(images)
    prob_results = tf.nn.softmax(logits)

    new_value = tf.concat([all_prob_results, prob_results], 0)
    update_prob_results = tf.assign(all_prob_results, new_value, validate_shape=False)
    tf.Print(all_prob_results, [all_prob_results], message="all_prob_results")

    new_value_labels = tf.concat([all_labels, labels], 0)
    update_labels = tf.assign(all_labels, new_value_labels, validate_shape=False)
    
    # Add print operation
    #logits = tf.Print(logits, [logits], message="This is logits: ", summarize=1000000)
    #labels = tf.Print(labels, [labels], message="This is labels: ", summarize=1000000)

    if FLAGS.moving_average_decay:
      variable_averages = tf.train.ExponentialMovingAverage(
          FLAGS.moving_average_decay, tf_global_step)
      variables_to_restore = variable_averages.variables_to_restore(
          slim.get_model_variables())
      variables_to_restore[tf_global_step.op.name] = tf_global_step
    else:
      variables_to_restore = slim.get_variables_to_restore()

    predictions = tf.argmax(logits, 1)
    labels = tf.squeeze(labels)

    # Define the metrics:
    names_to_values, names_to_updates = slim.metrics.aggregate_metric_map({
        'Accuracy': slim.metrics.streaming_accuracy(predictions, labels),
        #'Recall_5': slim.metrics.streaming_recall_at_k(
        #    logits, labels, 5),
    })

    # Print the summaries to screen.
    for name, value in names_to_values.iteritems():
      summary_name = 'eval/%s' % name
      op = tf.summary.scalar(summary_name, value, collections=[])
      op = tf.Print(op, [value], summary_name)
      tf.add_to_collection(tf.GraphKeys.SUMMARIES, op)

    # TODO(sguada) use num_epochs=1
    if FLAGS.max_num_batches:
      num_batches = FLAGS.max_num_batches
    else:
      # This ensures that we make a single pass over all of the data.
      num_batches = math.ceil(dataset.num_samples / float(FLAGS.batch_size))

    if tf.gfile.IsDirectory(FLAGS.checkpoint_path):
      checkpoint_path = tf.train.latest_checkpoint(FLAGS.checkpoint_path)
    else:
      checkpoint_path = FLAGS.checkpoint_path

    tf.logging.info('Evaluating %s' % checkpoint_path)

    out_prob, out_logits, out_labels, out_all_prob_results, out_all_labels = slim.evaluation.evaluate_once(
         master=FLAGS.master,
         checkpoint_path=checkpoint_path,
         logdir=FLAGS.eval_dir,
         num_evals=num_batches,
         eval_op=list(names_to_updates.values()) + [update_prob_results, update_labels],
         variables_to_restore=variables_to_restore,
         final_op=[prob_results, logits, labels, all_prob_results, all_labels]
    )

    #logger.info("return prob: %s", out_prob)
    #logger.info("labels: %s", out_labels)
    #logger.info("all_prob: %s", out_all_prob_results)
    #for idx, prob in enumerate(out_all_prob_results):
    #  logger.info("%s: %s", idx, prob)
    # Dump out prediction result
    result_dict = {}
    predict_split_dict = get_image_dict(FLAGS.predict_split_fname)
    logger.info("%s dict size %s", FLAGS.predict_split_fname, len(predict_split_dict))
    with open(FLAGS.predict_fname + ".tsv", 'w') as outfile:
      idx = 0
      for line in open(FLAGS.meta_fname):
        fname = line.strip().split('\t')[0]
        fname_base = os.path.basename(fname).split('_')[0]
        if not predict_split_dict.has_key(fname_base):
          # filter through the predict_split_dict to ignore training files.
          #print("Skipping %s" % fname_base)
          #logger.info("skipping filter %s %s", fname, fname_base)
          continue
        #if idx >= FLAGS.batch_size:
        #logger.error("XXXXXX Too many items provided!!! XXX")
        #continue
        # class_id 1 for "cancer" class probability
        the_prob = float(out_all_prob_results[idx][1])
        the_label = float(out_all_labels[idx])
        #the_prob = float(out_prob[idx][1])
        result_dict[fname_base] = the_prob
        diff = the_label - the_prob
        outfile.write("%s\t%.8f\t%.8f\t0.0\n" % (fname_base, the_label, the_prob))
        idx += 1
        pass
      pass

    logger.info("write result (len=%s) to json %s", len(result_dict), FLAGS.predict_fname)
    with open(FLAGS.predict_fname, 'w') as outfile:
      outfile.write(simplejson.dumps(result_dict))

      pass

if __name__ == '__main__':
  tf.app.run()
