#!/bin/sh
set -x
set -e

DOCKER_HOME=`pwd`
MY_NAME=`basename "$0"`

if [ -z "$1" ]; then
    DATA_HOME=${DOCKER_HOME}
else
    DATA_HOME=$1
fi

if [ -z "$2" ]; then
    HOST_TMP_DIR=/space/data/mimic-out3
else
    HOST_TMP_DIR=$2
fi

if [ -z "$3" ]; then
    #IMAGE=docker.synapse.org/syn8048873/f1
    #IMAGE=vacuum/dl:tensorflow-1.0.0sse-plus-b
    IMAGE=vacuum/dl:tensorflow-1.0.1-plus-b
    #IMAGE=docker.synapse.org/syn8048873/f1
    INSIDE_CODE_HOME=/src
    MAPPED_IN_CODE_HOME=/src
else
    IMAGE=$3
    INSIDE_CODE_HOME=/
    MAPPED_IN_CODE_HOME=/unused
fi

PREPROCESS_DIR=${HOST_TMP_DIR}/DreamPreprocessedData
MODELSTATE_DIR=${HOST_TMP_DIR}/DreamModelState
SCRATCH_DIR=${HOST_TMP_DIR}/DreamScratch
OUTPUT_DIR=${HOST_TMP_DIR}/Output
mkdir -p ${PREPROCESS_DIR}
mkdir -p ${MODELSTATE_DIR}
mkdir -p ${SCRATCH_DIR}

# NUM_GPU_DEVICES=2 MAIN_RESIZE_OPT=--main-resize=0.5
COMMON_OPTS="-e MAIN_RESIZE_OPT=${MAIN_RESIZE_OPT} -e NUM_GPU_DEVICES=${NUM_GPU_DEVICES} -e DISPLAY=$DISPLAY -e DREAM_TEST=$DREAM_TEST -v /tmp/.X11-unix:/tmp/.X11-unix -w ${INSIDE_CODE_HOME} ${IMAGE}"

#Consider run this command if docker run failed.
#sudo usermod -aG docker $USER

if [ "${MY_NAME}" = "run-preprocess.sh" ];
then
    nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/trainingData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${PREPROCESS_DIR}:/preprocessedData:rw ${COMMON_OPTS} ${INSIDE_CODE_HOME}/preprocess.sh
fi

if [ "${MY_NAME}" = "run-train.sh" ];
then
    nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/trainingData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${PREPROCESS_DIR}:/preprocessedData:ro -v ${MODELSTATE_DIR}:/modelState -v ${SCRATCH_DIR}:/scratch ${COMMON_OPTS} ${INSIDE_CODE_HOME}/train.sh
fi

if [ "${MY_NAME}" = "run-test-infer.sh" ];
then
    nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/trainingData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${PREPROCESS_DIR}:/preprocessedData:ro -v ${MODELSTATE_DIR}:/modelState -v ${SCRATCH_DIR}:/scratch ${COMMON_OPTS} ${INSIDE_CODE_HOME}/test_inference.sh
fi

if [ "${MY_NAME}" = "run-infer-test.sh" ];
then
    nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/inferenceData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${MODELSTATE_DIR}:/modelState -v ${SCRATCH_DIR}:/scratch -v ${OUTPUT_DIR}:/output ${COMMON_OPTS} /bin/bash
fi

if [ "${MY_NAME}" = "run-sc1-infer.sh" ];
then
    nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/inferenceData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${MODELSTATE_DIR}:/modelState -v ${SCRATCH_DIR}:/scratch -v ${OUTPUT_DIR}:/output ${COMMON_OPTS} ${INSIDE_CODE_HOME}/sc1_infer.sh
    #nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/inferenceData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${MODELSTATE_DIR}:/modelState -v ${SCRATCH_DIR}:/output -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -w ${INSIDE_CODE_HOME} ${IMAGE} /bin/bash
fi

if [ "${MY_NAME}" = "run-sc2-infer.sh" ];
then
    nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/inferenceData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${MODELSTATE_DIR}:/modelState -v ${SCRATCH_DIR}:/scratch -v ${OUTPUT_DIR}:/output ${COMMON_OPTS} ${INSIDE_CODE_HOME}/sc2_infer.sh
fi

# run our generic docker /bin/bash
if [ "${MY_NAME}" = "run-docker.sh" ];
then
    #xhost +
    nvidia-docker run -it --rm -v ${DOCKER_HOME}:${MAPPED_IN_CODE_HOME} -v ${DATA_HOME}/trainingData:/trainingData:ro -v ${DATA_HOME}/metadata:/metadata:ro -v ${PREPROCESS_DIR}:/preprocessedData -v ${MODELSTATE_DIR}:/modelState -v ${SCRATCH_DIR}:/scratch -v ${OUTPUT_DIR}:/output ${COMMON_OPTS} /bin/bash
fi
