#!/bin/bash
set -e
set -x
#
# This script is modified from the slim/scripts/finetune_inceptionv3_on_flowers.sh
# Where the pre-trained InceptionV3 checkpoint is saved to.
#DREAM_TEST=1
if [ -z "$1" ]; then
    #META_FNAME=crop_meta.tsv
    META_FNAME=resize_c400.csv
    #META_FNAME=calci-result.csv
else
    META_FNAME=$1
    # Other choices:
    # resize_400.csv calci-result.csv
fi

if [ -z "$2" ]; then
    META_DIR=/metadata
else
    META_DIR=$2
fi

if [ -z "$3" ]; then
    MODEL_DIR=/modelState
else
    MODEL_DIR=$3
fi

if [ -z "$4" ]; then
    PREPROCESS_BASE_DIR=/preprocessedData
else
    PREPROCESS_BASE_DIR=$4
fi

if [ -z "$5" ]; then
    SCRATCH_DIR=/scratch
else
    SCRATCH_DIR=$5
fi

# result_json file should container a json string like:
# - float number means prob of cancer
# - prediction means the number is from predition
# - python module json can be used to read/write this (load/dump)
# '{"1243": {"calci": 0.4, "mass": 0, "overview": 0.49, "prediction": true}, "3834": {"prediction": false}}'

if [ -z "$6" ]; then
    #This file is the name of json file holding the result prediction.
    PREDICT_FNAME=result_${META_FNAME}
    PREDICT_FULL=${SCRATCH_DIR}/${PREDICT_FNAME}
    #RESULT_JSON=${SCRATCH_DIR}/features_result.json
else
    PREDICT_FULL=$6
    #RESULT_JSON=$6
fi

META_FULL=${PREPROCESS_BASE_DIR}/dataset/${META_FNAME}
HOME_BASE_DIR=/src

# 0 means train, 1 means prediction, default is train
if [ -z "$7" ]; then
    PREDICT_MODEL=
    # These two files are the raw files split from prep.tsv, acting as filters to tell us which files should be used for training/eval.
    TRAIN_SPLIT_FULL=${SCRATCH_DIR}//prep_train.tsv
    PREDICT_SPLIT_FULL=${SCRATCH_DIR}//prep_predict.tsv
    PREDICT_SPLIT_PART=${SCRATCH_DIR}//prep_predict.tsv_part
    cd ${HOME_BASE_DIR}/car-plate
    mount
    ls -lrt /
    ls -lrt ${PREPROCESS_BASE_DIR}
    python patients_csv_parser.py --csv1 ${PREPROCESS_BASE_DIR}/dataset/images_crosswalk.tsv \
                                  --csv2 ${PREPROCESS_BASE_DIR}/dataset/exams_metadata.tsv \
                                  --filter-json ${PREPROCESS_BASE_DIR}/dataset/features_result.json \
                                  --list-files-output ${SCRATCH_DIR}/all.tsv \
                                  --list-files-train ${TRAIN_SPLIT_FULL} \
                                  --list-files-predict ${PREDICT_SPLIT_FULL} \
                                  --train-ratio 0.3 \
                                  --train-repeat 1 \
                                  --show-stats
    cd -
else
    PREDICT_MODEL=$7
    TRAIN_SPLIT_FULL=' '
    PREDICT_SPLIT_FULL=${META_FULL}
fi


PRETRAINED_CHECKPOINT_DIR=${HOME_BASE_DIR}/slim/checkpoints

# Where the training (fine-tuned) checkpoint and logs will be saved to.
DATASET_DIR=${SCRATCH_DIR}/import_data

# model dir
TRAIN_DIR=${SCRATCH_DIR}/train

# remove train dir only in predict_model
if [ ! -z ${PREDICT_MODEL} ]; then
    rm -fr ${TRAIN_DIR}
fi

mkdir -p ${TRAIN_DIR}
mkdir -p ${TRAIN_DIR}/all
mkdir -p ${DATASET_DIR}

if [ ! -z ${DREAM_TEST} ]; then
    echo "DREAM_TEST is set"
    TRAIN_STEP_1=300
    TRAIN_STEP_2=200
else
    TRAIN_STEP_1=10000
    TRAIN_STEP_2=20000
fi


# convert to the dataset slim can take
cd ${HOME_BASE_DIR}/slim/
#disable_convert='
python download_and_convert_data.py --dataset_name=dreamcancer --dataset_dir=${DATASET_DIR} --meta_fname=${META_FULL} \
       --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${PREDICT_SPLIT_FULL}
#'
cd -

if [ ! -z ${PREDICT_MODEL_XXX} ]; then
    echo "do prediction"
    cd ${HOME_BASE_DIR}/slim/
python eval_image_classifier.py \
  --checkpoint_path=${TRAIN_DIR} \
  --eval_dir=${TRAIN_DIR} \
  --dataset_name=dreamcancer \
  --dataset_split_name=validation \
  --dataset_dir=${DATASET_DIR} \
  --num_preprocessing_threads=1 \
  --meta_fname=${META_FULL} \
  --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${PREDICT_SPLIT_FULL} \
  --predict_fname=${PREDICT_FULL}_before_fine_tune \
  --model_name=inception_v4

python ../auroc.py --truth-file ${PREDICT_SPLIT_FULL} --predict-file ${PREDICT_FULL}_before_fine_tune.tsv

    # xxx zhe todo: change me!!!
    # echo '{"1243": {"calci": 0.4, "mass": 0, "overview": 0.49, "prediction": true}, "3834": {"prediction": false}}' > $6
    exit 0
fi

# dump sample data out.
if [ -z ${DREAM_TEST} ]; then
    echo "debugging: output from generated crop_meta.tsv"
    head -20 ${META_FULL}
    #echo "debugging: output from genearted crop_nega_meta.tsv"
    #cat ${DATASET_DIR}/crop_nega_meta.tsv
else
    echo "debugging: output from generated tsv for meta: mass, calc, or resized"
    head -20 ${META_FULL}
fi
echo "done debugging: output"

cd ${HOME_BASE_DIR}/slim

df -h
du -s ${MODEL_DIR}
date

# Check for the pre-trained checkpoint.
if [ ! -f ${PRETRAINED_CHECKPOINT_DIR}/inception_v4.ckpt ]; then
    echo "checkpoint doesn't exist! Bailing"
    exit 1
fi

#disable_1st_tune='
# Fine-tune only the new layers for 1000 steps.
#  --num_clones=2 \

python train_image_classifier.py \
  --train_dir=${TRAIN_DIR} \
  --dataset_name=dreamcancer \
  --dataset_split_name=train \
  --dataset_dir=${DATASET_DIR} \
  --meta_fname=${META_FULL} \
  --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${PREDICT_SPLIT_FULL} \
  --model_name=inception_v4 \
  --checkpoint_path=${PRETRAINED_CHECKPOINT_DIR}/inception_v4.ckpt \
  --checkpoint_exclude_scopes=InceptionV4/Logits,InceptionV4/AuxLogits \
  --trainable_scopes=InceptionV4/Logits,InceptionV4/AuxLogits \
  --max_number_of_steps=${TRAIN_STEP_1} \
  --batch_size=32 \
  --learning_rate=0.01 \
  --learning_rate_decay_type=exponential \
  --save_interval_secs=600 \
  --save_summaries_secs=600 \
  --log_every_n_steps=100 \
  --optimizer=adam \
  --weight_decay=0.00004

du -s ${TRAIN_DIR}
ls -lrt ${TRAIN_DIR}
date


# Run prediction.
python eval_image_classifier.py \
  --checkpoint_path=${TRAIN_DIR} \
  --eval_dir=${TRAIN_DIR} \
  --dataset_name=dreamcancer \
  --dataset_split_name=validation \
  --dataset_dir=${DATASET_DIR} \
  --num_preprocessing_threads=1 \
  --meta_fname=${META_FULL} \
  --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${PREDICT_SPLIT_FULL} \
  --predict_fname=${PREDICT_FULL}_before_fine_tune \
  --model_name=inception_v4

python ../auroc.py --truth-file ${PREDICT_SPLIT_FULL} --predict-file ${PREDICT_FULL}_before_fine_tune.tsv

date

#disable_2nd_train='
# Fine-tune all the new layers for 500 steps. XXX train properly
python train_image_classifier.py \
  --train_dir=${TRAIN_DIR}/all \
  --dataset_name=dreamcancer \
  --dataset_split_name=train \
  --dataset_dir=${DATASET_DIR} \
  --meta_fname=${META_FULL} \
  --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${PREDICT_SPLIT_FULL} \
  --model_name=inception_v4 \
  --checkpoint_path=${TRAIN_DIR} \
  --max_number_of_steps=${TRAIN_STEP_2} \
  --batch_size=32 \
  --learning_rate=0.003 \
  --learning_rate_decay_type=exponential \
  --save_interval_secs=600 \
  --save_summaries_secs=600 \
  --log_every_n_steps=100 \
  --optimizer=rmsprop \
  --weight_decay=0.00004

du -s ${TRAIN_DIR}/all
ls -lrt ${TRAIN_DIR}/all
date

# Run evaluation.
python eval_image_classifier.py \
  --checkpoint_path=${TRAIN_DIR}/all \
  --eval_dir=${TRAIN_DIR}/all \
  --dataset_name=dreamcancer \
  --dataset_split_name=validation \
  --dataset_dir=${DATASET_DIR} \
  --num_preprocessing_threads=1 \
  --meta_fname=${META_FULL} \
  --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${PREDICT_SPLIT_FULL} \
  --predict_fname=${PREDICT_FULL} \
  --model_name=inception_v4

python ../auroc.py --truth-file ${PREDICT_SPLIT_FULL} --predict-file ${PREDICT_FULL}.tsv

# Run real evaluation.
python eval_image_classifier.py \
  --checkpoint_path=${TRAIN_DIR}/all \
  --eval_dir=${TRAIN_DIR}/all \
  --dataset_name=dreamcancer \
  --dataset_split_name=validation \
  --dataset_dir=${DATASET_DIR} \
  --num_preprocessing_threads=1 \
  --meta_fname=${META_FULL} \
  --train_split_fname=${TRAIN_SPLIT_FULL} --predict_split_fname=${PREDICT_SPLIT_FULL} \
  --predict_fname=${PREDICT_FULL} \
  --model_name=inception_v4

date

cp -pr ${PREPROCESS_BASE_DIR}/dataset/images_crosswalk.tsv ${SCRATCH_DIR}/images_crosswalk.tsv
cp -pr ${PREPROCESS_BASE_DIR}/dataset/exams_metadata.tsv ${SCRATCH_DIR}/exams_metadata.tsv
cp -pr ${PREPROCESS_BASE_DIR}/dataset/dcm_info.json ${SCRATCH_DIR}/dcm_info.json

cd ${HOME_BASE_DIR}

python update_features_result.py --new-input ${PREDICT_FULL} --old-input ${PREPROCESS_BASE_DIR}/dataset/features_result.json --output ${SCRATCH_DIR}/features_result.json

MODEL_DIR_X0=${MODEL_DIR}/X0
MODEL_DIR_X1=${MODEL_DIR}/X1
mkdir -p ${MODEL_DIR_X0}
mkdir -p ${MODEL_DIR_X1}

echo ##################### before glob train, calci+mass
python xgb-testloop.py --testpart 0.33 ${PREPROCESS_BASE_DIR}/dataset/ --out ${MODEL_DIR_X0}
python xgb-testloop.py --testpart 0.10 ${PREPROCESS_BASE_DIR}/dataset/ --out ${MODEL_DIR_X0}
python xgb-testloop.py --testpart 0.01 ${PREPROCESS_BASE_DIR}/dataset/ --out ${MODEL_DIR_X0}

echo ##################### calci+mass+glob
python xgb-testloop.py --testpart 0.33 ${SCRATCH_DIR} --out ${MODEL_DIR_X1}
python xgb-testloop.py --testpart 0.10 ${SCRATCH_DIR} --out ${MODEL_DIR_X1}
python xgb-testloop.py --testpart 0.01 ${SCRATCH_DIR} --out ${MODEL_DIR_X1}

echo ##################### calci only
IGNORE_F_MASS=1 IGNORE_F_GLOB=1 python xgb-testloop.py --testpart 0.33 ${SCRATCH_DIR}
IGNORE_F_MASS=1 IGNORE_F_GLOB=1 python xgb-testloop.py --testpart 0.10 ${SCRATCH_DIR}

echo ##################### mass only
IGNORE_F_CALCI=1 IGNORE_F_GLOB=1 python xgb-testloop.py --testpart 0.33 ${SCRATCH_DIR}
IGNORE_F_CALCI=1 IGNORE_F_GLOB=1 python xgb-testloop.py --testpart 0.10 ${SCRATCH_DIR}

echo ##################### glob only
IGNORE_F_CALCI=1 IGNORE_F_MASS=1 python xgb-testloop.py --testpart 0.33 ${SCRATCH_DIR}
IGNORE_F_CALCI=1 IGNORE_F_MASS=1 python xgb-testloop.py --testpart 0.10 ${SCRATCH_DIR}

echo ##################### calci+glob only
IGNORE_F_MASS=1 python xgb-testloop.py --testpart 0.33 ${SCRATCH_DIR}
IGNORE_F_MASS=1 python xgb-testloop.py --testpart 0.10 ${SCRATCH_DIR}

echo ##################### calci+mass only
IGNORE_F_GLOB=1 python xgb-testloop.py --testpart 0.33 ${SCRATCH_DIR}
IGNORE_F_GLOB=1 python xgb-testloop.py --testpart 0.10 ${SCRATCH_DIR}

echo ##################### glob+mass only
IGNORE_F_CALCI=1 python xgb-testloop.py --testpart 0.10 ${SCRATCH_DIR}

date

mkdir -p ${MODEL_DIR}/${META_FNAME}
cp -pr ${TRAIN_DIR}/graph.pbtxt ${MODEL_DIR}/${META_FNAME}
cp -pr ${TRAIN_DIR}/checkpoint ${MODEL_DIR}/${META_FNAME}
cp -pr ${TRAIN_DIR}/model.ckpt-${TRAIN_STEP_1}.* ${MODEL_DIR}/${META_FNAME}

mkdir -p ${MODEL_DIR}/${META_FNAME}/all
cp -pr ${TRAIN_DIR}/all/graph.pbtxt ${MODEL_DIR}/${META_FNAME}/all
cp -pr ${TRAIN_DIR}/all/checkpoint ${MODEL_DIR}/${META_FNAME}/all
cp -pr ${TRAIN_DIR}/all/model.ckpt-${TRAIN_STEP_2}.* ${MODEL_DIR}/${META_FNAME}/all

