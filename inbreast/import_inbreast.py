#!/usr/bin/env python
import argparse
import os
import sys
import glob
import logging
import simplejson as json
import subprocess
import dicom
import plistlib
import picpac
import cv2
import numpy
import histogram as his

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

def bounding_box (shape):   # shape must be polygon
    if shape['type'] != 'polygon':
        return None
    points = shape['geometry']['points']
    x, y = points[0]['x'], points[0]['y']
    min_x, min_y = x, y
    max_x, max_y = x, y
    for point in points[1:]:
        x, y = point['x'], point['y']
        min_x = min(x, min_x)
        min_y = min(y, min_y)
        max_x = max(x, max_x)
        max_y = max(y, max_y)
        pass
    return (min_x, min_y, max_x, max_y)

def inside (i, o):
    # whether bbox i is totally inside o
    return i[0] >= o[0] and i[1] >= o[1] and i[2] <= o[2] and i[3] <= o[3]

def remove_big_calci(shapes, max_diameter, row, col):
    found = 0
    if max_diameter == 0:
        return shapes, found
    result = []
    for shape in shapes:
        bbox = bounding_box(shape)
        if bbox:
            w = (bbox[2] - bbox[0]) * row
            h = (bbox[3] - bbox[1]) * col
            if w > max_diameter or h > max_diameter:
                logger.info("remove calci %s x %s", w, h)
                found += 1
                continue
        result.append(shape)
    return result, found

def remove_outer_circle (shapes):
    bbox = [bounding_box(shape) for shape in shapes]
    found = 0
    for i in range(len(bbox)):
        for j in range(0, len(bbox)):
            if i == j:
                continue
            if inside(bbox[i], bbox[j]):
                if shapes[j]:
                    found += 1
                    shapes[j] = None
                    logger.info("remove overlapping mass")
    return [x for x in shapes if x], found

def import_roi_mass (run, image, roi, rows, cols, mass_value):
    lesion = roi['Name']
    points = []
    for s in roi['Point_px']:
        xy = s.strip('()').split(',')
        x = float(xy[0].strip())
        y = float(xy[1].strip())
        #if not (x <= cols and y <= rows and x >= 0 and y >= 0):
        #    print x, cols, y, rows
        x /= cols
        y /= rows
        if x < 0:
            x = 0
        if y < 0:
            y = 0
        if x > 1:
            x = 1
        if y > 1:
            y = 1
        #assert x <= 1
        #assert y <= 1
        points.append({'x':x, 'y':y})
    if lesion == 'Mass':
        pass
    elif 'piculated' in lesion:
        pass
    else:
        return None
    if mass_value is None or mass_value <= 0:
        assert False, str(mass_value)
    if mass_value == 3:
        mass_value = 2 # convert like-cancer to cancer
    return {'type':'polygon', 'label': mass_value, 'geometry':{'points': points}}
    #print '\t', lesion

HISTO_RADIUS = 4

calci_histo = {}

def show_histogram():
    for k in calci_histo:
        import matplotlib.pylab as plt
        his = calci_histo[k]

        #plt.show()
        logger.info("level %s, histo %s, len %s, min-bucket=%s, max-bucket=%s, sum=%s",
                    k, his, len(his), his[0], his[len(his) - 1], numpy.sum(his))
        plt.bar(range(len(his)), his)
        plt.savefig("calci_histo_{}_expand_{}_{}.png".format(args.adjust_color_profile, args.contrast_expand, k))
        plt.clf()
    pass

def add_histogram(image, orig_image, point):
    def get_v(c, r):
        if r < shape[0] and r >= 0 and c < shape[1] and c >= 0:
            va = image[r][c]
            # print("############### va:", orig_image[r][c], va)
            return va
        else:
            print("badddd", c, r, shape)
            return 0
    def get_level(c1, r1, c, r):
        return max(abs(c1-c), abs(r1-r))

    def add_histo(level, v):
        level = str(level)
        if level not in calci_histo:
            calci_histo[level] = numpy.zeros(4096)
        if v < 4096:
            calci_histo[level][v] += 1
            # print("add_histo", level, v, calci_histo[level][v])
        else:
            logger.error("value out of range %s", v)

    if not args.collect_histogram:
        return
    shape = image.shape
    x = int(point['x'] * shape[1])
    y = int(point['y'] * shape[0])

    radius = HISTO_RADIUS
    for i in range(x-radius, x + radius + 1):
        for j in range(y - radius, y + radius + 1):
            level = get_level(i, j, x, y)
            v = int(get_v(i, j))
            add_histo(level, v)
            # print(str(point), level, v, i, j)

def import_roi_calci (run, image, roi, rows, cols, orig_image):
    lesion = roi['Name']
    points = []
    for s in roi['Point_px']:
        xy = s.strip('()').split(',')
        x = float(xy[0].strip())
        y = float(xy[1].strip())
        #if not (x <= cols and y <= rows and x >= 0 and y >= 0):
        #    print x, cols, y, rows
        x /= cols
        y /= rows
        if x < 0:
            x = 0
        if y < 0:
            y = 0
        if x > 1:
            x = 1
        if y > 1:
            y = 1
        #assert x <= 1
        #assert y <= 1
        points.append({'x':x, 'y':y})
    if lesion == 'Calcification':
        pass
    else:
        return None

    if len(points) > 1:
        return {'type':'polygon', 'geometry':{'points': points}}
    else:
        add_histogram(image, orig_image, points[0])
        return {'type':'point', 'geometry': points[0]}

def sigmoid_v1(image):
    c, k, x0, y0 = [2674.38285022, 0.00734754245424, 1532.51097978, 291.341664686]
    return c / (1 + numpy.exp(-k * (image - x0))) + y0

def sigmoid_v3(image):
    c, k, x0 = [1.53469848475, 0.00629729347558, 1396.78954557]
    return c * image / (1.0 + numpy.exp(-k * (image - x0)))

def sigmoid_v5(image):
    c, k, x0, y0 = [2800.0, 0.00434754245424, 1700.0, 291.341664686]
    return c / (1 + numpy.exp(-k * (image - x0))) + y0

def do_adjust_color_profile(image, adjust_color_profile):
    # print "do_adjust_color_profile", adjust_color_profile
    if adjust_color_profile == 'v1':
        image = sigmoid_v1(image)
    elif adjust_color_profile == 'v3':
        image = sigmoid_v3(image)
    elif adjust_color_profile == 'v5':
        image = sigmoid_v5(image)
    else:
        assert False, 'invalid color_profile {}'.format(adjust_color_profile)
    image = image.astype(numpy.int16)
    return image

SKIP_FILE_LIST = ['20587320', '28587426']

def do_import(base_dir, db, csv, pic, no_pic, contrast_expand,
              max_calci_diameter, adjust_color_profile, jpg_dir, annotation_file):
    pp = picpac.Writer(pic)
    nopp = picpac.Writer(no_pic)
    removed_total = 0
    total_shapes = 0
    filenum = 0
    try:
        with open(annotation_file) as jsondata:
            annotation = json.load(jsondata)
            mass_info = annotation['mass']
    except:
        logger.info("cannot open annotation file %s. assume assume all 1", annotation_file)
        mass_info = {}

    with open(os.path.join(base_dir, csv)) as f:
        f.readline()
        C = 0
        for l in f:
            l = l.strip().split(';')
            name = l[5]
            side = l[2]
            view = l[3]
            density = None
            try:
                density = float(l[6])
            except:
                logging.warning('bad density %s: %s' % (l[6], name))
                pass
            birads = l[7]
            try:
                dcm_path = glob.glob(os.path.join(base_dir, 'AllDICOMs/%s*' % name))[0]
            except Exception:
                dcm_path = glob.glob(os.path.join(base_dir, '%s.dcm' % name))[0]
            print dcm_path
            for skipped in SKIP_FILE_LIST:
                if skipped in dcm_path:
                    logger.info("skip file %s", dcm_path)
                    continue
            _, filename = os.path.split(dcm_path)
            filebase_name, _ = os.path.splitext(filename)
            dcm = dicom.read_file(dcm_path)
            rows = int(dcm.data_element('Rows').value)
            cols = int(dcm.data_element('Columns').value)
            depth = int(dcm.data_element('BitsStored').value)
            image = dcm.pixel_array
            orig_image = image
            if adjust_color_profile:
                image = do_adjust_color_profile(image, adjust_color_profile)
            print image.dtype, image.shape, depth
            if contrast_expand != 'no':
                histogram = his.compute_histogram(image)
                equalized_image = his.histogram_expansion(image, histogram, saturate=contrast_expand=='sat')
                image = equalized_image
                print 'contrast expansion', image.dtype, image.shape, depth, numpy.max(image), numpy.min(image), contrast_expand

            roi = os.path.join(base_dir, 'AllXML/%s.xml' % name)
            if not os.path.exists(roi):
                roi = os.path.join(base_dir, '%s.xml' % name)
                if not os.path.exists(roi):
                    continue
            plist = plistlib.readPlist(roi)
            cc = 0
            assert len(plist['Images']) == 1
            shapes = []
            if db == 'mass':
                try:
                    mass_value = mass_info[filebase_name]
                except Exception:
                    logger.info("cannot find mass %s, assume mass is 1", filebase_name)
                    mass_value = 1
                if not mass_value is None:
                    mass_value = int(mass_value)
            for yyy in plist['Images'][0]['ROIs']:
                if db == 'mass':
                    shape = import_roi_mass(False, image, yyy, rows, cols, mass_value)
                else:
                    shape = import_roi_calci(False, image, yyy, rows, cols, orig_image)
                if shape:
                    # logger.info("add roi: %s", shape)
                    shapes.append(shape)
                pass
            removed = 0
            if db == 'mass':
                shapes, removed = remove_outer_circle(shapes)
            elif db == 'calci':
                shapes, removed = remove_big_calci(shapes, max_calci_diameter, rows, cols)

            if jpg_dir:
                _, file = os.path.split(dcm_path)
                filebase, ext = os.path.splitext(file)
                jpeg_base = os.path.join(jpg_dir, filebase)
                jpeg1 = jpeg_base + '.png'
                jpeg2 = jpeg_base + '_lab.png'
                image1 = (image / 16).astype('int32')
                image2 = (image / 16).astype('int32')
                for shape in shapes:
                    if shape['type'] == 'polygon':
                        points = shape['geometry']['points']
                        pts = numpy.array([[point['x'] * cols, point['y'] * rows] for point in points], numpy.int32)
                        pts = pts.reshape((-1, 1, 2))
                        print(image2.dtype, pts.dtype)
                        cv2.polylines(image2, pts=[pts], isClosed=True, color=(255,0,0), thickness=2)
                    elif shape['type'] == 'point':
                        point = shape['geometry']
                        cv2.circle(image2, center=(int(point['x'] * cols), int(point['y'] * rows)), radius=10, color=(255,0,0), thickness=2)
                    else:
                        assert False, str(shape)
                if len(shapes):
                    cv2.imwrite(jpeg1, image1)
                    cv2.imwrite(jpeg2, image2)

            removed_total += removed
            filenum += 1
            total_shapes += len(shapes)
            anno = {"shapes": shapes,
                    "meta": {"side": side,
                             "view": view,
                             "birads": birads
                        }
                    }
            if args.collect_histogram:
                continue

            buf1 = cv2.imencode('.tiff', image)[1].tostring()
            buf2 = json.dumps(anno)
            if shapes:
                pp.append(buf1, buf2)
            else:
                nopp.append(buf1, buf2)
                pass #continue
            pass
        pass
    logger.info("file_num=%s total_shapes=%s, removed_shapes=%s", filenum, total_shapes, removed_total)
    pass

args = None
if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='PROG')
    parser.add_argument('--basedir', required=True, help='base directory')
    parser.add_argument('--jpg-dir', default=None, help='save-jpeg')
    parser.add_argument('--pic',required=True, help='picpac file for positive. elg. inb.mass.pic')
    parser.add_argument('--pic-no', required=True, help='picpac file for negative, e.g.: inb.no_mass.pic')
    parser.add_argument('--db', required=True, choices=['mass', 'calci'])
    parser.add_argument('--annotation-json', type=str, default='annotation.json')
    parser.add_argument('--csv', default='INbreast.csv', help='csv file')
    parser.add_argument('--max-calci-diameter', type=int, default=0, help="max calcification diameter")
    parser.add_argument('--contrast-expand', default='no', choices=['no', 'yes', 'sat'])
    parser.add_argument('--collect-histogram', action='store_true', default=False, help='collect histogram')
    parser.add_argument('--adjust-color-profile', default=None, help='adjust color profile')

    args = parser.parse_args()
    do_import(base_dir=args.basedir, csv=args.csv,
              pic=args.pic, no_pic=args.pic_no, db=args.db,
              contrast_expand=args.contrast_expand,
              max_calci_diameter=args.max_calci_diameter,
              adjust_color_profile=args.adjust_color_profile,
              jpg_dir=args.jpg_dir,
              annotation_file=args.annotation_json)

    if args.collect_histogram:
        show_histogram()
