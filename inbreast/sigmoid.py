#!/usr/bin/env python

import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def sigmoid_v1 (X):
    c, k, x0, y0 = [2674.38285022, 0.00734754245424, 1532.51097978, 291.341664686]
    return c / (1 + np.exp(-k * (X - x0))) + y0

def sigmoid_v2 (X):
    c, k, x0 = [3151.17453392, 0.00557097631011, 1519.68755078]
    return c / (1.0 + np.exp(-k * (X - x0))) - c / (1.0 + np.exp(-k * (0 - x0)))

    
def sigmoid_v3 (X):
    c, k, x0 = [1.53469848475, 0.00629729347558, 1396.78954557]
    return c * X / (1.0 + np.exp(-k * (X - x0))) 


X = np.linspace(0, 4000, 400)
Y1 = sigmoid_v1(X)
Y2 = sigmoid_v2(X)
Y3 = sigmoid_v3(X)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(X, Y1, label='v1, $\sigma(x)$')
ax.plot(X, Y2, label='v2, $\sigma(x)-\sigma(0)$')
ax.plot(X, Y3, label='v3, $x*\sigma(x)$')
ax.legend()
fig.savefig('sigmoid.png')
