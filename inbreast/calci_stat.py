#!/usr/bin/env python



# ./calci_stat.py --db calci-ellipse.pic --out xxx.png
# processing very slow, use --max to limit data size when testing



import argparse
import cv2
import numpy as np
import matplotlib as mpl 
mpl.use('Agg')
import matplotlib.pyplot as plt 
from skimage import measure
import picpac

parser = argparse.ArgumentParser(prog='PROG')
parser.add_argument('--db', required=True, help='caclci picpac db')
parser.add_argument('--out', default='calci-stat.png', help='output image')
parser.add_argument('--max', default=0, type=int, help='max images to read')
parser.add_argument('--bins', default=200, type=int, help='histogram bins')
args = parser.parse_args()

config = dict(
        loop=False,
        shuffle=False,
        batch=1,
        channels=1,
        annotate='json',
        perturb=False
        )

stream = picpac.ImageStream(args.db, **config)

sizes = []
cc = 0
for image, anno, _ in stream:
    #print anno.shape
    anno = np.squeeze(anno, axis=[0,3])
    anno = anno.astype(dtype=np.int8)
    labels = measure.label(anno)
    vals, counts = np.unique(labels, return_counts=True)
    biggest = counts[0]
    counts = sorted(counts, reverse=True)
    assert biggest == counts[0]
    print counts[:5]
    sizes.extend(counts[1:])
    if args.max > 0 and cc >= args.max:
        break
    cc += 1
    pass

with open(args.out, 'w') as f:
    f.write('\n'.join([str(x) for x in sizes]))
#fig = plt.figure()
#ax = fig.add_subplot(111)
#ax.hist(np.array(sizes), bins=args.bins)
#fig.savefig(args.out)


