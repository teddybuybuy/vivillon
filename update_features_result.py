import argparse
import logging
import simplejson

logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s %(process)d:[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

def main():

    #with open(FLAGS.new_input) as f:
    #    new_input = f.readlines()
    #    new_input = [x.split('\t') for x in new_input]

    with open(FLAGS.new_input, 'r') as data:
        new_input = simplejson.load(data)

    with open(FLAGS.old_input, 'r') as data:
        old_input = simplejson.load(data)

    output = {}
    for items in new_input:
        #print(items)
        i = items
        v = [float(new_input[items]), 0]
        output[i] = old_input[i]
        output[i]['glob'] = str(v)

    with open(FLAGS.output, 'w') as data:
        simplejson.dump(output, data)

    logger.info("old_input=%s new_input=%s output=%s", len(old_input), len(new_input), len(output))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='PROG')
    parser.add_argument('--new-input', required=True, type=str)
    parser.add_argument('--old-input', required=True, type=str)
    parser.add_argument('--output', required=True, type=str)

    FLAGS = parser.parse_args()

    main()
