import argparse
import csv
import os
import sys
import logging
import simplejson


logger = logging.getLogger(__name__)
logging.basicConfig(format='%(asctime)s - %(levelname)s [%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s', level=logging.DEBUG)

def main(args):
    parser = argparse.ArgumentParser(description="process_patients_images.py")
    parser.add_argument("--input", type=str)
    parser.add_argument("--output", type=str)
    parser.add_argument("--max-gpu", type=int, default=1)

    opts = parser.parse_args(args[1:])


    lines = open(opts.input, 'rb').readlines()
    total_lines = len(lines)
    splitting_point = (total_lines - 1) / (opts.max_gpu + 1) + 1
    if splitting_point == 0:
        logger.error('empty file %s', opts.input)
        return

    l = 0
    num_files = 0
    for line in lines:
        if l % splitting_point == 0:
            path, filename = os.path.split(opts.output)
            filebase, ext = os.path.splitext(filename)
            fout = '{}_{}{}'.format(os.path.join(path, filebase), num_files, ext)
            logger.info('open file %s', fout)
            f = open(fout, 'w')
            num_files += 1
        f.write(line)
        l += 1

    while num_files < opts.max_gpu + 1:
        fout = '{}_{}{}'.format(os.path.join(path, filebase), num_files, ext)
        logger.info('open file %s', fout)
        f = open(fout, 'w')
        num_files += 1

    return 0

if __name__ == '__main__':
    main(sys.argv)
